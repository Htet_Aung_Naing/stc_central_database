package com.stc.centraldatabase.mgr.templatetwo;



import com.stc.centraldatabase.dao.templatetwo.TemplateTwoDataDao;
import com.stc.centraldatabase.dao.templatetwo.TemplateTwoDataSearchDao;
import com.stc.centraldatabase.model.templateone.TemplateSearchData;
import com.stc.centraldatabase.model.templatetwo.TemplateTwoData;
import com.stc.centraldatabase.model.templatetwo.TemplateTwoSearchPaginateData;

public class TemplateTwoMgr 
{
	public static boolean inserData(TemplateTwoData template)
	{
		TemplateTwoDataDao templateFiveDao = new TemplateTwoDataDao();
		return templateFiveDao.insertTemplateTwoData(template);
	}
	
	public static boolean updateData(TemplateTwoData activity)
	{
		TemplateTwoDataDao templateFiveDao = new TemplateTwoDataDao();
		return templateFiveDao.updateTemplateTwoData(activity);
	}
	
	public static TemplateTwoSearchPaginateData find(TemplateSearchData settingSearch)
	{
		TemplateTwoDataSearchDao settingSearchDao = new TemplateTwoDataSearchDao();
		return settingSearchDao.find(settingSearch);
	}
	
	public static TemplateTwoData findById(String id)
	{
		TemplateTwoDataSearchDao activitySearchDao = new TemplateTwoDataSearchDao();
		return activitySearchDao.findById(id);
	}
	
	public static boolean validateUpdate(String id,String code)
	{
		TemplateTwoDataSearchDao activitySearchDao = new TemplateTwoDataSearchDao();
		return activitySearchDao.validateSettingUpdate(id, code);
	}

	public static boolean delete(String id)
	{
		TemplateTwoDataDao templateFiveDao = new TemplateTwoDataDao();
		return templateFiveDao.delete(id);
	}
}
