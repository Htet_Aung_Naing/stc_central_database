package com.stc.centraldatabase.service.activity;

import com.stc.centraldatabase.mgr.activity.ActivityMgr;
import com.stc.centraldatabase.model.activity.ActivityData;
import com.stc.centraldatabase.model.activity.ActivitySearchData;
import com.stc.centraldatabase.model.activity.ActivitySearchPaginateData;
import com.stc.centraldatatbase.util.Result;

public class ActivityDataServiceImpl implements ActivityDataService{

	@Override
	public Result insertActivityData(ActivityData activity) {
		return ActivityMgr.inserData(activity);
	}

	@Override
	public boolean updateData(ActivityData activity) {
		return ActivityMgr.updateData(activity);
	}

	@Override
	public ActivitySearchPaginateData find(ActivitySearchData settingSearch) {
		return ActivityMgr.find(settingSearch);
	}

	@Override
	public ActivityData findById(String id) {
		return ActivityMgr.findById(id);
	}

	@Override
	public boolean validateSettingUpdate(String id, String code) {
		return ActivityMgr.validateUpdate(id, code);
	}

	@Override
	public boolean delete(String activityId) {
		return ActivityMgr.delete(activityId);
	}

}
