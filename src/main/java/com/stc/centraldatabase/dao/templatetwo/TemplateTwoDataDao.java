package com.stc.centraldatabase.dao.templatetwo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Date;

import com.stc.centraldatabase.model.templatetwo.TemplateTwoData;
import com.stc.centraldatatbase.util.DataBaseConnection;
import com.stc.centraldatatbase.util.PrjUtil;

public class TemplateTwoDataDao {
	
	public boolean insertTemplateTwoData(TemplateTwoData setting)
	{
		Connection con = null;
		PreparedStatement ps = null;
		

		 	try 
		 	{
				String query;
				
				if(setting != null)
				{
					con = DataBaseConnection.getConnection();
				     query = "INSERT INTO TEMPLATE_TWO (PARENT_ID, CREATED_DATE, CREATED_USER, MODIFIED_DATE, "
				     		+ "MODIFIED_USER, CODE, DESCRIPTION, SETTING_DATE, OSC_COMMUNITY_BOY, OSC_COMMUNITY_GIRL, "
				     		+ "COMMUNITY_DATASOURCE, OSC_CMSS_BOY, OSC_CMSS_GIRL, CMSS_DATASOURCE)"
				     		+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?)";
				     
				     
				     int i = 1;
				     ps = con.prepareStatement(query);
				     ps.setInt(i++, setting.getParentid());				    
				     ps.setString(i++, PrjUtil.stringToDate(new Date()));
				     ps.setString(i++, setting.getCreatedUserName());
				     ps.setString(i++, PrjUtil.stringToDate(new Date()));	
				     ps.setString(i++, setting.getModifiedUserName());			    
				     ps.setString(i++, setting.getCode());
				     ps.setString(i++, setting.getDesc() );
				     ps.setString(i++, PrjUtil.stringToDate(setting.getSettingDate()));
				     ps.setInt(i++, setting.getOscCommunityBoy());
				     ps.setInt(i++, setting.getOscCommunityGirl());
				     ps.setString(i++, setting.getOscCommunityDataSource());
				     ps.setInt(i++, setting.getOscEnrollCmssBoy());
				     ps.setInt(i++, setting.getOscEnrollCmssGirl());
				     ps.setString(i++, setting.getOscEnrollCmssDatasource());
				     ps.execute();
				     con.close();
				     return true; 
					   				  
				}else
				{
				
					return false;
				}
				 
			} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
	
			return false;
			
		}
				
		}
	
	public  boolean updateTemplateTwoData(TemplateTwoData setting) 
	{
		Connection con = null;
		PreparedStatement ps = null;
		String query;
		try {
			con = DataBaseConnection.getConnection();
			
		
	     query = "UPDATE TEMPLATE_TWO SET MODIFIED_DATE=?, MODIFIED_USER=?"
			   + ", CODE=?, DESCRIPTION=?, SETTING_DATE=?, OSC_COMMUNITY_BOY=?, OSC_COMMUNITY_GIRL=?, "
			   + "COMMUNITY_DATASOURCE=?, OSC_CMSS_BOY=?, OSC_CMSS_GIRL=?, CMSS_DATASOURCE=?"
			      + " WHERE ID = ?";
			     		
			    int i = 1;
				ps = con.prepareStatement(query);
				ps.setString(i++, PrjUtil.stringToDate(new Date()));
				ps.setString(i++, setting.getModifiedUserName());	
			    ps.setString(i++, setting.getCode());
				ps.setString(i++, setting.getDesc());			
				ps.setString(i++, PrjUtil.stringToDate(setting.getSettingDate()));
				ps.setInt(i++, setting.getOscCommunityBoy());
				ps.setInt(i++, setting.getOscCommunityGirl());
				ps.setString(i++, setting.getOscCommunityDataSource());
				ps.setInt(i++, setting.getOscEnrollCmssBoy());
				ps.setInt(i++, setting.getOscEnrollCmssGirl());
				ps.setString(i++, setting.getOscEnrollCmssDatasource());
				ps.setLong(i++, setting.getId());
				ps.execute();
			      
			    con.close();
			    return true;
			
			
			
		} catch (SQLException ex) {
			System.out.println("Templage Two Data Registration Update error -->" + ex.getMessage());
			return false;
		} finally {
			DataBaseConnection.close(con);
		}
	}
	
	public  boolean delete(String settingid) {
		Connection con = null;
		PreparedStatement ps = null;
		String query;
		try {
			con = DataBaseConnection.getConnection();
			    	        
			     query = "DELETE FROM TEMPLATE_TWO where id=?";
			    int i = 1;
				ps = con.prepareStatement(query);
				ps.setString(i++, settingid);
				ps.execute();
			      
			    con.close();
			    return true;
			
			
			
		} catch (SQLException ex) {
			System.out.println("Template Two Delete error -->" + ex.getMessage());
			return false;
		} finally {
			DataBaseConnection.close(con);
		}
	}

}
