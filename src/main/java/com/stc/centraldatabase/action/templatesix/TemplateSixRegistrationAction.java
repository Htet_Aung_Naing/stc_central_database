package com.stc.centraldatabase.action.templatesix;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import com.stc.centraldatabase.model.setting.SettingData;
import com.stc.centraldatabase.model.templatesix.TemplateSixData;
import com.stc.centraldatabase.service.templatesix.TemplateSixDataService;
import com.stc.centraldatabase.service.templatesix.TemplateSixDataServiceImpl;
import com.stc.centraldatatbase.util.SessionUtil;



@ManagedBean(name = "templateSixRegistrationAction")
@ViewScoped
public class TemplateSixRegistrationAction implements Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = -8891520626941344235L;
	/**
	 * 
	 */


	TemplateSixDataService templateService;
	String title;
	String parentid;
	DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
	boolean isPopup = false;
	

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	





	public TemplateSixData getTemplateSixData() {
		return templateSixData;
	}

	public void setTemplateSixData(TemplateSixData templateSixData) {
		this.templateSixData = templateSixData;
	}







	@ManagedProperty(value = "#{templatesix}")
	TemplateSixData templateSixData;
	
	


	


	@PostConstruct
	public void init()  
	{
		this.templateSixData = new TemplateSixData();	
		if(FacesContext.getCurrentInstance().getExternalContext().getFlash().get("templateParentid") != null)
		{
			this.parentid = (String) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("templateParentid");
			this.title = (String) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("templateTitle");
		}else
		{
			HttpSession session = SessionUtil.getSession();
			this.title = session.getAttribute("templateTitle").toString();
			this.parentid = session.getAttribute("templateParentid").toString();
			isPopup = true;
		}
		
		this.templateService = new TemplateSixDataServiceImpl();
		
	}
	
	public String getSettingmenuLabel(List<SettingData> settingList , long key)
	{
		String res = "";
		
		for (SettingData settingData : settingList) 
		{
			if(key == settingData.getId())
			{
				res = settingData.getT1();
				break;
			}
				
		}
		return res;
	}
	


	
	
	public String register()
	{
		HttpSession session = SessionUtil.getSession();
		templateSixData.setCreatedUserName((String) session.getAttribute("userid"));
		templateSixData.setModifiedUserName((String) session.getAttribute("userid"));
		templateSixData.setParentid(Integer.parseInt(parentid));
		if(templateService.insertTemplateSixData(templateSixData))
		{	
			
			if(isPopup)
			{
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "INFO!", "Template Six Data  Register Successfully!"));
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				return "template_six_popup";
			}		
			else
			{
				FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateParentid",  String.valueOf(templateSixData.getParentid()));		
				FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateTitle",  templateSixData.getCode());
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "INFO!", "Template Six Data  Register Successfully!"));
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				return "template_six_registration";
			}				
		}			
		else
		{
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "ERROR!", "Setting's Codes are duplicate!"));
			return null;
		}
	}
	
	public String back()
	{	
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateTitle",  templateSixData.getCode());
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateParentid", String.valueOf(templateSixData.getParentid()));		
		return "activitySearch";
	}
	
}
