package com.stc.centraldatabase.model.activity;

import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.stc.centraldatabase.service.activity.ActivityDataService;
import com.stc.centraldatabase.service.activity.ActivityDataServiceImpl;




public class LazyActivityDataModel extends LazyDataModel<ActivityData>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7167457755325524229L;
	/**
	 * 
	 */


	
	ActivitySearchData activitySearchData;
	ActivityDataService settingService;



	public ActivitySearchData getActivitySearchData() {
		return activitySearchData;
	}

	public void setActivitySearchData(ActivitySearchData ActivitySearchData) {
		this.activitySearchData = ActivitySearchData;
	}

	public  LazyActivityDataModel(ActivitySearchData ActivitySearchData) {
		this.activitySearchData = ActivitySearchData;

	}

	@Override
	public List<ActivityData> load(int first, int pageSize, String sortField,
			SortOrder sortOrder, Map<String, Object> filters) {
		settingService = new ActivityDataServiceImpl();
		
		activitySearchData.setOffset(first);
		activitySearchData.setLimit(pageSize);
		
		ActivitySearchPaginateData userPaginateData = settingService.find(activitySearchData);
		
		
		setRowCount(userPaginateData.getCount());
		setPageSize(activitySearchData.getLimit());
		
		List<ActivityData> userList = userPaginateData.getActivityDataLsit();
		
		return userList;
	}

}
