package com.stc.centraldatabase.dao.templateone;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Date;

import com.stc.centraldatabase.model.templateone.TemplateOneData;
import com.stc.centraldatatbase.util.DataBaseConnection;
import com.stc.centraldatatbase.util.PrjUtil;

public class TemplateOneDataDao {
	
	public boolean insertTemplateOneData(TemplateOneData setting)
	{
		Connection con = null;
		PreparedStatement ps = null;
		

		 	try 
		 	{
				String query;
				
				if(setting != null)
				{
					con = DataBaseConnection.getConnection();
				     query = "INSERT INTO TEMPLATE_ONE ( PARENT_ID,CREATED_USER,CREATED_DATE,MODIFIED_USER"
				     		+ ",MODIFIED_DATE,SETTING_DATE,CODE,DESCRIPTION,NO_OF_HOUSE, NO_OF_HOUSEHOLD, MALE_LESS_5,"
				     		+ " FEMALE_LESS_5, MALE_BTW_5_18, FEMALE_BTW_5_18, MALE_OVER_18, FEMALE_OVER_18)"
				     		+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?,?,?)";
				     
				     
				     int i = 1;
				     ps = con.prepareStatement(query);
				     ps.setInt(i++, setting.getParentid());
				     ps.setString(i++, setting.getCreatedUserName());
				     ps.setString(i++, PrjUtil.stringToDate(new Date()));
				     ps.setString(i++, setting.getModifiedUserName());
				     ps.setString(i++, PrjUtil.stringToDate(new Date()));
				     ps.setString(i++, PrjUtil.stringToDate(setting.getSettingDate()));
				     ps.setString(i++, setting.getCode());
				     ps.setString(i++, setting.getDesc() );
				     ps.setInt(i++, setting.getNoOfHouse());
				     ps.setInt(i++, setting.getNoOfHouseHold());
				     ps.setInt(i++, setting.getMaleLessthan5YearsCount());
				     ps.setInt(i++, setting.getFemaleLessthan5YearsCount());
				     ps.setInt(i++, setting.getMale5T18Count());
				     ps.setInt(i++, setting.getFemale5T18Count());
				     ps.setInt(i++, setting.getMaleOver18Count());
				     ps.setInt(i++, setting.getFemaleOver18Count());
				     
				     ps.execute();
				     con.close();
				     return true; 
					   				  
				}else
				{
				
					return false;
				}
				 
			} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
	
			return false;
			
		}
				
		}
	
	public  boolean updateTemplateOneData(TemplateOneData setting) 
	{
		Connection con = null;
		PreparedStatement ps = null;
		String query;
		try {
			con = DataBaseConnection.getConnection();
			
		
	     query = "UPDATE TEMPLATE_ONE SET SETTING_DATE = ?,MODIFIED_DATE = ?, MODIFIED_USER = ?,"
			     		+ "CODE=?,Description=?,NO_OF_HOUSE=?,NO_OF_HOUSEHOLD=?,MALE_LESS_5=?,"
			     		+"FEMALE_LESS_5=?, MALE_BTW_5_18=?, FEMALE_BTW_5_18=?, MALE_OVER_18=?, FEMALE_OVER_18=?"
			     		+ " WHERE ID = ?";
			     		
			    int i = 1;
				ps = con.prepareStatement(query);
				 ps.setString(i++, PrjUtil.stringToDate(setting.getSettingDate()));

				ps.setString(i++, PrjUtil.stringToDate(new Date()));
				ps.setString(i++, setting.getModifiedUserName());
				ps.setString(i++, setting.getCode());
				ps.setString(i++, setting.getDesc());
				ps.setInt(i++, setting.getNoOfHouse());
				ps.setInt(i++, setting.getNoOfHouseHold());
				ps.setInt(i++, setting.getMaleLessthan5YearsCount());
				ps.setInt(i++, setting.getFemaleLessthan5YearsCount());
				ps.setInt(i++, setting.getMale5T18Count());
				ps.setInt(i++, setting.getFemale5T18Count());
				ps.setInt(i++, setting.getMaleOver18Count());
				ps.setInt(i++, setting.getFemaleOver18Count());
				ps.setLong(i++, setting.getId());
				ps.execute();
			      
			    con.close();
			    return true;
			
			
			
		} catch (SQLException ex) {
			System.out.println("Templage One Data Registration Update error -->" + ex.getMessage());
			return false;
		} finally {
			DataBaseConnection.close(con);
		}
	}
	
	public  boolean delete(String settingid) {
		Connection con = null;
		PreparedStatement ps = null;
		String query;
		try {
			con = DataBaseConnection.getConnection();
			    	        
			     query = "DELETE FROM TEMPLATE_ONE where id=?";
			    int i = 1;
				ps = con.prepareStatement(query);
				ps.setString(i++, settingid);
				ps.execute();
			      
			    con.close();
			    return true;
			
			
			
		} catch (SQLException ex) {
			System.out.println("Template one Delete error -->" + ex.getMessage());
			return false;
		} finally {
			DataBaseConnection.close(con);
		}
	}

}
