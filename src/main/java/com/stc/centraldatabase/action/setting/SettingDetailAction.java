package com.stc.centraldatabase.action.setting;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import com.stc.centraldatabase.model.setting.SettingData;
import com.stc.centraldatabase.service.setting.SettingDataService;
import com.stc.centraldatabase.service.setting.SettingDataServiceImpl;
import com.stc.centraldatatbase.util.SessionUtil;



@ManagedBean(name = "settingDetailAction")
@ViewScoped
public class SettingDetailAction implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4372970301156244048L;
	SettingDataService settingService;
	String settingid;
	String title;
	String tbname;

	SettingData settingData;

	public String getSettingid() {
		return settingid;
	}


	public void setSettingid(String settingid) {
		this.settingid = settingid;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public SettingData getSettingData() {
		return settingData;
	}

	public void setSettingData(SettingData settingData) {
		this.settingData = settingData;
	}

	@PostConstruct
	public void init()
	{
		HttpSession session = SessionUtil.getSession();
		title = (String) session.getAttribute("detail_title");
			settingService = new  SettingDataServiceImpl();
			settingData = new SettingData();
			if(settingid == null)
			{
				tbname = (String) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("tbname");
				settingid = (String) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("impact_id");
				settingData = settingService.findByid(settingid,tbname);
			}
		
	}
	
}
