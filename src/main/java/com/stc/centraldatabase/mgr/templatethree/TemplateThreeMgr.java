package com.stc.centraldatabase.mgr.templatethree;



import com.stc.centraldatabase.dao.templatethree.TemplateThreeDataDao;
import com.stc.centraldatabase.dao.templatethree.TemplateThreeDataSearchDao;
import com.stc.centraldatabase.model.templateone.TemplateSearchData;
import com.stc.centraldatabase.model.templatethree.TemplateThreeData;
import com.stc.centraldatabase.model.templatethree.TemplateThreeSearchPaginateData;

public class TemplateThreeMgr 
{
	public static boolean inserData(TemplateThreeData template)
	{
		TemplateThreeDataDao termplateThreeDao = new TemplateThreeDataDao();
		return termplateThreeDao.insertTemplateThreeData(template);
	}
	
	public static boolean updateData(TemplateThreeData activity)
	{
		TemplateThreeDataDao termplateThreeDao = new TemplateThreeDataDao();
		return termplateThreeDao.updateTemplateThreeData(activity);
	}
	
	public static TemplateThreeSearchPaginateData find(TemplateSearchData settingSearch)
	{
		TemplateThreeDataSearchDao settingSearchDao = new TemplateThreeDataSearchDao();
		return settingSearchDao.find(settingSearch);
	}
	
	public static TemplateThreeData findById(String id)
	{
		TemplateThreeDataSearchDao activitySearchDao = new TemplateThreeDataSearchDao();
		return activitySearchDao.findById(id);
	}
	
	public static boolean validateUpdate(String id,String code)
	{
		TemplateThreeDataSearchDao activitySearchDao = new TemplateThreeDataSearchDao();
		return activitySearchDao.validateSettingUpdate(id, code);
	}

	public static boolean delete(String id)
	{
		TemplateThreeDataDao termplateThreeDao = new TemplateThreeDataDao();
		return termplateThreeDao.delete(id);
	}
}
