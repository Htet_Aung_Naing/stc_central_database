package com.stc.centraldatabase.model.setting;

import java.io.Serializable;

public class SettingSearchData implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6926228751031854183L;
	
	String code;
	
	int n1;
	int n2;
	int n3;
	int n4;
	
	
	
	public int getN3() {
		return n3;
	}
	public void setN3(int n3) {
		this.n3 = n3;
	}
	public int getN4() {
		return n4;
	}
	public void setN4(int n4) {
		this.n4 = n4;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public int getN1() {
		return n1;
	}
	public void setN1(int n1) {
		this.n1 = n1;
	}
	public int getN2() {
		return n2;
	}
	public void setN2(int n2) {
		this.n2 = n2;
	}
	public int getOffset() {
		return offset;
	}
	public void setOffset(int offset) {
		this.offset = offset;
	}
	public int getLimit() {
		return limit;
	}
	public void setLimit(int limit) {
		this.limit = limit;
	}


	int offset;
	int limit;
	

	
	public SettingSearchData()
	{
		this.code = "";
		this.n1 = 0;
		this.n2 = 0;
		this.n3 = 0;
		this.n4 = 0;
		this.offset = 0;
		this.limit = 0;
	}
	
	

}
