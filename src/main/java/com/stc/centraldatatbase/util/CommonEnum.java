package com.stc.centraldatatbase.util;

public class CommonEnum {
	
	
	public enum Gender{  
		Male(1,"Male"),Female(2,"Female"); 

		private final int value;  
		private final String description;        
		public int value(){  return this.value;}  
		public String description(){ return this.description; }

		Gender(int aStatus, String desc){  
			this.value = aStatus;  
			this.description = desc;  
		}  
	} 
	
	public enum Template{  
		template_one(1,"Template one"),template_2(2,"Template two"),template_3(3,"Template three"),template_4(4,"Template four"),template_5(5,"Template five")
		,template_six(6, "Template six"); 

		private final int value;  
		private final String description;        
		public int value(){  return this.value;}  
		public String description(){ return this.description; }

		Template(int aStatus, String desc){  
			this.value = aStatus;  
			this.description = desc;  
		}  
	} 
	
	public enum Template_Search_Page{  
		Template_One_Search_Page(1,"template_one_search"),Template_Two_Search_Page(2,"template_two_search"),Template_Three_Search_Page(3,"template_three_search"),
		Template_Four_Search_Page(4,"template_four_search"),Template_Five_Search_Page(5,"template_five_search"),Template_Six_Page(6,"template_six_search"); 

		private final int value;  
		private final String description;        
		public int value(){  return this.value;}  
		public String description(){ return this.description; }

		Template_Search_Page(int aStatus, String desc){  
			this.value = aStatus;  
			this.description = desc;  
		}  
	}
	
	public enum Template_Popup_Page{  
		Template1(1,"template_one_popup"),Template2(2,"template_two_popup"),Template3(3,"template_three_popup"),
		Template4(4,"template_four_popup"),Template5(5,"template_five_popup"),Template6(6,"template_six_popup"); 

		private final int value;  
		private final String description;        
		public int value(){  return this.value;}  
		public String description(){ return this.description; }

		Template_Popup_Page(int aStatus, String desc){  
			this.value = aStatus;  
			this.description = desc;  
		}  
	}
	
	
	
	public enum Template_Page{  
		Template1(1,"template_one_registration"),Template2(2,"template_two_registration"),Template3(3,"template_three_registration"),
		Template4(4,"template_four_registration"),Template5(5,"template_five_registration"),Template6(6,"template_six_registration"); 

		private final int value;  
		private final String description;        
		public int value(){  return this.value;}  
		public String description(){ return this.description; }

		Template_Page(int aStatus, String desc){  
			this.value = aStatus;  
			this.description = desc;  
		}  
	}
	
	public enum Role
	{  
		adminRole(2,"Admin Role"),userRole(3,"User Role"); 

		private final int value;  
		private final String description;        
		public int value(){  return this.value;}  
		public String description(){ return this.description; }

		Role(int aStatus, String desc){  
			this.value = aStatus;  
			this.description = desc;  
		}  
	} 
	
	public enum UserStatus{  
		
		active(1,"Active"),delete(0,"Not Active"); 
		private final int value;  
		private final String description;        
		public int value(){  return this.value;}  
		public String description(){ return this.description; }

		UserStatus(int aStatus, String desc){  
			this.value = aStatus;  
			this.description = desc;  
		}  
	}
	


}
