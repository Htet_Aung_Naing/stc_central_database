package com.stc.centraldatabase.model.templatesix;

import java.io.Serializable;
import java.util.List;

public class TemplateSixSearchPaginateData implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8793922769046576099L;
	
	int count;
	List<TemplateSixData> templateSixDataList;
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public List<TemplateSixData> getTemplateSixDataList() {
		return templateSixDataList;
	}
	public void setTemplateSixDataList(List<TemplateSixData> templateSixDataList) {
		this.templateSixDataList = templateSixDataList;
	}

	
	

}
