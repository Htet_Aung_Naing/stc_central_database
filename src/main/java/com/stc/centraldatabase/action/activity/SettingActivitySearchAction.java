package com.stc.centraldatabase.action.activity;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.primefaces.component.datatable.DataTable;

import com.stc.centraldatabase.model.activity.ActivitySearchData;
import com.stc.centraldatabase.model.activity.LazyActivityDataModel;
import com.stc.centraldatabase.model.setting.SettingData;
import com.stc.centraldatabase.service.setting.SettingDataService;
import com.stc.centraldatabase.service.setting.SettingDataServiceImpl;
import com.stc.centraldatatbase.util.CommonEnum;
import com.stc.centraldatatbase.util.SessionUtil;





@ManagedBean(name = "settingActivitySearchAction")
@ViewScoped
public class SettingActivitySearchAction implements Serializable{


	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6499344160282589714L;
	
	ActivitySearchData activitySearchData;
	List<SettingData> impactList;
	List<SettingData> outcomeList;
	List<SettingData> outputList;
	LazyActivityDataModel settingdataModel;
	SettingDataService settingService;

	public ActivitySearchData getActivitySearchData() {
		return activitySearchData;
	}

	public void setActivitySearchData(ActivitySearchData activitySearchData) {
		this.activitySearchData = activitySearchData;
	}

	public List<SettingData> getOutputList() {
		return outputList;
	}

	public void setOutputList(List<SettingData> outputList) {
		this.outputList = outputList;
	}

	public List<SettingData> getImpactList() {
		return impactList;
	}

	public void setImpactList(List<SettingData> impactList) {
		this.impactList = impactList;
	}

	public List<SettingData> getOutcomeList() {
		return outcomeList;
	}

	public void setOutcomeList(List<SettingData> outcomeList) {
		this.outcomeList = outcomeList;
	}


	public LazyActivityDataModel getSettingdataModel() {
		return settingdataModel;
	}

	public void setSettingdataModel(LazyActivityDataModel settingdataModel) {
		this.settingdataModel = settingdataModel;
	}

	public void reset(String targetDataTable) {
		resetPagination(targetDataTable);
		LoadData();
	}

	
	private void resetPagination(String targetDataTable) {
		DataTable d = (DataTable) FacesContext.getCurrentInstance().getViewRoot()
				.findComponent("settingGoalSearchForm:settingSearchTable");
		d.setFirst(0);
	}
	
	public void search(String targetDataTable) {

		// Check Joined Date From should be earlier than Joined Date To.
			// Reset pagination and do the lazy data model process.
			resetPagination(targetDataTable);
			settingdataModel = new LazyActivityDataModel(activitySearchData);
		
	}
	
	public String getTemplatePage(long id)
	{
		String res = "";
		for(CommonEnum.Template_Page g : CommonEnum.Template_Page.values())
		{
			if(g.value() == id)
			{
				res = g.description();
				break;
			}
			
		}
		return res;
	}
	
	public String getTemplateSearchPage(long id)
	{
		String res = "";
		for(CommonEnum.Template_Search_Page g : CommonEnum.Template_Search_Page.values())
		{
			if(g.value() == id)
			{
				res = g.description();
				break;
			}
			
		}
		return res;
	}
	
	
	@PostConstruct
	public void LoadData()
	{
		settingService = new SettingDataServiceImpl();
		activitySearchData = new ActivitySearchData();
		impactList = settingService.getSettingList("setting_goal");
		outcomeList = settingService.getSettingList("setting_outcome");
		this.outputList = settingService.getSettingList("setting_output");
		settingdataModel = new LazyActivityDataModel(activitySearchData);
		
	}
	
	public String edit(String id) {
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("tbname", "setting_activity");
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("activity_id", id);
		return "activityUpdate";
	}



	public String delete(String id) {
		HttpSession session = SessionUtil.getSession();
		session.setAttribute("delete_title", "Activity Delete");
		session.setAttribute("return_page", "activitySearch");
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("tbname", "setting_activity");
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("impact_id", id);
		return "settingActivityDelete";
	}
	
	public String detail(String id) {
		HttpSession session = SessionUtil.getSession();
		session.setAttribute("detail_title", "Activity Detail");
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("tbname", "setting_activity");
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("impact_id", id);
		return "settingActivityDetail";
	}
	
	public String registerTemplateData(String id , long templateKey , String code)
	{
		String returnPage = "";
		returnPage = getTemplatePage(templateKey);
		
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateParentid", id );
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateTitle",  code);
		return returnPage;
	}
	
	public String list(String id , long templateKey , String code)
	{
		String returnPage = "";
		returnPage = getTemplateSearchPage(templateKey);
		
		HttpSession session = SessionUtil.getSession();
		session.setAttribute("activityTitle", code);
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateParentid", id );
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateTitle",  code);
		return returnPage;
	}
	
	
	

}
