package com.stc.centraldatabase.model.templatesix;

import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.stc.centraldatabase.model.templateone.TemplateSearchData;
import com.stc.centraldatabase.service.templatesix.TemplateSixDataService;
import com.stc.centraldatabase.service.templatesix.TemplateSixDataServiceImpl;






public class LazyTemplateSixDataModel extends LazyDataModel<TemplateSixData>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7167457755325524229L;
	/**
	 * 
	 */


	
	TemplateSearchData settingSearchData;
	TemplateSixDataService settingService;
	String tbname = "";


	

	public TemplateSearchData getSettingSearchData() {
		return settingSearchData;
	}

	public void setSettingSearchData(TemplateSearchData settingSearchData) {
		this.settingSearchData = settingSearchData;
	}

	public  LazyTemplateSixDataModel(TemplateSearchData settingSearchData) {
		this.settingSearchData = settingSearchData;
	}

	@Override
	public List<TemplateSixData> load(int first, int pageSize, String sortField,
			SortOrder sortOrder, Map<String, Object> filters) {
		settingService = new TemplateSixDataServiceImpl();
		
		settingSearchData.setOffset(first);
		settingSearchData.setLimit(pageSize);
		
		TemplateSixSearchPaginateData userPaginateData = settingService.find(settingSearchData);
		
		setRowCount(userPaginateData.getCount());
		setPageSize(settingSearchData.getLimit());
		
		List<TemplateSixData> userList = userPaginateData.getTemplateSixDataList();
		
		return userList;
	}

}
