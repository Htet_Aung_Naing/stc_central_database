package com.stc.centraldatabase.mgr.setting;

import java.util.List;

import com.stc.centraldatabase.dao.setting.SettingDataDao;
import com.stc.centraldatabase.dao.setting.SettingSearchDao;
import com.stc.centraldatabase.model.setting.SettingData;
import com.stc.centraldatabase.model.setting.SettingSearchData;
import com.stc.centraldatabase.model.setting.SettingSearchPaginateData;

public class SettingDataMgr {
	
	public static boolean InsertSetting(SettingData setting , String tbname)
	{
		
		SettingSearchDao settingSearch = new SettingSearchDao();
		if(settingSearch.validateSettingUpdate(String.valueOf(setting.getId()), setting.getT1(), tbname))
		{
			SettingDataDao settingDao = new SettingDataDao();
			
			return settingDao.insertSettingData(setting,tbname);
		}else 
			return false;
	
	}
	
	public static SettingSearchPaginateData find(SettingSearchData settingSearch , String tbname)
	{
		SettingSearchDao settingSearchDao = new SettingSearchDao();
		return settingSearchDao.find(settingSearch, tbname);
	}
	
	public static SettingData findById(String id, String tbname)
	{
		SettingSearchDao settingSearchDao = new SettingSearchDao();
		return settingSearchDao.getSettingById(id, tbname);
	}
	
	public static SettingData findByCode(String id, String tbname)
	{
		SettingSearchDao settingSearchDao = new SettingSearchDao();
		return settingSearchDao.getSettingByCode(id, tbname);
	}

	public static boolean delete(String id , String tbname)
	{
		SettingDataDao settingDao = new SettingDataDao();
		return settingDao.delete(id, tbname);
	}
	
	public static boolean update(SettingData setting , String tbname)
	{
		SettingDataDao settingDao = new SettingDataDao();
		return settingDao.update(setting,tbname);
	}
	
	public static List<SettingData> getSettingList(String tbname)
	{
		SettingSearchDao settingSearchDao = new SettingSearchDao();
		return settingSearchDao.getSettingList(tbname);
	}
	
	public static boolean validateSettingUpdate(String id, String code, String tbname)
	{
		SettingSearchDao serttingSearchDao = new SettingSearchDao();
		return serttingSearchDao.validateSettingUpdate(id, code, tbname);
	}
	
}
