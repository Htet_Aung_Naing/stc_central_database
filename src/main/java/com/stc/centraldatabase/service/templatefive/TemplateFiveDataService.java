package com.stc.centraldatabase.service.templatefive;

import com.stc.centraldatabase.model.templatefive.TemplateFiveData;
import com.stc.centraldatabase.model.templatefive.TemplateFiveSearchPaginateData;
import com.stc.centraldatabase.model.templateone.TemplateSearchData;

public interface TemplateFiveDataService {
	
	
boolean insertTemplateFiveData(TemplateFiveData activity);
	
	boolean updateData(TemplateFiveData activity);
	
	TemplateFiveSearchPaginateData find(TemplateSearchData settingSearch );
	
	TemplateFiveData findById(String id);
	
	boolean validateTemplateDataUpdate(String id, String code);
	
	boolean delete(String activityId);

}
