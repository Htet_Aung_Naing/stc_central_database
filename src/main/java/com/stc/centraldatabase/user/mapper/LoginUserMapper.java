package com.stc.centraldatabase.user.mapper;



import org.apache.ibatis.annotations.Param;

import com.stc.centraldatabase.model.user.UserInfo;


/**
 * LoginUserDaoImpl.java
 *
 * @author Ei Ei Swe Minn
 */
public interface LoginUserMapper {

	/**
	 * 
	 * @param id
	 * @return
	 */
	UserInfo selectUser(@Param("userId") String id);

}
