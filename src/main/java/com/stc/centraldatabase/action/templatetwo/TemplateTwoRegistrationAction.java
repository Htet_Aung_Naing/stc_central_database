package com.stc.centraldatabase.action.templatetwo;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import com.stc.centraldatabase.model.setting.SettingData;
import com.stc.centraldatabase.model.templatetwo.TemplateTwoData;
import com.stc.centraldatabase.service.templatetwo.TemplateTwoDataService;
import com.stc.centraldatabase.service.templatetwo.TemplateTwoDataServiceImpl;
import com.stc.centraldatatbase.util.SessionUtil;



@ManagedBean(name = "templateTwoRegistrationAction")
@ViewScoped
public class TemplateTwoRegistrationAction implements Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = -8891520626941344235L;
	/**
	 * 
	 */


	TemplateTwoDataService templateService;
	String title;
	String parentid;
	DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
	boolean isPopup = false;
	

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	





	public TemplateTwoData getTemplateTwoData() {
		return templateTwoData;
	}

	public void setTemplateTwoData(TemplateTwoData templateTwoData) {
		this.templateTwoData = templateTwoData;
	}







	@ManagedProperty(value = "#{templatetwo}")
	TemplateTwoData templateTwoData;
	
	


	


	@PostConstruct
	public void init()  
	{
		this.templateTwoData = new TemplateTwoData();	
		if(FacesContext.getCurrentInstance().getExternalContext().getFlash().get("templateParentid") != null)
		{
			this.parentid = (String) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("templateParentid");
			this.title = (String) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("templateTitle");
		}else
		{
			HttpSession session = SessionUtil.getSession();
			this.title = session.getAttribute("templateTitle").toString();
			this.parentid = session.getAttribute("templateParentid").toString();
			isPopup = true;
		}
		
		this.templateService = new TemplateTwoDataServiceImpl();
		
	}
	
	public String getSettingmenuLabel(List<SettingData> settingList , long key)
	{
		String res = "";
		
		for (SettingData settingData : settingList) 
		{
			if(key == settingData.getId())
			{
				res = settingData.getT1();
				break;
			}
				
		}
		return res;
	}
	


	
	
	public String register()
	{
		HttpSession session = SessionUtil.getSession();
		templateTwoData.setCreatedUserName((String) session.getAttribute("userid"));
		templateTwoData.setModifiedUserName((String) session.getAttribute("userid"));
		templateTwoData.setParentid(Integer.parseInt(parentid));
		if(templateService.insertTemplateTwoData(templateTwoData))
		{	
			
			if(isPopup)
			{
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "INFO!", "Template Two Data  Register Successfully!"));
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				return "template_two_popup";
			}			
			else
			{
				FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateParentid",  String.valueOf(templateTwoData.getParentid()));		
				FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateTitle",  templateTwoData.getCode());
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "INFO!", "Template Two Data  Register Successfully!"));
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				return "template_two_registration";
			}
				
		}			
		else
		{
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "ERROR!", "Setting's Codes are duplicate!"));
			return null;
		}
	}
	
	public String back()
	{	
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateTitle",  templateTwoData.getCode());
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateParentid", String.valueOf(templateTwoData.getParentid()));		
		return "activitySearch";
	}
	
}
