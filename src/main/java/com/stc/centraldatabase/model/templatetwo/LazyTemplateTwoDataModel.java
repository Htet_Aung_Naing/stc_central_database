package com.stc.centraldatabase.model.templatetwo;

import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.stc.centraldatabase.model.templateone.TemplateSearchData;
import com.stc.centraldatabase.service.templatetwo.TemplateTwoDataService;
import com.stc.centraldatabase.service.templatetwo.TemplateTwoDataServiceImpl;






public class LazyTemplateTwoDataModel extends LazyDataModel<TemplateTwoData>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7167457755325524229L;
	/**
	 * 
	 */


	
	TemplateSearchData settingSearchData;
	TemplateTwoDataService settingService;
	String tbname = "";


	

	public TemplateSearchData getSettingSearchData() {
		return settingSearchData;
	}

	public void setSettingSearchData(TemplateSearchData settingSearchData) {
		this.settingSearchData = settingSearchData;
	}

	public  LazyTemplateTwoDataModel(TemplateSearchData settingSearchData) {
		this.settingSearchData = settingSearchData;
	}

	@Override
	public List<TemplateTwoData> load(int first, int pageSize, String sortField,
			SortOrder sortOrder, Map<String, Object> filters) {
		settingService = new TemplateTwoDataServiceImpl();
		
		settingSearchData.setOffset(first);
		settingSearchData.setLimit(pageSize);
		
		TemplateTwoSearchPaginateData userPaginateData = settingService.find(settingSearchData);
		
		setRowCount(userPaginateData.getCount());
		setPageSize(settingSearchData.getLimit());
		
		List<TemplateTwoData> userList = userPaginateData.getTemplateTwoDataList();
		
		return userList;
	}

}
