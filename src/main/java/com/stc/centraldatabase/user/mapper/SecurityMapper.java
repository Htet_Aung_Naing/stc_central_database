package com.stc.centraldatabase.user.mapper;

import com.stc.centraldatabase.model.user.LoginUser;



public interface SecurityMapper {
	/**
	 * For Spring Security
	 */
	public LoginUser loadUserByUserId(String username);
}