package com.stc.centraldatabase.dao.templatefour;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Date;

import com.stc.centraldatabase.model.templatefour.TemplateFourData;
import com.stc.centraldatatbase.util.DataBaseConnection;
import com.stc.centraldatatbase.util.PrjUtil;

public class TemplateFourDataDao {
	
	public boolean insertTemplateFourData(TemplateFourData setting)
	{
		Connection con = null;
		PreparedStatement ps = null;
		

		 	try 
		 	{
				String query;
				
				if(setting != null)
				{
					con = DataBaseConnection.getConnection();
				     query = "INSERT INTO TEMPLATE_FOUR (PARENT_ID, CREATED_DATE, CREATED_USER, "
				     		+ "MODIFIED_DATE, MODIFIED_USER, CODE, DESCRIPTION, SETTING_DATE, "
				     		+ "IEC_NAME, NO_OF_PIECES)"
				     		+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
				     
				     
				     int i = 1;
				     ps = con.prepareStatement(query);
				     ps.setInt(i++, setting.getParentid());				    
				     ps.setString(i++, PrjUtil.stringToDate(new Date()));
				     ps.setString(i++, setting.getCreatedUserName());
				     ps.setString(i++, PrjUtil.stringToDate(new Date()));	
				     ps.setString(i++, setting.getModifiedUserName());			    
				     ps.setString(i++, setting.getCode());
				     ps.setString(i++, setting.getDesc() );
				     ps.setString(i++, PrjUtil.stringToDate(setting.getSettingDate()));
				     ps.setString(i++, setting.getNameOfIEC());
				     ps.setInt(i++, setting.getNoOfPiece());
				     
				     ps.execute();
				     con.close();
				     return true; 
					   				  
				}else
				{
				
					return false;
				}
				 
			} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
	
			return false;
			
		}
				
		}
	
	public  boolean updateTemplateFourData(TemplateFourData setting) 
	{
		Connection con = null;
		PreparedStatement ps = null;
		String query;
		try {
			con = DataBaseConnection.getConnection();
			
		
	     query = "UPDATE TEMPLATE_FOUR SET MODIFIED_DATE=?, MODIFIED_USER=?, CODE=?, DESCRIPTION=?, SETTING_DATE=?, "
				     		+ "IEC_NAME=?, NO_OF_PIECES=? WHERE ID = ?";
			     		
			    int i = 1;
				ps = con.prepareStatement(query);
				ps.setString(i++, PrjUtil.stringToDate(new Date()));
				ps.setString(i++, setting.getModifiedUserName());	
			    ps.setString(i++, setting.getCode());
				ps.setString(i++, setting.getDesc());			
				ps.setString(i++, PrjUtil.stringToDate(setting.getSettingDate()));
				ps.setString(i++, setting.getNameOfIEC());
				ps.setInt(i++, setting.getNoOfPiece());
				ps.setLong(i++, setting.getId());
				ps.execute();
			      
			    con.close();
			    return true;
			
			
			
		} catch (SQLException ex) {
			System.out.println("Templage Four Data Registration Update error -->" + ex.getMessage());
			return false;
		} finally {
			DataBaseConnection.close(con);
		}
	}
	
	public  boolean delete(String settingid) {
		Connection con = null;
		PreparedStatement ps = null;
		String query;
		try {
			con = DataBaseConnection.getConnection();
			    	        
			     query = "DELETE FROM TEMPLATE_FOUR where id=?";
			    int i = 1;
				ps = con.prepareStatement(query);
				ps.setString(i++, settingid);
				ps.execute();
			      
			    con.close();
			    return true;
			
			
			
		} catch (SQLException ex) {
			System.out.println("Template Four Delete error -->" + ex.getMessage());
			return false;
		} finally {
			DataBaseConnection.close(con);
		}
	}

}
