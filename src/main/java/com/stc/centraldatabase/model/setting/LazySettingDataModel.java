package com.stc.centraldatabase.model.setting;

import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.stc.centraldatabase.service.setting.SettingDataService;
import com.stc.centraldatabase.service.setting.SettingDataServiceImpl;




public class LazySettingDataModel extends LazyDataModel<SettingData>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7167457755325524229L;
	/**
	 * 
	 */


	
	SettingSearchData settingSearchData;
	SettingDataService settingService;
	String tbname = "";


	public SettingSearchData getSettingSearchData() {
		return settingSearchData;
	}

	public void setSettingSearchData(SettingSearchData settingSearchData) {
		this.settingSearchData = settingSearchData;
	}

	public  LazySettingDataModel(SettingSearchData settingSearchData , String tbName) {
		this.settingSearchData = settingSearchData;
		this.tbname = tbName;
	}

	@Override
	public List<SettingData> load(int first, int pageSize, String sortField,
			SortOrder sortOrder, Map<String, Object> filters) {
		settingService = new SettingDataServiceImpl();
		
		settingSearchData.setOffset(first);
		settingSearchData.setLimit(pageSize);
		
		SettingSearchPaginateData userPaginateData = settingService.find(settingSearchData,tbname);
		
		setRowCount(userPaginateData.getCount());
		setPageSize(settingSearchData.getLimit());
		
		List<SettingData> userList = userPaginateData.getSettingDataList();
		
		return userList;
	}

}
