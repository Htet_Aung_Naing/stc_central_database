package com.stc.centraldatabase.action.user;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.stc.centraldatabase.model.user.UserInfo;
import com.stc.centraldatabase.service.user.UserService;
import com.stc.centraldatabase.service.user.UserServiceImpl;



@ManagedBean(name = "userDetailAction")
@ViewScoped
public class UserDetailAction implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4372970301156244048L;
	UserService userService;
	String userid;
	

	UserInfo userData;

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public UserInfo getUserData() {
		return userData;
	}

	public void setUserData(UserInfo userData) {
		this.userData = userData;
	}
	
	
	@PostConstruct
	public void init()
	{
			userService = new  UserServiceImpl();
			userData = new UserInfo();
			if(userid == null)
			{
				userid = (String) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("userId");
				userData = userService.getUserbyId(userid);
			}
		
	}
	
}
