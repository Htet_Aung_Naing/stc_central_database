package com.stc.centraldatabase.action.templatefour;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import com.stc.centraldatabase.model.setting.SettingData;
import com.stc.centraldatabase.model.templatefour.TemplateFourData;
import com.stc.centraldatabase.service.templatefour.TemplateFourDataService;
import com.stc.centraldatabase.service.templatefour.TemplateFourDataServiceImpl;
import com.stc.centraldatatbase.util.SessionUtil;



@ManagedBean(name = "templateFourUpdateAction")
@ViewScoped
public class TemplateFourUpdateAction implements Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = -425655372664387173L;

	TemplateFourDataService templateService;
	String title;
	DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
	String templateId;
	String templateOnecode= "";

	

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public TemplateFourData getTemplateFourData() {
		return templateFourData;
	}

	public void setTemplateFourData(TemplateFourData templateFourData) {
		this.templateFourData = templateFourData;
	}


	@ManagedProperty(value = "#{templatefour}")
	TemplateFourData templateFourData;
	

	@PostConstruct
	public void init()  
	{
		this.templateFourData = new TemplateFourData();
		
		this.templateId = (String) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("templateid");
		this.templateOnecode = (String) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("templatCode");
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateParentid", templateId );
		this.templateService = new TemplateFourDataServiceImpl();
		this.templateFourData = templateService.findById(templateId);
		this.title = templateFourData.getCode();
	}
	
	public String getSettingmenuLabel(List<SettingData> settingList , long key)
	{
		String res = "";
		
		for (SettingData settingData : settingList) 
		{
			if(key == settingData.getId())
			{
				res = settingData.getT1();
				break;
			}
				
		}
		return res;
	}
	


	
	
	public String register()
	{
		HttpSession session = SessionUtil.getSession();
		templateFourData.setCreatedUserName((String) session.getAttribute("userid"));
		templateFourData.setModifiedUserName((String) session.getAttribute("userid"));
		if(templateService.validateTemplateDataUpdate(templateId, templateOnecode))
		{
			if(templateService.updateData(templateFourData))
			{	
				FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateParentid",  String.valueOf(templateFourData.getParentid()));	
				FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateTitle",  templateFourData.getCode());
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "INFO!", "Template Four Update Successfully!"));
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				return "template_four_search";
			}
			else
			{
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "ERROR!", "Setting's Codes are duplicate!"));
				return null;
			}
		}else
		{
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "ERROR!", "Codes must not be duplicate!"));
			return null;
		}		
	}
	
	public String back()
	{	
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateTitle",  templateFourData.getCode());
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateParentid", String.valueOf(templateFourData.getParentid()));		
		return "template_four_search";
	}
	
}
