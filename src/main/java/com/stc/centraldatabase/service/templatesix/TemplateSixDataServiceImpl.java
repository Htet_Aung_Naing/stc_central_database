package com.stc.centraldatabase.service.templatesix;

import com.stc.centraldatabase.mgr.templatesix.TemplateSixMgr;
import com.stc.centraldatabase.model.templateone.TemplateSearchData;
import com.stc.centraldatabase.model.templatesix.TemplateSixData;
import com.stc.centraldatabase.model.templatesix.TemplateSixSearchPaginateData;

public class TemplateSixDataServiceImpl implements TemplateSixDataService{

	@Override
	public boolean insertTemplateSixData(TemplateSixData activity) {
		return TemplateSixMgr.inserData(activity);
	}

	@Override
	public boolean updateData(TemplateSixData activity) {
		return TemplateSixMgr.updateData(activity);
	}

	@Override
	public TemplateSixSearchPaginateData find(TemplateSearchData settingSearch) {
		return TemplateSixMgr.find(settingSearch);
	}

	@Override
	public TemplateSixData findById(String id) {
		return TemplateSixMgr.findById(id);
	}

	@Override
	public boolean validateTemplateDataUpdate(String id, String code) {
		return TemplateSixMgr.validateUpdate(id, code);
	}

	@Override
	public boolean delete(String activityId) {
		return TemplateSixMgr.delete(activityId);
	}

}
