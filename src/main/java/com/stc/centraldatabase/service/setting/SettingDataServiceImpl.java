package com.stc.centraldatabase.service.setting;

import java.util.List;

import com.stc.centraldatabase.mgr.setting.SettingDataMgr;
import com.stc.centraldatabase.model.setting.SettingData;
import com.stc.centraldatabase.model.setting.SettingSearchData;
import com.stc.centraldatabase.model.setting.SettingSearchPaginateData;

public class SettingDataServiceImpl implements SettingDataService{

	@Override
	public boolean insertSettingData(SettingData setting , String tbname) {
		
		return SettingDataMgr.InsertSetting(setting,tbname);
	}

	@Override
	public SettingSearchPaginateData find(SettingSearchData settingSearch , String tbname) {
		return SettingDataMgr.find(settingSearch , tbname);
	}

	@Override
	public boolean delete(String id, String tbname) {
		return SettingDataMgr.delete(id, tbname);
	}

	@Override
	public SettingData findByid(String id, String tbname) {
		return SettingDataMgr.findById(id, tbname);
	}

	@Override
	public boolean update(SettingData setting, String tbname) {
		return SettingDataMgr.update(setting, tbname);
	}

	@Override
	public SettingData findByCode(String code, String tbname) {
		return SettingDataMgr.findByCode(code, tbname);
	}

	@Override
	public List<SettingData> getSettingList(String tbname) {
		return SettingDataMgr.getSettingList(tbname);
	}

	@Override
	public boolean validateSettingUpdate(String id, String code , String tbname) {
		return SettingDataMgr.validateSettingUpdate(id, code, tbname);
	}

}
