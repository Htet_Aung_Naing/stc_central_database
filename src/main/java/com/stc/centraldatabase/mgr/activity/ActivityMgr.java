package com.stc.centraldatabase.mgr.activity;

import com.stc.centraldatabase.dao.activity.ActivityDao;
import com.stc.centraldatabase.dao.activity.ActivitySearchDao;
import com.stc.centraldatabase.model.activity.ActivityData;
import com.stc.centraldatabase.model.activity.ActivitySearchData;
import com.stc.centraldatabase.model.activity.ActivitySearchPaginateData;
import com.stc.centraldatatbase.util.Result;

public class ActivityMgr {
	
	public static Result inserData(ActivityData activity)
	{
		ActivityDao activityDao = new ActivityDao();
		return activityDao.insertActionData(activity);
	}
	
	public static boolean updateData(ActivityData activity)
	{
		ActivityDao activityDao = new ActivityDao();
		return activityDao.updateActivity(activity);
	}
	
	public static ActivitySearchPaginateData find(ActivitySearchData settingSearch)
	{
		ActivitySearchDao settingSearchDao = new ActivitySearchDao();
		return settingSearchDao.find(settingSearch);
	}
	
	public static ActivityData findById(String id)
	{
		ActivitySearchDao activitySearchDao = new ActivitySearchDao();
		return activitySearchDao.findById(id);
	}
	
	public static boolean validateUpdate(String id,String code)
	{
		ActivitySearchDao activitySearchDao = new ActivitySearchDao();
		return activitySearchDao.validateSettingUpdate(id, code);
	}

	public static boolean delete(String id)
	{
		ActivityDao activityDao = new ActivityDao();
		return activityDao.delete(id);
	}
	
}
