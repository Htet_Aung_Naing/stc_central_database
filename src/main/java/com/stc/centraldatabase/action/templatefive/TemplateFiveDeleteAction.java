package com.stc.centraldatabase.action.templatefive;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.stc.centraldatabase.model.templatefive.TemplateFiveData;
import com.stc.centraldatabase.service.templatefive.TemplateFiveDataService;
import com.stc.centraldatabase.service.templatefive.TemplateFiveDataServiceImpl;



@ManagedBean(name = "templateFiveDeleteAction")
@ViewScoped
public class TemplateFiveDeleteAction implements Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = -425655372664387173L;

	TemplateFiveDataService templateService;
	String title;
	DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
	String templateId;
	String templateOnecode= "";

	

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	

	@ManagedProperty(value = "#{templatefive}")
	TemplateFiveData templateFiveData;
	

	public TemplateFiveData getTemplateFiveData() {
		return templateFiveData;
	}

	public void setTemplateFiveData(TemplateFiveData templateFiveData) {
		this.templateFiveData = templateFiveData;
	}

	@PostConstruct
	public void init()  
	{
		this.templateFiveData = new TemplateFiveData();
		
		this.templateId = (String) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("templateid");
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateParentid", templateId );
		this.templateService = new TemplateFiveDataServiceImpl();
		this.templateFiveData = templateService.findById(templateId);
		this.title = templateFiveData.getCode()+" Delete";
	}
	
	public String delete()
	{
		
		if(templateService.delete(templateId))
		{
			FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateParentid",  String.valueOf(templateFiveData.getParentid()));	
			FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateTitle",  templateFiveData.getCode());
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "INFO!", "Delete Template Six Data Successfully!"));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			return "template_five_search";
		}
		else
		{
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "ERROR!", "Setting Data cannot delete!"));
			return null;
		}
		
	}
	
	public String back()
	{	
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateTitle",  templateFiveData.getCode());
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateParentid", String.valueOf(templateFiveData.getParentid()));		
		return "template_five_search";
	}
	
	
	
}
