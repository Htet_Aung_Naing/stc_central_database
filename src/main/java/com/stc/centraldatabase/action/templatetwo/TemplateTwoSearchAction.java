package com.stc.centraldatabase.action.templatetwo;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.component.datatable.DataTable;

import com.stc.centraldatabase.model.templateone.TemplateSearchData;
import com.stc.centraldatabase.model.templatetwo.LazyTemplateTwoDataModel;
import com.stc.centraldatabase.service.templatetwo.TemplateTwoDataService;
import com.stc.centraldatabase.service.templatetwo.TemplateTwoDataServiceImpl;
import com.stc.centraldatatbase.util.CommonEnum;





@ManagedBean(name = "templateTwoSearchAction")
@ViewScoped
public class TemplateTwoSearchAction implements Serializable{


	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6499344160282589714L;
	
	TemplateSearchData templateSearchData;
	String title;
	String parentid ;
	LazyTemplateTwoDataModel settingdataModel;
	TemplateTwoDataService settingService;



	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public TemplateSearchData getTemplateSearchData() {
		return templateSearchData;
	}

	public void setTemplateSearchData(TemplateSearchData templateSearchData) {
		this.templateSearchData = templateSearchData;
	}


	public LazyTemplateTwoDataModel getSettingdataModel() {
		return settingdataModel;
	}

	public void setSettingdataModel(LazyTemplateTwoDataModel settingdataModel) {
		this.settingdataModel = settingdataModel;
	}

	public void reset(String targetDataTable) {
		resetPagination(targetDataTable);
		LoadData();
	}

	
	private void resetPagination(String targetDataTable) {
		DataTable d = (DataTable) FacesContext.getCurrentInstance().getViewRoot()
				.findComponent("settingGoalSearchForm:settingSearchTable");
		d.setFirst(0);
	}
	
	public void search(String targetDataTable) {

		// Check Joined Date From should be earlier than Joined Date To.
			// Reset pagination and do the lazy data model process.
			resetPagination(targetDataTable);
			settingdataModel = new LazyTemplateTwoDataModel(templateSearchData);
		
	}
	
	public String getTemplatePage(long id)
	{
		String res = "";
		for(CommonEnum.Template_Page g : CommonEnum.Template_Page.values())
		{
			if(g.value() == id)
			{
				res = g.description();
				break;
			}
			
		}
		return res;
	}
	
	@PostConstruct
	public void LoadData()
	{
		settingService = new TemplateTwoDataServiceImpl();
		templateSearchData = new TemplateSearchData();
		if(parentid == null)
			this.parentid = (String) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("templateParentid");
		if(title == null)
			this.title = (String) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("templateTitle");
		templateSearchData.setParentid(parentid);
		settingdataModel = new LazyTemplateTwoDataModel(templateSearchData);
		
	}
	
	public String edit(String id , String code) {
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateid", id);
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateCode", code);
		return "template_two_update";
	}



	public String delete(String id) {
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateid", id);
		
		return "template_two_delete";
	}
	
	public String detail(String id) {
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateid", id);
		return "template_two_detail";
	}
	
	
	

}
