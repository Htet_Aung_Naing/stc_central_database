package com.stc.centraldatabase.model.templatethree;

import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.stc.centraldatabase.model.templateone.TemplateSearchData;
import com.stc.centraldatabase.service.templatethree.TemplateThreeDataService;
import com.stc.centraldatabase.service.templatethree.TemplateThreeDataServiceImpl;






public class LazyTemplateThreeDataModel extends LazyDataModel<TemplateThreeData>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7167457755325524229L;
	/**
	 * 
	 */


	
	TemplateSearchData settingSearchData;
	TemplateThreeDataService settingService;
	String tbname = "";


	

	public TemplateSearchData getSettingSearchData() {
		return settingSearchData;
	}

	public void setSettingSearchData(TemplateSearchData settingSearchData) {
		this.settingSearchData = settingSearchData;
	}

	public  LazyTemplateThreeDataModel(TemplateSearchData settingSearchData) {
		this.settingSearchData = settingSearchData;
	}

	@Override
	public List<TemplateThreeData> load(int first, int pageSize, String sortField,
			SortOrder sortOrder, Map<String, Object> filters) {
		settingService = new TemplateThreeDataServiceImpl();
		
		settingSearchData.setOffset(first);
		settingSearchData.setLimit(pageSize);
		
		TemplateThreeSearchPaginateData userPaginateData = settingService.find(settingSearchData);
		
		setRowCount(userPaginateData.getCount());
		setPageSize(settingSearchData.getLimit());
		
		List<TemplateThreeData> userList = userPaginateData.getTemplateOneDataList();
		
		return userList;
	}

}
