package com.stc.centraldatabase.service.templatefour;

import com.stc.centraldatabase.model.templatefour.TemplateFourData;
import com.stc.centraldatabase.model.templatefour.TemplateFourSearchPaginateData;
import com.stc.centraldatabase.model.templateone.TemplateSearchData;

public interface TemplateFourDataService {
	
	
boolean insertTemplateFourData(TemplateFourData activity);
	
	boolean updateData(TemplateFourData activity);
	
	TemplateFourSearchPaginateData find(TemplateSearchData settingSearch );
	
	TemplateFourData findById(String id);
	
	boolean validateTemplateDataUpdate(String id, String code);
	
	boolean delete(String activityId);

}
