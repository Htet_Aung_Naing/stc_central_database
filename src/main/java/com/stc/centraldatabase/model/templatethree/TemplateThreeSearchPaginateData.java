package com.stc.centraldatabase.model.templatethree;

import java.io.Serializable;
import java.util.List;

public class TemplateThreeSearchPaginateData implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8793922769046576099L;
	
	int count;
	List<TemplateThreeData> templateOneDataList;
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public List<TemplateThreeData> getTemplateOneDataList() {
		return templateOneDataList;
	}
	public void setTemplateOneDataList(List<TemplateThreeData> templateOneDataList) {
		this.templateOneDataList = templateOneDataList;
	}
	
	

}
