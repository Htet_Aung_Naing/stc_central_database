package com.stc.centraldatabase.action.setting;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import com.stc.centraldatabase.model.setting.SettingData;
import com.stc.centraldatabase.service.setting.SettingDataService;
import com.stc.centraldatabase.service.setting.SettingDataServiceImpl;
import com.stc.centraldatatbase.util.SessionUtil;



@ManagedBean(name = "settingGoalUpadatAction")
@ViewScoped
public class SettingGoalUpdateAction implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3356441403577889990L;
	
	String settingID;
	SettingData settingData;
	String tbname = "";


	public SettingData getSettingData() {
		return settingData;
	}
	public void setSettingData(SettingData settingData) {
		this.settingData = settingData;
	}

	SettingDataService settingService;

	public String getSettingID() {
		return settingID;
	}
	public void setSettingID(String settingID) {
		this.settingID = settingID;
	}


	
	@PostConstruct
	public void init()
	{

			settingData = new SettingData();
			settingService = new SettingDataServiceImpl();
			if(settingID == null)
			{
				settingID = (String) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("impact_id");
				settingData = settingService.findByid(settingID , "setting_goal");
			}

		
	}
	
	public String update()
	{
		HttpSession session = SessionUtil.getSession();
		settingData.setModifiedUserName((String) session.getAttribute("userid"));
		session.setAttribute("update_success_title", "Impact Update Successfully!");
		
		if(settingService.validateSettingUpdate(settingID, settingData.getT1(), tbname))
		{
			if(settingService.update(settingData, "setting_goal")) 
			{	
				FacesContext.getCurrentInstance().getExternalContext().getFlash().put("settingUpdateData", settingData);
				return "settingUpdateSuccess";
			}
				
			else
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "ERROR!", "Cannot Update!"));
		}else
		{
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "ERROR!", "Code must not duplicate!"));
		}
		
		return null;
	}
	

}
