package com.stc.centraldatabase.service.templatetwo;

import com.stc.centraldatabase.mgr.templatetwo.TemplateTwoMgr;
import com.stc.centraldatabase.model.templateone.TemplateSearchData;
import com.stc.centraldatabase.model.templatetwo.TemplateTwoData;
import com.stc.centraldatabase.model.templatetwo.TemplateTwoSearchPaginateData;

public class TemplateTwoDataServiceImpl implements TemplateTwoDataService{

	@Override
	public boolean insertTemplateTwoData(TemplateTwoData activity) {
		return TemplateTwoMgr.inserData(activity);
	}

	@Override
	public boolean updateData(TemplateTwoData activity) {
		return TemplateTwoMgr.updateData(activity);
	}

	@Override
	public TemplateTwoSearchPaginateData find(TemplateSearchData settingSearch) {
		return TemplateTwoMgr.find(settingSearch);
	}

	@Override
	public TemplateTwoData findById(String id) {
		return TemplateTwoMgr.findById(id);
	}

	@Override
	public boolean validateTemplateDataUpdate(String id, String code) {
		return TemplateTwoMgr.validateUpdate(id, code);
	}

	@Override
	public boolean delete(String activityId) {
		return TemplateTwoMgr.delete(activityId);
	}

}
