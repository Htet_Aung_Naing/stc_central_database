package com.stc.centraldatabase.model.setting;

import java.io.Serializable;
import java.util.List;

public class SettingSearchPaginateData implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8793922769046576099L;
	
	int count;
	List<SettingData> SettingDataList;
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public List<SettingData> getSettingDataList() {
		return SettingDataList;
	}
	public void setSettingDataList(List<SettingData> SettingDataList) {
		this.SettingDataList = SettingDataList;
	}

}
