package com.stc.centraldatabase.model.activity;

import java.io.Serializable;

public class ActivitySearchData implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3061792254915542438L;
	
	int impactkey;
	int outcomekey;
	int outputkey;
	int templatekey;
	String code;
	String townshipCode;
	String townshipName;
	String villageCode;
	String villageName;
	int offset;
	int limit;
	
	
	public int getOffset() {
		return offset;
	}
	public void setOffset(int offset) {
		this.offset = offset;
	}
	public int getLimit() {
		return limit;
	}
	public void setLimit(int limit) {
		this.limit = limit;
	}
	public int getImpactkey() {
		return impactkey;
	}
	public void setImpactkey(int impactkey) {
		this.impactkey = impactkey;
	}
	public int getOutcomekey() {
		return outcomekey;
	}
	public void setOutcomekey(int outcomekey) {
		this.outcomekey = outcomekey;
	}
	public int getOutputkey() {
		return outputkey;
	}
	public void setOutputkey(int outputkey) {
		this.outputkey = outputkey;
	}
	public int getTemplatekey() {
		return templatekey;
	}
	public void setTemplatekey(int templatekey) {
		this.templatekey = templatekey;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getTownshipCode() {
		return townshipCode;
	}
	public void setTownshipCode(String townshipCode) {
		this.townshipCode = townshipCode;
	}
	public String getTownshipName() {
		return townshipName;
	}
	public void setTownshipName(String townshipName) {
		this.townshipName = townshipName;
	}
	public String getVillageCode() {
		return villageCode;
	}
	public void setVillageCode(String villageCode) {
		this.villageCode = villageCode;
	}
	public String getVillageName() {
		return villageName;
	}
	public void setVillageName(String villageName) {
		this.villageName = villageName;
	}
	
	public ActivitySearchData()
	{
		this.impactkey = 0;
		this.outcomekey = 0;
		this.outputkey = 0;
		this.templatekey = 0;
		this.townshipCode = "";
		this.townshipName = "";
		this.villageCode = "";
		this.villageName = "";
		this.code = "";
		this.offset = 0 ;
		this.limit = 0;
	}

}
