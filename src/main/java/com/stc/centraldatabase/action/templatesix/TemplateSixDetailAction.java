package com.stc.centraldatabase.action.templatesix;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.stc.centraldatabase.model.templatesix.TemplateSixData;
import com.stc.centraldatabase.service.templatesix.TemplateSixDataService;
import com.stc.centraldatabase.service.templatesix.TemplateSixDataServiceImpl;



@ManagedBean(name = "templatSixDetailAction")
@ViewScoped
public class TemplateSixDetailAction implements Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = -425655372664387173L;

	TemplateSixDataService templateService;
	String title;
	DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
	String templateId;
	String templateOnecode= "";

	

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	
	@ManagedProperty(value = "#{templatesix}")
	TemplateSixData templateSixData;
	

	

	public TemplateSixData getTemplateSixData() {
		return templateSixData;
	}

	public void setTemplateSixData(TemplateSixData templateSixData) {
		this.templateSixData = templateSixData;
	}

	@PostConstruct
	public void init()  
	{
		this.templateSixData = new TemplateSixData();
		
		this.templateId = (String) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("templateid");
		this.templateService = new TemplateSixDataServiceImpl();
		this.templateSixData = templateService.findById(templateId);		
		this.title = templateSixData.getCode()+" Detail";
	}
	
	public String back()
	{	
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateTitle",  templateSixData.getCode());
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateParentid", String.valueOf(templateSixData.getParentid()));		
		return "template_six_search";
	}
	
	
	
}
