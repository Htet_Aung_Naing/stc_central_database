package com.stc.centraldatabase.dao.templatetwo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.stc.centraldatabase.model.templateone.TemplateSearchData;
import com.stc.centraldatabase.model.templatetwo.TemplateTwoData;
import com.stc.centraldatabase.model.templatetwo.TemplateTwoSearchPaginateData;
import com.stc.centraldatatbase.util.DataBaseConnection;
import com.stc.centraldatatbase.util.PrjUtil;

public class TemplateTwoDataSearchDao 
{
	public  String getCriteria(TemplateSearchData criteria)
	{
		String filter = " where 1=1";
		
		if(!criteria.getCode().equals(""))
			
		{
			filter += " and code like '%"+criteria.getCode()+"%'";
			
		}
		
		if(!criteria.getDesc().equals(""))
			
		{
			filter += " and description like '%"+criteria.getDesc()+"%'";
			
		}
		if(!criteria.getParentid().equals(""))
		{
			filter += " and parent_id = "+criteria.getParentid();
		}
		if(criteria.getLimit() != 0)
			filter += " limit "+criteria.getOffset()+","+criteria.getLimit();

		return filter;
	}
	
	public static String getCountCriteria(TemplateSearchData criteria)
	{
		String filter = " where 1=1";
		
		if(!criteria.getCode().equals(""))
			
		{
			filter += " and code like '%"+criteria.getCode()+"%'";
			
		}
		
		if(!criteria.getDesc().equals(""))
			
		{
			filter += " and Description like '%"+criteria.getDesc()+"%'";
			
		}
		if(!criteria.getParentid().equals(""))
		{
			filter += " and parent_id = "+criteria.getParentid();
		}
		return filter;
	}
	
	public boolean validateSettingUpdate(String id,String code ) {
		PreparedStatement ps = null;
		try {
			Connection con = DataBaseConnection.getConnection();
			ps = con.prepareStatement("Select * from template_two where code = ? and parent_id <> ?");
			ps.setString(1, code);
			ps.setString(2, id);

			ResultSet rs = ps.executeQuery();

			if (rs.next()) 
			{
				return false;
			}
			else return true;
		} catch (SQLException ex) {
			System.out.println("Validation error -->" + ex.getMessage());
			return false;
		} 
		
	}
	
	public  int getTotalCount(TemplateSearchData search,Connection con)
	{
		int count = 0;
		String filter = getCountCriteria(search);
		String query = "Select count(*) as count from TEMPLATE_TWO "+ filter;
		Statement st;
		try {
			st = con.createStatement();
			ResultSet rs = st.executeQuery(query);
			if(rs.next())
				count = rs.getInt("count");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 return count;
	}
	
	public  TemplateTwoSearchPaginateData find(TemplateSearchData usearch)
	{
		TemplateTwoSearchPaginateData res = new TemplateTwoSearchPaginateData();
		String filter = getCriteria(usearch);
		List<TemplateTwoData> resList = new ArrayList<TemplateTwoData>();
		Connection con = null;
		TemplateTwoData setting;
		String query = "Select * from TEMPLATE_TWO" +filter;
		
		
		try {
			con = DataBaseConnection.getConnection();
			Statement st = con.createStatement();
			 ResultSet rs = st.executeQuery(query);
			 
			 while(rs.next())
			 {
				 setting = new TemplateTwoData();
				 setting.setId(rs.getInt("id"));
				 setting.setSettingDate(PrjUtil.StringtoDate(rs.getString("setting_date")));				 
				 setting.setCode(rs.getString("code"));
				 setting.setDesc(rs.getString("description"));
				 setting.setParentid(rs.getInt("parent_id"));
				 setting.setOscCommunityBoy(rs.getInt("OSC_COMMUNITY_BOY"));
				 setting.setOscCommunityGirl(rs.getInt("OSC_COMMUNITY_GIRL"));
				 setting.setOscCommunityDataSource(rs.getString("COMMUNITY_DATASOURCE"));
				 setting.setOscEnrollCmssBoy(rs.getInt("OSC_CMSS_BOY"));
				 setting.setOscEnrollCmssGirl(rs.getInt("OSC_CMSS_GIRL"));
				 setting.setOscEnrollCmssDatasource(rs.getString("CMSS_DATASOURCE"));
				 setting.setSettingDateInDateFormat(PrjUtil.datetimeTostring(rs.getString("setting_date")));
				 
				 resList.add(setting);
			 }
			
			res.setCount(getTotalCount(usearch,con));
			res.setTemplateTwoDataList(resList);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			DataBaseConnection.close(con);
		}
			
		return res;
	}
	
	
	public TemplateTwoData findById(String id)
	{	
		Connection con = null;
		TemplateTwoData setting = null;
		String query = "Select * from template_two where id="+id ;
		
		
		try {
			con = DataBaseConnection.getConnection();
			Statement st = con.createStatement();
			 ResultSet rs = st.executeQuery(query);
			 
			 while(rs.next())
			 {
				 setting = new TemplateTwoData();
				 setting.setId(rs.getInt("id"));
				 setting.setSettingDate(PrjUtil.StringtoDate(rs.getString("setting_date")));
				 setting.setParentid(rs.getInt("parent_id"));
				 setting.setSettingDate(PrjUtil.StringtoDate(rs.getString("setting_date")));				 
				 setting.setCode(rs.getString("code"));
				 setting.setDesc(rs.getString("description"));
				 setting.setParentid(rs.getInt("parent_id"));
				 setting.setOscCommunityBoy(rs.getInt("OSC_COMMUNITY_BOY"));
				 setting.setOscCommunityGirl(rs.getInt("OSC_COMMUNITY_GIRL"));
				 setting.setOscCommunityDataSource(rs.getString("COMMUNITY_DATASOURCE"));
				 setting.setOscEnrollCmssBoy(rs.getInt("OSC_CMSS_BOY"));
				 setting.setOscEnrollCmssGirl(rs.getInt("OSC_CMSS_GIRL"));
				 setting.setOscEnrollCmssDatasource(rs.getString("CMSS_DATASOURCE"));
				 setting.setSettingDateInDateFormat(PrjUtil.datetimeTostring(rs.getString("setting_date")));
				 setting.setPercentCommunityBoy(((double)setting.getOscCommunityBoy()/setting.getOscEnrollCmssBoy())*100);
				 setting.setPercentCommunityGirl(((double)setting.getOscCommunityGirl()/setting.getOscEnrollCmssGirl())*100);
				 setting.setToscCommunity(setting.getOscCommunityBoy() + setting.getOscCommunityGirl());
				 setting.setToscEnrollCmss(setting.getOscEnrollCmssBoy() + setting.getOscEnrollCmssGirl());
				 setting.setTpercent(setting.getPercentCommunityBoy() + setting.getPercentCommunityGirl());
			 }
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			DataBaseConnection.close(con);
		}
			
		 return setting;
	}
	
	
}
