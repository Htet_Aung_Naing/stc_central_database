package com.stc.centraldatabase.service.templatesix;

import com.stc.centraldatabase.model.templateone.TemplateSearchData;
import com.stc.centraldatabase.model.templatesix.TemplateSixData;
import com.stc.centraldatabase.model.templatesix.TemplateSixSearchPaginateData;

public interface TemplateSixDataService {
	
	
boolean insertTemplateSixData(TemplateSixData activity);
	
	boolean updateData(TemplateSixData activity);
	
	TemplateSixSearchPaginateData find(TemplateSearchData settingSearch );
	
	TemplateSixData findById(String id);
	
	boolean validateTemplateDataUpdate(String id, String code);
	
	boolean delete(String activityId);

}
