package com.stc.centraldatabase.action.activity;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.stc.centraldatabase.model.activity.ActivityData;




@ManagedBean(name = "activityUpdateCompleteAction")
@ViewScoped
public class ActivityUpdateCompleteAction implements Serializable {



	/**
	 * 
	 */
	private static final long serialVersionUID = 3496409674505241614L;
	ActivityData setting;
	
	
	

	public ActivityData getSetting() {
		return setting;
	}




	public void setSetting(ActivityData setting) {
		this.setting = setting;
	}




	@PostConstruct
	public void init() 
	{
		this.setting = (ActivityData) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("settingUpdateData");
		FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		
	}
}
