package com.stc.centraldatabase.action.setting;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import com.stc.centraldatabase.model.setting.SettingData;
import com.stc.centraldatabase.service.setting.SettingDataService;
import com.stc.centraldatabase.service.setting.SettingDataServiceImpl;
import com.stc.centraldatatbase.util.SessionUtil;



@ManagedBean(name = "settingOutputUpdateAction")
@ViewScoped
public class SettingOutputUpdateAction implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8960748410953236774L;

	SettingDataService settingService;
	DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
	List<SettingData> settingImpactList;
	List<SettingData> settingOutcomeList;
	String settingID;
	
	public List<SettingData> getSettingOutcomeList() {
		return settingOutcomeList;
	}

	public void setSettingOutcomeList(List<SettingData> settingOutcomeList) {
		this.settingOutcomeList = settingOutcomeList;
	}

	public List<SettingData> getSettingImpactList() {
		return settingImpactList;
	}

	public void setSettingImpactList(List<SettingData> settingImpactList) {
		this.settingImpactList = settingImpactList;
	}

	@ManagedProperty(value = "#{setting}")
	SettingData setting;
	
	


	public SettingData getSetting() {
		return setting;
	}

	public void setSetting(SettingData setting) {
		this.setting = setting;
	}

	@PostConstruct
	public void init()  
	{
		this.setting = new SettingData();	
		this.settingService  = new SettingDataServiceImpl();
		this.settingImpactList = settingService.getSettingList("setting_goal");
		this.settingOutcomeList = settingService.getSettingList("setting_outcome");
		settingID = (String) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("output_id");
		this.setting = settingService.findByid(settingID, "setting_output");

	}
	
	public String update()
	{
		HttpSession session = SessionUtil.getSession();
		setting.setModifiedUserName((String) session.getAttribute("userid"));
		session.setAttribute("update_success_title", "Output Update Successfully!");
		if(settingService.validateSettingUpdate(settingID, setting.getT1(), "setting_output"))
		{
			if(settingService.update(setting, "setting_output")) 
			{	
				FacesContext.getCurrentInstance().getExternalContext().getFlash().put("settingUpdateData", setting);
				return "settingUpdateSuccess";
			}
				
			else
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "ERROR!", "Cannot Update!"));
		}else
		{
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "ERROR!", "Code must not duplicate!"));
		}
		
		return null;
	}
	
}
