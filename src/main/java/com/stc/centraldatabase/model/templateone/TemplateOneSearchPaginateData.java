package com.stc.centraldatabase.model.templateone;

import java.io.Serializable;
import java.util.List;

public class TemplateOneSearchPaginateData implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8793922769046576099L;
	
	int count;
	List<TemplateOneData> templateOneDataList;
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public List<TemplateOneData> getTemplateOneDataList() {
		return templateOneDataList;
	}
	public void setTemplateOneDataList(List<TemplateOneData> templateOneDataList) {
		this.templateOneDataList = templateOneDataList;
	}
	
	

}
