package com.stc.centraldatabase.service.templateone;

import com.stc.centraldatabase.mgr.templateone.TemplateOneMgr;
import com.stc.centraldatabase.model.templateone.TemplateOneData;
import com.stc.centraldatabase.model.templateone.TemplateOneSearchPaginateData;
import com.stc.centraldatabase.model.templateone.TemplateSearchData;

public class TemplateOneDataServiceImpl implements TemplateOneDataService{

	@Override
	public boolean insertTemplateOneData(TemplateOneData activity) {
		return TemplateOneMgr.inserData(activity);
	}

	@Override
	public boolean updateData(TemplateOneData activity) {
		return TemplateOneMgr.updateData(activity);
	}

	@Override
	public TemplateOneSearchPaginateData find(TemplateSearchData settingSearch) {
		return TemplateOneMgr.find(settingSearch);
	}

	@Override
	public TemplateOneData findById(String id) {
		return TemplateOneMgr.findById(id);
	}

	@Override
	public boolean validateTemplateDataUpdate(String id, String code) {
		return TemplateOneMgr.validateUpdate(id, code);
	}

	@Override
	public boolean delete(String activityId) {
		return TemplateOneMgr.delete(activityId);
	}

}
