package com.stc.centraldatabase.model.templatefive;

import java.io.Serializable;
import java.util.List;

public class TemplateFiveSearchPaginateData implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8793922769046576099L;
	
	int count;
	List<TemplateFiveData> templatefiveDataList;
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public List<TemplateFiveData> getTemplatefiveDataList() {
		return templatefiveDataList;
	}
	public void setTemplatefiveDataList(List<TemplateFiveData> templatefiveDataList) {
		this.templatefiveDataList = templatefiveDataList;
	}

	
	

}
