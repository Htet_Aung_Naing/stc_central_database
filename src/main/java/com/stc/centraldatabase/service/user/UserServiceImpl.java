package com.stc.centraldatabase.service.user;

import java.util.List;

import com.stc.centraldatabase.dao.user.LoginDao;
import com.stc.centraldatabase.dao.user.UserDao;
import com.stc.centraldatabase.dao.user.UserRoleDao;
import com.stc.centraldatabase.dao.user.UserSearchDao;
import com.stc.centraldatabase.model.user.PasswordData;
import com.stc.centraldatabase.model.user.UserInfo;
import com.stc.centraldatabase.model.user.UserSearchData;
import com.stc.centraldatabase.model.user.UserSearchPaginateData;
import com.stc.centraldatatbase.util.SetupData;

public class UserServiceImpl implements UserService{

	@Override
	public UserInfo getUser(String id, String password) {
		return LoginDao.validate(id, password);
	}

	@Override
	public boolean userInsert(UserInfo user) {
		return UserDao.insert(user);
	}

	@Override
	public List<SetupData> getRoleData() {
		return UserRoleDao.getRoleList();
	}

	@Override
	public UserInfo getUserbyId(String userid) {
		return UserSearchDao.getUserById(userid);
	}

	@Override
	public boolean delete(String userid) {
		return UserDao.delete(userid);
	}

	@Override
	public boolean updatePassword(PasswordData pwd) {
		return UserDao.updatePassword(pwd);
	}

	@Override
	public boolean validateUserUpdate(String userid, long id) {
		return LoginDao.validateUserUpdate(userid, id);
	}

	@Override
	public boolean update(UserInfo user) {
		return UserDao.update(user);
	}

	@Override
	public UserSearchPaginateData find(UserSearchData userSearch) {
		return UserSearchDao.find(userSearch);
	}

}
