package com.stc.centraldatabase.action.templateone;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import com.stc.centraldatabase.model.setting.SettingData;
import com.stc.centraldatabase.model.templateone.TemplateOneData;
import com.stc.centraldatabase.service.templateone.TemplateOneDataService;
import com.stc.centraldatabase.service.templateone.TemplateOneDataServiceImpl;
import com.stc.centraldatatbase.util.SessionUtil;



@ManagedBean(name = "templateOneUpdateAction")
@ViewScoped
public class TemplateOneUpdateAction implements Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = -425655372664387173L;

	TemplateOneDataService templateService;
	String title;
	DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
	String templateId;
	String templateOnecode= "";

	

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public TemplateOneData getTemplateOneData() {
		return templateOneData;
	}

	public void setTemplateOneData(TemplateOneData templateOneData) {
		this.templateOneData = templateOneData;
	}


	@ManagedProperty(value = "#{templateone}")
	TemplateOneData templateOneData;
	

	@PostConstruct
	public void init()  
	{
		this.templateOneData = new TemplateOneData();
		
		this.templateId = (String) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("templateid");
		this.templateOnecode = (String) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("templatCode");
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateParentid", templateId );
		this.templateService = new TemplateOneDataServiceImpl();
		this.templateOneData = templateService.findById(templateId);
		this.title = templateOneData.getCode();
	}
	
	public String getSettingmenuLabel(List<SettingData> settingList , long key)
	{
		String res = "";
		
		for (SettingData settingData : settingList) 
		{
			if(key == settingData.getId())
			{
				res = settingData.getT1();
				break;
			}
				
		}
		return res;
	}
	


	
	
	public String register()
	{
		HttpSession session = SessionUtil.getSession();
		templateOneData.setCreatedUserName((String) session.getAttribute("userid"));
		templateOneData.setModifiedUserName((String) session.getAttribute("userid"));
		if(templateService.validateTemplateDataUpdate(templateId, templateOnecode))
		{
			if(templateService.updateData(templateOneData))
			{	
				FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateParentid",  String.valueOf(templateOneData.getParentid()));	
				FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateTitle",  templateOneData.getCode());
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "INFO!", "Template One Update Successfully!"));
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				return "template_one_search";
			}
			else
			{
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "ERROR!", "Setting's Codes are duplicate!"));
				return null;
			}
		}else
		{
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "ERROR!", "Codes must not be duplicate!"));
			return null;
		}		
	}
	
	public String back()
	{	
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateTitle",  templateOneData.getCode());
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateParentid", String.valueOf(templateOneData.getParentid()));		
		return "template_one_search";
	}
	
}
