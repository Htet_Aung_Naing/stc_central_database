package com.stc.centraldatabase.service.templatefive;

import com.stc.centraldatabase.mgr.templatefive.TemplateFiveMgr;
import com.stc.centraldatabase.model.templatefive.TemplateFiveData;
import com.stc.centraldatabase.model.templatefive.TemplateFiveSearchPaginateData;
import com.stc.centraldatabase.model.templateone.TemplateSearchData;

public class TemplateFiveDataServiceImpl implements TemplateFiveDataService{

	@Override
	public boolean insertTemplateFiveData(TemplateFiveData activity) {
		return TemplateFiveMgr.inserData(activity);
	}

	@Override
	public boolean updateData(TemplateFiveData activity) {
		return TemplateFiveMgr.updateData(activity);
	}

	@Override
	public TemplateFiveSearchPaginateData find(TemplateSearchData settingSearch) {
		return TemplateFiveMgr.find(settingSearch);
	}

	@Override
	public TemplateFiveData findById(String id) {
		return TemplateFiveMgr.findById(id);
	}

	@Override
	public boolean validateTemplateDataUpdate(String id, String code) {
		return TemplateFiveMgr.validateUpdate(id, code);
	}

	@Override
	public boolean delete(String activityId) {
		return TemplateFiveMgr.delete(activityId);
	}

}
