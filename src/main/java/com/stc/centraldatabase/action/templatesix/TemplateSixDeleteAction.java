package com.stc.centraldatabase.action.templatesix;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.stc.centraldatabase.model.templatesix.TemplateSixData;
import com.stc.centraldatabase.service.templatesix.TemplateSixDataService;
import com.stc.centraldatabase.service.templatesix.TemplateSixDataServiceImpl;



@ManagedBean(name = "templateSixDeleteAction")
@ViewScoped
public class TemplateSixDeleteAction implements Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = -425655372664387173L;

	TemplateSixDataService templateService;
	String title;
	DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
	String templateId;
	String templateOnecode= "";

	

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	

	@ManagedProperty(value = "#{templatesix}")
	TemplateSixData templateSixData;
	

	public TemplateSixData getTemplateSixData() {
		return templateSixData;
	}

	public void setTemplateSixData(TemplateSixData templateSixData) {
		this.templateSixData = templateSixData;
	}

	@PostConstruct
	public void init()  
	{
		this.templateSixData = new TemplateSixData();
		
		this.templateId = (String) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("templateid");
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateParentid", templateId );
		this.templateService = new TemplateSixDataServiceImpl();
		this.templateSixData = templateService.findById(templateId);
		this.title = templateSixData.getCode()+" Delete";
	}
	
	public String delete()
	{
		
		if(templateService.delete(templateId))
		{
			FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateParentid",  String.valueOf(templateSixData.getParentid()));	
			FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateTitle",  templateSixData.getCode());
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "INFO!", "Delete Template Six Data Successfully!"));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			return "template_six_search";
		}
		else
		{
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "ERROR!", "Setting Data cannot delete!"));
			return null;
		}
		
	}
	
	public String back()
	{	
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateTitle",  templateSixData.getCode());
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateParentid", String.valueOf(templateSixData.getParentid()));		
		return "template_six_search";
	}
	
	
	
}
