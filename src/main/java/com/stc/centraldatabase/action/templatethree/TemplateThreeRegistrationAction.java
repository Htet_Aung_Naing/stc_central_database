package com.stc.centraldatabase.action.templatethree;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import com.stc.centraldatabase.model.setting.SettingData;
import com.stc.centraldatabase.model.templatethree.TemplateThreeData;
import com.stc.centraldatabase.service.templatethree.TemplateThreeDataService;
import com.stc.centraldatabase.service.templatethree.TemplateThreeDataServiceImpl;
import com.stc.centraldatatbase.util.SessionUtil;



@ManagedBean(name = "templateThreeRegistrationAction")
@ViewScoped
public class TemplateThreeRegistrationAction implements Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = -425655372664387173L;

	TemplateThreeDataService templateService;
	String title;
	String parentid;
	DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
	boolean isPopup = false;
	

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	





	public TemplateThreeData getTemplateThreeData() {
		return templateThreeData;
	}

	public void setTemplateThreeData(TemplateThreeData templateThreeData) {
		this.templateThreeData = templateThreeData;
	}







	@ManagedProperty(value = "#{templatethree}")
	TemplateThreeData templateThreeData;
	
	


	


	@PostConstruct
	public void init()  
	{
		this.templateThreeData = new TemplateThreeData();	
		if(FacesContext.getCurrentInstance().getExternalContext().getFlash().get("templateParentid") != null)
		{
			this.parentid = (String) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("templateParentid");
			this.title = (String) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("templateTitle");
		}else
		{
			HttpSession session = SessionUtil.getSession();
			this.title = session.getAttribute("templateTitle").toString();
			this.parentid = session.getAttribute("templateParentid").toString();
			isPopup = true;
		}
		
		this.templateService = new TemplateThreeDataServiceImpl();
		
	}
	
	public String getSettingmenuLabel(List<SettingData> settingList , long key)
	{
		String res = "";
		
		for (SettingData settingData : settingList) 
		{
			if(key == settingData.getId())
			{
				res = settingData.getT1();
				break;
			}
				
		}
		return res;
	}
	


	
	
	public String register()
	{
		HttpSession session = SessionUtil.getSession();
		templateThreeData.setCreatedUserName((String) session.getAttribute("userid"));
		templateThreeData.setModifiedUserName((String) session.getAttribute("userid"));
		templateThreeData.setParentid(Integer.parseInt(parentid));
		if(templateService.insertTemplateOneData(templateThreeData))
		{	
			
			if(isPopup)
			{
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "INFO!", "Template Three Register Successfully!"));
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				return "template_three_popup";
			}				
			else
			{
				FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateParentid",  String.valueOf(templateThreeData.getParentid()));		
				FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateTitle",  templateThreeData.getCode());
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "INFO!", "Template Three Register Successfully!"));
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				return "template_three_registration";
			}
				
		}			
		else
		{
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "ERROR!", "Setting's Codes are duplicate!"));
			return null;
		}
	}
	
	public String back()
	{	
		
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateParentid", String.valueOf(templateThreeData.getParentid()));		
		return "activitySearch";
	}
	
}
