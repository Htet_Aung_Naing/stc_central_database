package com.stc.centraldatabase.action.templatetwo;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.stc.centraldatabase.model.templatetwo.TemplateTwoData;
import com.stc.centraldatabase.service.templatetwo.TemplateTwoDataService;
import com.stc.centraldatabase.service.templatetwo.TemplateTwoDataServiceImpl;



@ManagedBean(name = "templateTwoDeleteAction")
@ViewScoped
public class TemplateTwoDeleteAction implements Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = -425655372664387173L;

	TemplateTwoDataService templateService;
	String title;
	DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
	String templateId;
	String templateOnecode= "";

	

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	

	@ManagedProperty(value = "#{templatesix}")
	TemplateTwoData templateTwoData;
	

	public TemplateTwoData getTemplateTwoData() {
		return templateTwoData;
	}

	public void setTemplateTwoData(TemplateTwoData templateTwoData) {
		this.templateTwoData = templateTwoData;
	}

	@PostConstruct
	public void init()  
	{
		this.templateTwoData = new TemplateTwoData();
		
		this.templateId = (String) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("templateid");
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateParentid", templateId );
		this.templateService = new TemplateTwoDataServiceImpl();
		this.templateTwoData = templateService.findById(templateId);
		this.title = templateTwoData.getCode()+" Delete";
	}
	
	public String delete()
	{
		
		if(templateService.delete(templateId))
		{
			FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateParentid",  String.valueOf(templateTwoData.getParentid()));	
			FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateTitle",  templateTwoData.getCode());
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "INFO!", "Delete Template Two Data Successfully!"));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			return "template_two_search";
		}
		else
		{
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "ERROR!", "Setting Data cannot delete!"));
			return null;
		}
		
	}
	
	public String back()
	{	
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateTitle",  templateTwoData.getCode());
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateParentid", String.valueOf(templateTwoData.getParentid()));		
		return "template_two_search";
	}
	
	
	
}
