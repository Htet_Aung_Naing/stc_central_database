package com.stc.centraldatabase.dao.templatefive;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Date;

import com.stc.centraldatabase.model.templatefive.TemplateFiveData;
import com.stc.centraldatatbase.util.DataBaseConnection;
import com.stc.centraldatatbase.util.PrjUtil;

public class TemplateFiveDataDao {
	
	public boolean insertTemplateFiveData(TemplateFiveData setting)
	{
		Connection con = null;
		PreparedStatement ps = null;
		

		 	try 
		 	{
				String query;
				
				if(setting != null)
				{
					con = DataBaseConnection.getConnection();
				     query = "INSERT INTO TEMPLATE_FIVE ( PARENT_ID, CREATED_USER, CREATED_DATE, MODIFIED_USER_NAME, "
				     		+ "MODIFIED_DATE, CODE, DESCRIPTION, SETTING_DATE, DIRECT_MALE, DIRECT_FEMALE, "
				     		+ "DIRECT_ADULT_MALE, DIRECT_ADULT_FEMALE, SCI_FUND, COMMUNITY_FUND)"
				     		+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?)";
				     
				     
				     int i = 1;
				     ps = con.prepareStatement(query);
				     ps.setInt(i++, setting.getParentid());
				     ps.setString(i++, setting.getCreatedUserName());
				     ps.setString(i++, PrjUtil.stringToDate(new Date()));
				     ps.setString(i++, setting.getModifiedUserName());
				     ps.setString(i++, PrjUtil.stringToDate(new Date()));			    
				     ps.setString(i++, setting.getCode());
				     ps.setString(i++, setting.getDesc() );
				     ps.setString(i++, PrjUtil.stringToDate(setting.getSettingDate()));
				     ps.setInt(i++, setting.getDirectMale());
				     ps.setInt(i++, setting.getDirectFemale());
				     ps.setInt(i++, setting.getDirectAdultMale());
				     ps.setInt(i++, setting.getDirectAdultFemale());
				     ps.setLong(i++, setting.getSciFund());
				     ps.setLong(i++, setting.getCommunityFund());
				     
				     ps.execute();
				     con.close();
				     return true; 
					   				  
				}else
				{
				
					return false;
				}
				 
			} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
	
			return false;
			
		}
				
		}
	
	public  boolean updateTemplateFiveData(TemplateFiveData setting) 
	{
		Connection con = null;
		PreparedStatement ps = null;
		String query;
		try {
			con = DataBaseConnection.getConnection();
			
		
	     query = "UPDATE TEMPLATE_FIVE SET MODIFIED_USER_NAME = ?,"
				  + "MODIFIED_DATE = ?, CODE = ?, DESCRIPTION = ?, SETTING_DATE = ?, DIRECT_MALE = ?, DIRECT_FEMALE = ?"     
				  + ",DIRECT_ADULT_MALE = ?, DIRECT_ADULT_FEMALE = ?, SCI_FUND = ?, COMMUNITY_FUND = ?"
			      + " WHERE ID = ?";
			     		
			    int i = 1;
				ps = con.prepareStatement(query);
				ps.setString(i++, setting.getModifiedUserName());
				ps.setString(i++, PrjUtil.stringToDate(new Date()));
			    ps.setString(i++, setting.getCode());
				ps.setString(i++, setting.getDesc());			
				ps.setString(i++, PrjUtil.stringToDate(setting.getSettingDate()));
				ps.setInt(i++, setting.getDirectMale());
				ps.setInt(i++, setting.getDirectFemale());
				ps.setInt(i++, setting.getDirectAdultMale());
				ps.setInt(i++, setting.getDirectAdultFemale());
				ps.setLong(i++, setting.getSciFund());
				ps.setLong(i++, setting.getCommunityFund());
				ps.setLong(i++, setting.getId());
				ps.execute();
			      
			    con.close();
			    return true;
			
			
			
		} catch (SQLException ex) {
			System.out.println("Templage Five Data Registration Update error -->" + ex.getMessage());
			return false;
		} finally {
			DataBaseConnection.close(con);
		}
	}
	
	public  boolean delete(String settingid) {
		Connection con = null;
		PreparedStatement ps = null;
		String query;
		try {
			con = DataBaseConnection.getConnection();
			    	        
			     query = "DELETE FROM TEMPLATE_FIVE where id=?";
			    int i = 1;
				ps = con.prepareStatement(query);
				ps.setString(i++, settingid);
				ps.execute();
			      
			    con.close();
			    return true;
			
			
			
		} catch (SQLException ex) {
			System.out.println("Template five Delete error -->" + ex.getMessage());
			return false;
		} finally {
			DataBaseConnection.close(con);
		}
	}

}
