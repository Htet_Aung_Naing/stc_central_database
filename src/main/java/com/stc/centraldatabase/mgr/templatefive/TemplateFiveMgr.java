package com.stc.centraldatabase.mgr.templatefive;



import com.stc.centraldatabase.dao.templatefive.TemplateFiveDataDao;
import com.stc.centraldatabase.dao.templatefive.TemplateFiveDataSearchDao;
import com.stc.centraldatabase.model.templatefive.TemplateFiveData;
import com.stc.centraldatabase.model.templatefive.TemplateFiveSearchPaginateData;
import com.stc.centraldatabase.model.templateone.TemplateSearchData;

public class TemplateFiveMgr 
{
	public static boolean inserData(TemplateFiveData template)
	{
		TemplateFiveDataDao templateFiveDao = new TemplateFiveDataDao();
		return templateFiveDao.insertTemplateFiveData(template);
	}
	
	public static boolean updateData(TemplateFiveData activity)
	{
		TemplateFiveDataDao templateFiveDao = new TemplateFiveDataDao();
		return templateFiveDao.updateTemplateFiveData(activity);
	}
	
	public static TemplateFiveSearchPaginateData find(TemplateSearchData settingSearch)
	{
		TemplateFiveDataSearchDao settingSearchDao = new TemplateFiveDataSearchDao();
		return settingSearchDao.find(settingSearch);
	}
	
	public static TemplateFiveData findById(String id)
	{
		TemplateFiveDataSearchDao activitySearchDao = new TemplateFiveDataSearchDao();
		return activitySearchDao.findById(id);
	}
	
	public static boolean validateUpdate(String id,String code)
	{
		TemplateFiveDataSearchDao activitySearchDao = new TemplateFiveDataSearchDao();
		return activitySearchDao.validateSettingUpdate(id, code);
	}

	public static boolean delete(String id)
	{
		TemplateFiveDataDao templateFiveDao = new TemplateFiveDataDao();
		return templateFiveDao.delete(id);
	}
}
