package com.stc.centraldatabase.mgr.templatefour;



import com.stc.centraldatabase.dao.templatefour.TemplateFourDataDao;
import com.stc.centraldatabase.dao.templatefour.TemplateFourDataSearchDao;
import com.stc.centraldatabase.model.templatefour.TemplateFourData;
import com.stc.centraldatabase.model.templatefour.TemplateFourSearchPaginateData;
import com.stc.centraldatabase.model.templateone.TemplateSearchData;

public class TemplateFourMgr 
{
	public static boolean inserData(TemplateFourData template)
	{
		TemplateFourDataDao templateFiveDao = new TemplateFourDataDao();
		return templateFiveDao.insertTemplateFourData(template);
	}
	
	public static boolean updateData(TemplateFourData activity)
	{
		TemplateFourDataDao templateFiveDao = new TemplateFourDataDao();
		return templateFiveDao.updateTemplateFourData(activity);
	}
	
	public static TemplateFourSearchPaginateData find(TemplateSearchData settingSearch)
	{
		TemplateFourDataSearchDao settingSearchDao = new TemplateFourDataSearchDao();
		return settingSearchDao.find(settingSearch);
	}
	
	public static TemplateFourData findById(String id)
	{
		TemplateFourDataSearchDao activitySearchDao = new TemplateFourDataSearchDao();
		return activitySearchDao.findById(id);
	}
	
	public static boolean validateUpdate(String id,String code)
	{
		TemplateFourDataSearchDao activitySearchDao = new TemplateFourDataSearchDao();
		return activitySearchDao.validateSettingUpdate(id, code);
	}

	public static boolean delete(String id)
	{
		TemplateFourDataDao templateFiveDao = new TemplateFourDataDao();
		return templateFiveDao.delete(id);
	}
}
