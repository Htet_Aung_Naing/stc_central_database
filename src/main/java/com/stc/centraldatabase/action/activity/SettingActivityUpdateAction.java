package com.stc.centraldatabase.action.activity;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import com.stc.centraldatabase.model.activity.ActivityData;
import com.stc.centraldatabase.model.setting.SettingData;
import com.stc.centraldatabase.service.activity.ActivityDataService;
import com.stc.centraldatabase.service.activity.ActivityDataServiceImpl;
import com.stc.centraldatabase.service.setting.SettingDataService;
import com.stc.centraldatabase.service.setting.SettingDataServiceImpl;
import com.stc.centraldatatbase.util.SessionUtil;
import com.stc.centraldatatbase.util.SetupData;



@ManagedBean(name = "settingActivityUpdateAction")
@ViewScoped
public class SettingActivityUpdateAction implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8960748410953236774L;

	SettingDataService settingService;
	ActivityDataService activityService;
	DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
	List<SettingData> settingImpactList;
	List<SettingData> settingOutcomeList;
	List<SettingData> settingOutputList;
	List<SetupData> templateList = new ArrayList<SetupData>();
	String settingID;
	
	public List<SettingData> getSettingOutcomeList() {
		return settingOutcomeList;
	}

	public void setSettingOutcomeList(List<SettingData> settingOutcomeList) {
		this.settingOutcomeList = settingOutcomeList;
	}

	public List<SettingData> getSettingImpactList() {
		return settingImpactList;
	}

	public List<SettingData> getSettingOutputList() {
		return settingOutputList;
	}

	public void setSettingOutputList(List<SettingData> settingOutputList) {
		this.settingOutputList = settingOutputList;
	}

	public List<SetupData> getTemplateList() {
		return templateList;
	}

	public void setTemplateList(List<SetupData> templateList) {
		this.templateList = templateList;
	}

	public void setSettingImpactList(List<SettingData> settingImpactList) {
		this.settingImpactList = settingImpactList;
	}

	@ManagedProperty(value = "#{setting}")
	ActivityData setting;
	
	


	
	
	public ActivityData getSetting() {
		return setting;
	}

	public void setSetting(ActivityData setting) {
		this.setting = setting;
	}

	public void addTemplateData()
	{
		SetupData setup = new SetupData();
		setup.setLabel("Template one");
		setup.setValue(1);
		templateList.add(setup);
		setup = new SetupData();
		setup.setLabel("Template two");
		setup.setValue(2);
		templateList.add(setup);
	}

	@PostConstruct
	public void init()  
	{
		this.setting = new ActivityData();	
		this.settingService  = new SettingDataServiceImpl();
		activityService = new ActivityDataServiceImpl();
		this.settingImpactList = settingService.getSettingList("setting_goal");
		this.settingOutcomeList = settingService.getSettingList("setting_outcome");
		this.settingOutputList = settingService.getSettingList("setting_output");
		addTemplateData();
		settingID = (String) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("activity_id");
		this.setting = activityService.findById(settingID);

	}
	
	public String update()
	{
		HttpSession session = SessionUtil.getSession();
		setting.setModifiedUserName((String) session.getAttribute("userid"));
		if(activityService.validateSettingUpdate(settingID, setting.getCode()))
		{
			if(activityService.updateData(setting)) 
			{	
				FacesContext.getCurrentInstance().getExternalContext().getFlash().put("settingUpdateData", setting);
				return "activityUpdateSuccess";
			}
				
			else
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "ERROR!", "Cannot Update!"));
		}else
		{
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "ERROR!", "Code must not duplicate!"));
		}
		
		return null;
	}
	
}
