package com.stc.centraldatabase.service.activity;

import com.stc.centraldatabase.model.activity.ActivityData;
import com.stc.centraldatabase.model.activity.ActivitySearchData;
import com.stc.centraldatabase.model.activity.ActivitySearchPaginateData;
import com.stc.centraldatatbase.util.Result;

public interface ActivityDataService {

	Result insertActivityData(ActivityData activity);
	
	boolean updateData(ActivityData activity);
	
	ActivitySearchPaginateData find(ActivitySearchData settingSearch );
	
	ActivityData findById(String id);
	
	boolean validateSettingUpdate(String id, String code);
	
	boolean delete(String activityId);
	
}
