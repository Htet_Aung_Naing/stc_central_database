package com.stc.centraldatabase.service.templatethree;

import com.stc.centraldatabase.mgr.templatethree.TemplateThreeMgr;
import com.stc.centraldatabase.model.templateone.TemplateSearchData;
import com.stc.centraldatabase.model.templatethree.TemplateThreeData;
import com.stc.centraldatabase.model.templatethree.TemplateThreeSearchPaginateData;

public class TemplateThreeDataServiceImpl implements TemplateThreeDataService{

	@Override
	public boolean insertTemplateOneData(TemplateThreeData activity) {
		return TemplateThreeMgr.inserData(activity);
	}

	@Override
	public boolean updateData(TemplateThreeData activity) {
		return TemplateThreeMgr.updateData(activity);
	}

	@Override
	public TemplateThreeSearchPaginateData find(TemplateSearchData settingSearch) {
		return TemplateThreeMgr.find(settingSearch);
	}

	@Override
	public TemplateThreeData findById(String id) {
		return TemplateThreeMgr.findById(id);
	}

	@Override
	public boolean validateTemplateDataUpdate(String id, String code) {
		return TemplateThreeMgr.validateUpdate(id, code);
	}

	@Override
	public boolean delete(String activityId) {
		return TemplateThreeMgr.delete(activityId);
	}

}
