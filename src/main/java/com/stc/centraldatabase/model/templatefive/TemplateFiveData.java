package com.stc.centraldatabase.model.templatefive;

import java.io.Serializable;
import java.util.Date;

public class TemplateFiveData implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1906394879722310958L;
	int id;
	int parentid;
	String modifiedDate;
	String createdUserName;
	String modifiedUserName;
	String createdDate;
	String code;
	String desc;
	Date settingDate;
	String settingDateInDateFormat;
	int directMale;
	int directFemale;
	int directAdultMale;
	int directAdultFemale;
	long sciFund;
	long communityFund;
	long totalFund;
	int tdirect;
	int tdirectAdult;
	
	public long getTotalFund() {
		return totalFund;
	}

	public void setTotalFund(long totalFund) {
		this.totalFund = totalFund;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getParentid() {
		return parentid;
	}

	public void setParentid(int parentid) {
		this.parentid = parentid;
	}

	public String getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getCreatedUserName() {
		return createdUserName;
	}

	public void setCreatedUserName(String createdUserName) {
		this.createdUserName = createdUserName;
	}

	public String getModifiedUserName() {
		return modifiedUserName;
	}

	public void setModifiedUserName(String modifiedUserName) {
		this.modifiedUserName = modifiedUserName;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public Date getSettingDate() {
		return settingDate;
	}

	public void setSettingDate(Date settingDate) {
		this.settingDate = settingDate;
	}

	public String getSettingDateInDateFormat() {
		return settingDateInDateFormat;
	}

	public void setSettingDateInDateFormat(String settingDateInDateFormat) {
		this.settingDateInDateFormat = settingDateInDateFormat;
	}

	public int getDirectMale() {
		return directMale;
	}

	public void setDirectMale(int directMale) {
		this.directMale = directMale;
	}

	public int getDirectFemale() {
		return directFemale;
	}

	public void setDirectFemale(int directFemale) {
		this.directFemale = directFemale;
	}

	public int getDirectAdultMale() {
		return directAdultMale;
	}

	public void setDirectAdultMale(int directAdultMale) {
		this.directAdultMale = directAdultMale;
	}

	public int getDirectAdultFemale() {
		return directAdultFemale;
	}

	public void setDirectAdultFemale(int directAdultFemale) {
		this.directAdultFemale = directAdultFemale;
	}

	public long getSciFund() {
		return sciFund;
	}

	public void setSciFund(long sciFund) {
		this.sciFund = sciFund;
	}

	public long getCommunityFund() {
		return communityFund;
	}

	public void setCommunityFund(long communityFund) {
		this.communityFund = communityFund;
	}

	public int getTdirect() {
		return tdirect;
	}

	public void setTdirect(int tdirect) {
		this.tdirect = tdirect;
	}

	public int getTdirectAdult() {
		return tdirectAdult;
	}

	public void setTdirectAdult(int tdirectAdult) {
		this.tdirectAdult = tdirectAdult;
	}

	public TemplateFiveData()
	{
		this.id = 0;
		this.parentid = 0;
		this.modifiedDate = "";
		this.createdUserName = "";
		this.modifiedDate = "";
		this.modifiedUserName = "";
		this.createdDate = "";
		this.code = "";
		this.desc = "";
		this.settingDate = new Date();
		this.settingDateInDateFormat = "";
		this.directMale = 0;
		this.directFemale = 0;
		this.directAdultMale = 0;
		this.directAdultFemale = 0;
		this.sciFund =0 ;
		this.communityFund = 0;
		this.totalFund = 0;
		this.tdirect = 0 ;
		this.tdirectAdult = 0;
	}
	
}
