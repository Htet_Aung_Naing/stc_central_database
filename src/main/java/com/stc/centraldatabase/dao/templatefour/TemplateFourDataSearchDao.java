package com.stc.centraldatabase.dao.templatefour;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.stc.centraldatabase.model.templatefour.TemplateFourData;
import com.stc.centraldatabase.model.templatefour.TemplateFourSearchPaginateData;
import com.stc.centraldatabase.model.templateone.TemplateSearchData;
import com.stc.centraldatatbase.util.DataBaseConnection;
import com.stc.centraldatatbase.util.PrjUtil;

public class TemplateFourDataSearchDao 
{
	public  String getCriteria(TemplateSearchData criteria)
	{
		String filter = " where 1=1";
		
		if(!criteria.getCode().equals(""))
			
		{
			filter += " and code like '%"+criteria.getCode()+"%'";
			
		}
		
		if(!criteria.getDesc().equals(""))
			
		{
			filter += " and description like '%"+criteria.getDesc()+"%'";
			
		}
		if(!criteria.getParentid().equals(""))
		{
			filter += " and parent_id = "+criteria.getParentid();
		}
		if(criteria.getLimit() != 0)
			filter += " limit "+criteria.getOffset()+","+criteria.getLimit();

		return filter;
	}
	
	public static String getCountCriteria(TemplateSearchData criteria)
	{
		String filter = " where 1=1";
		
		if(!criteria.getCode().equals(""))
			
		{
			filter += " and code like '%"+criteria.getCode()+"%'";
			
		}
		
		if(!criteria.getDesc().equals(""))
			
		{
			filter += " and Description like '%"+criteria.getDesc()+"%'";
			
		}
		if(!criteria.getParentid().equals(""))
		{
			filter += " and parent_id = "+criteria.getParentid();
		}
		return filter;
	}
	
	public boolean validateSettingUpdate(String id,String code ) {
		PreparedStatement ps = null;
		try {
			Connection con = DataBaseConnection.getConnection();
			ps = con.prepareStatement("Select * from template_four where code = ? and parent_id <> ?");
			ps.setString(1, code);
			ps.setString(2, id);

			ResultSet rs = ps.executeQuery();

			if (rs.next()) 
			{
				return false;
			}
			else return true;
		} catch (SQLException ex) {
			System.out.println("Validation error -->" + ex.getMessage());
			return false;
		} 
		
	}
	
	public  int getTotalCount(TemplateSearchData search,Connection con)
	{
		int count = 0;
		String filter = getCountCriteria(search);
		String query = "Select count(*) as count from TEMPLATE_FOUR "+ filter;
		Statement st;
		try {
			st = con.createStatement();
			ResultSet rs = st.executeQuery(query);
			if(rs.next())
				count = rs.getInt("count");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 return count;
	}
	
	public  TemplateFourSearchPaginateData find(TemplateSearchData usearch)
	{
		TemplateFourSearchPaginateData res = new TemplateFourSearchPaginateData();
		String filter = getCriteria(usearch);
		List<TemplateFourData> resList = new ArrayList<TemplateFourData>();
		Connection con = null;
		TemplateFourData setting;
		String query = "Select * from TEMPLATE_FOUR" +filter;
		
		
		try {
			con = DataBaseConnection.getConnection();
			Statement st = con.createStatement();
			 ResultSet rs = st.executeQuery(query);
			 
			 while(rs.next())
			 {
				 setting = new TemplateFourData();
				 setting.setId(rs.getInt("id"));
				 setting.setSettingDate(PrjUtil.StringtoDate(rs.getString("setting_date")));				 
				 setting.setCode(rs.getString("code"));
				 setting.setDesc(rs.getString("description"));
				 setting.setParentid(rs.getInt("parent_id"));
				 setting.setSettingDateInDateFormat(PrjUtil.datetimeTostring(rs.getString("setting_date")));
				 setting.setNameOfIEC(rs.getString("IEC_NAME"));
				 setting.setNoOfPiece(rs.getInt("NO_OF_PIECES"));
				 resList.add(setting);
			 }
			
			res.setCount(getTotalCount(usearch,con));
			res.setTemplateFourDataList(resList);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			DataBaseConnection.close(con);
		}
			
		return res;
	}
	
	
	public TemplateFourData findById(String id)
	{	
		Connection con = null;
		TemplateFourData setting = null;
		String query = "Select * from template_four where id="+id ;
		
		
		try {
			con = DataBaseConnection.getConnection();
			Statement st = con.createStatement();
			 ResultSet rs = st.executeQuery(query);
			 
			 while(rs.next())
			 {
				 setting = new TemplateFourData();
				 setting.setId(rs.getInt("id"));
				 setting.setSettingDate(PrjUtil.StringtoDate(rs.getString("setting_date")));
				 setting.setParentid(rs.getInt("parent_id"));
				 setting.setSettingDate(PrjUtil.StringtoDate(rs.getString("setting_date")));				 
				 setting.setCode(rs.getString("code"));
				 setting.setDesc(rs.getString("description"));
				 setting.setParentid(rs.getInt("parent_id"));
				 setting.setSettingDateInDateFormat(PrjUtil.datetimeTostring(rs.getString("setting_date")));
				 setting.setNameOfIEC(rs.getString("IEC_NAME"));
				 setting.setNoOfPiece(rs.getInt("NO_OF_PIECES"));
			 }
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			DataBaseConnection.close(con);
		}
			
		 return setting;
	}
	
	
}
