package com.stc.centraldatabase.service.setting;

import java.util.List;

import com.stc.centraldatabase.model.setting.SettingData;
import com.stc.centraldatabase.model.setting.SettingSearchData;
import com.stc.centraldatabase.model.setting.SettingSearchPaginateData;

public interface SettingDataService {
	
	boolean insertSettingData(SettingData setting , String tbName);
	
	SettingSearchPaginateData find(SettingSearchData settingSearch , String tbname);
	
	boolean delete(String id , String tbname);
	
	SettingData findByid(String id , String tbname);
	
	boolean update(SettingData setting , String tbname);
	
	SettingData findByCode(String code , String tbname);
	
	List<SettingData> getSettingList(String tbname);
	
	boolean validateSettingUpdate(String id, String code , String tbname);
	
}
