package com.stc.centraldatabase.mgr.templateone;


import com.stc.centraldatabase.dao.templateone.TemplateOneDataDao;
import com.stc.centraldatabase.dao.templateone.TemplateOneDataSearchDao;
import com.stc.centraldatabase.model.templateone.TemplateOneData;
import com.stc.centraldatabase.model.templateone.TemplateOneSearchPaginateData;
import com.stc.centraldatabase.model.templateone.TemplateSearchData;

public class TemplateOneMgr 
{
	public static boolean inserData(TemplateOneData template)
	{
		TemplateOneDataDao templateOneDao = new TemplateOneDataDao();
		return templateOneDao.insertTemplateOneData(template);
	}
	
	public static boolean updateData(TemplateOneData activity)
	{
		TemplateOneDataDao templateOneDao = new TemplateOneDataDao();
		return templateOneDao.updateTemplateOneData(activity);
	}
	
	public static TemplateOneSearchPaginateData find(TemplateSearchData settingSearch)
	{
		TemplateOneDataSearchDao settingSearchDao = new TemplateOneDataSearchDao();
		return settingSearchDao.find(settingSearch);
	}
	
	public static TemplateOneData findById(String id)
	{
		TemplateOneDataSearchDao activitySearchDao = new TemplateOneDataSearchDao();
		return activitySearchDao.findById(id);
	}
	
	public static boolean validateUpdate(String id,String code)
	{
		TemplateOneDataSearchDao activitySearchDao = new TemplateOneDataSearchDao();
		return activitySearchDao.validateSettingUpdate(id, code);
	}

	public static boolean delete(String id)
	{
		TemplateOneDataDao templateOneDao = new TemplateOneDataDao();
		return templateOneDao.delete(id);
	}
}
