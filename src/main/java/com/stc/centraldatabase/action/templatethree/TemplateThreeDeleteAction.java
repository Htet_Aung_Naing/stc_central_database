package com.stc.centraldatabase.action.templatethree;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.stc.centraldatabase.model.templatethree.TemplateThreeData;
import com.stc.centraldatabase.service.templatethree.TemplateThreeDataService;
import com.stc.centraldatabase.service.templatethree.TemplateThreeDataServiceImpl;



@ManagedBean(name = "templateThreeDeleteAction")
@ViewScoped
public class TemplateThreeDeleteAction implements Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = -425655372664387173L;

	TemplateThreeDataService templateService;
	String title;
	DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
	String templateId;
	String templateOnecode= "";

	

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	

	@ManagedProperty(value = "#{templatethree}")
	TemplateThreeData templateThreeData;
	

	public TemplateThreeData getTemplateThreeData() {
		return templateThreeData;
	}

	public void setTemplateThreeData(TemplateThreeData templateThreeData) {
		this.templateThreeData = templateThreeData;
	}

	@PostConstruct
	public void init()  
	{
		this.templateThreeData = new TemplateThreeData();
		
		this.templateId = (String) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("templateid");
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateParentid", templateId );
		this.templateService = new TemplateThreeDataServiceImpl();
		this.templateThreeData = templateService.findById(templateId);
		this.title = templateThreeData.getCode()+" Delete";
	}
	
	public String delete()
	{
		
		if(templateService.delete(templateId))
		{
			FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateParentid",  String.valueOf(templateThreeData.getParentid()));	
			FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateTitle",  templateThreeData.getCode());
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "INFO!", "Delete Template Three Data Successfully!"));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			return "template_three_search";
		}
		else
		{
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "ERROR!", "Setting Data cannot delete!"));
			return null;
		}
		
	}
	
	public String back()
	{	
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateTitle",  templateThreeData.getCode());
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateParentid", String.valueOf(templateThreeData.getParentid()));		
		return "template_three_search";
	}
	
	
	
}
