package com.stc.centraldatabase.action.setting;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import com.stc.centraldatabase.model.setting.SettingData;
import com.stc.centraldatabase.service.setting.SettingDataService;
import com.stc.centraldatabase.service.setting.SettingDataServiceImpl;
import com.stc.centraldatatbase.util.SessionUtil;



@ManagedBean(name = "settingAction")
@ViewScoped
public class SettingGoalAction implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8960748410953236774L;

	SettingDataService settingService;
	DateFormat df = new SimpleDateFormat("dd/MM/yyyy");

	
	@ManagedProperty(value = "#{setting}")
	SettingData setting;
	
	


	public SettingData getSetting() {
		return setting;
	}

	public void setSetting(SettingData setting) {
		this.setting = setting;
	}

	@PostConstruct
	public void init()  
	{
		this.setting = new SettingData();	
		this.settingService  = new SettingDataServiceImpl();
	

	}
	
	public String register()
	{
		HttpSession session = SessionUtil.getSession();
		setting.setCreatedUserName((String) session.getAttribute("userid"));
		setting.setModifiedUserName((String) session.getAttribute("userid"));
		session.setAttribute("insert_success_title", "Impact Register Successfully!");
		if(settingService.insertSettingData(setting,"setting_goal"))
		{	
			
			FacesContext.getCurrentInstance().getExternalContext().getFlash().put("settingData", setting);
			return "goalRegistrationSuccess";
		}			
		else
		{
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "ERROR!", "Setting's Codes are duplicate!"));
			return null;
		}
	}
	
}
