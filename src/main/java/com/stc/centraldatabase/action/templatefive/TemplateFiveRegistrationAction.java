package com.stc.centraldatabase.action.templatefive;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import com.stc.centraldatabase.model.setting.SettingData;
import com.stc.centraldatabase.model.templatefive.TemplateFiveData;
import com.stc.centraldatabase.service.templatefive.TemplateFiveDataService;
import com.stc.centraldatabase.service.templatefive.TemplateFiveDataServiceImpl;
import com.stc.centraldatatbase.util.SessionUtil;



@ManagedBean(name = "templateFiveRegistrationAction")
@ViewScoped
public class TemplateFiveRegistrationAction implements Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = -8891520626941344235L;
	/**
	 * 
	 */


	TemplateFiveDataService templateService;
	String title;
	String parentid;
	DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
	boolean isPopup = false;
	

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	





	public TemplateFiveData getTemplateFiveData() {
		return templateFiveData;
	}

	public void setTemplateFiveData(TemplateFiveData templateFiveData) {
		this.templateFiveData = templateFiveData;
	}

	@ManagedProperty(value = "#{templatefive}")
	TemplateFiveData templateFiveData;
	
	
	@PostConstruct
	public void init()  
	{
		this.templateFiveData = new TemplateFiveData();	
		
		if(FacesContext.getCurrentInstance().getExternalContext().getFlash().get("templateParentid") != null)
		{
			this.parentid = (String) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("templateParentid");
			this.title = (String) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("templateTitle");
		}
		else
		{
			HttpSession session = SessionUtil.getSession();
			this.title = session.getAttribute("templateTitle").toString();
			this.parentid = session.getAttribute("templateParentid").toString();
			isPopup = true;
		}
		
		this.templateService = new TemplateFiveDataServiceImpl();
		
	}
	
	public String getSettingmenuLabel(List<SettingData> settingList , long key)
	{
		String res = "";
		
		for (SettingData settingData : settingList) 
		{
			if(key == settingData.getId())
			{
				res = settingData.getT1();
				break;
			}
				
		}
		return res;
	}
	


	
	
	public String register()
	{
		HttpSession session = SessionUtil.getSession();
		templateFiveData.setCreatedUserName((String) session.getAttribute("userid"));
		templateFiveData.setModifiedUserName((String) session.getAttribute("userid"));
		templateFiveData.setParentid(Integer.parseInt(parentid));
		if(templateService.insertTemplateFiveData(templateFiveData))
		{	
			
			if(isPopup)
			{
				FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateParentid",  String.valueOf(templateFiveData.getParentid()));		
				FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateTitle",  templateFiveData.getCode());
				return "template_five_popup";
			}				
			else
			{
				FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateParentid",  String.valueOf(templateFiveData.getParentid()));		
				FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateTitle",  templateFiveData.getCode());
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "INFO!", "Template Five Data  Register Successfully!"));
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				return "template_five_registration";
			}
				
		}			
		else
		{
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "ERROR!", "Setting's Codes are duplicate!"));
			return null;
		}
	}
	
	public String back()
	{	
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateTitle",  templateFiveData.getCode());
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateParentid", String.valueOf(templateFiveData.getParentid()));		
		return "activitySearch";
	}
	
}
