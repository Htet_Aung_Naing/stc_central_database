package com.stc.centraldatabase.dao.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.stc.centraldatabase.user.mapper.SecurityMapper;

@Repository("securityDaoImpl")
@Transactional(propagation = Propagation.REQUIRED)
public class SecurityDaoImpl implements UserDetailsService{

	@Autowired(required = true)
	 SecurityMapper securityMapper;
	
	public UserDetails loadUserByUsername(String userId) throws UsernameNotFoundException {

		UserDetails result = null;
		try { 
			result = (UserDetails) securityMapper.loadUserByUserId(userId);
		} catch (RuntimeException pe) {
			pe.printStackTrace();
			
		}
		return new User(result.getUsername(), result.getPassword(),true,true,true,true,result.getAuthorities());
	}

}
