package com.stc.centraldatabase.service.templatefour;

import com.stc.centraldatabase.mgr.templatefour.TemplateFourMgr;
import com.stc.centraldatabase.model.templatefour.TemplateFourData;
import com.stc.centraldatabase.model.templatefour.TemplateFourSearchPaginateData;
import com.stc.centraldatabase.model.templateone.TemplateSearchData;

public class TemplateFourDataServiceImpl implements TemplateFourDataService{

	@Override
	public boolean insertTemplateFourData(TemplateFourData activity) {
		return TemplateFourMgr.inserData(activity);
	}

	@Override
	public boolean updateData(TemplateFourData activity) {
		return TemplateFourMgr.updateData(activity);
	}

	@Override
	public TemplateFourSearchPaginateData find(TemplateSearchData settingSearch) {
		return TemplateFourMgr.find(settingSearch);
	}

	@Override
	public TemplateFourData findById(String id) {
		return TemplateFourMgr.findById(id);
	}

	@Override
	public boolean validateTemplateDataUpdate(String id, String code) {
		return TemplateFourMgr.validateUpdate(id, code);
	}

	@Override
	public boolean delete(String activityId) {
		return TemplateFourMgr.delete(activityId);
	}

}
