package com.stc.centraldatabase.model.templatefour;

import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.stc.centraldatabase.model.templateone.TemplateSearchData;
import com.stc.centraldatabase.service.templatefour.TemplateFourDataService;
import com.stc.centraldatabase.service.templatefour.TemplateFourDataServiceImpl;






public class LazyTemplateFourDataModel extends LazyDataModel<TemplateFourData>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7167457755325524229L;
	/**
	 * 
	 */


	
	TemplateSearchData settingSearchData;
	TemplateFourDataService settingService;
	String tbname = "";


	

	public TemplateSearchData getSettingSearchData() {
		return settingSearchData;
	}

	public void setSettingSearchData(TemplateSearchData settingSearchData) {
		this.settingSearchData = settingSearchData;
	}

	public  LazyTemplateFourDataModel(TemplateSearchData settingSearchData) {
		this.settingSearchData = settingSearchData;
	}

	@Override
	public List<TemplateFourData> load(int first, int pageSize, String sortField,
			SortOrder sortOrder, Map<String, Object> filters) {
		settingService = new TemplateFourDataServiceImpl();
		
		settingSearchData.setOffset(first);
		settingSearchData.setLimit(pageSize);
		
		TemplateFourSearchPaginateData userPaginateData = settingService.find(settingSearchData);
		
		setRowCount(userPaginateData.getCount());
		setPageSize(settingSearchData.getLimit());
		
		List<TemplateFourData> userList = userPaginateData.getTemplateFourDataList();
		
		return userList;
	}

}
