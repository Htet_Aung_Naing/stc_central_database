package com.stc.centraldatabase.action.activity;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.primefaces.context.RequestContext;

import com.stc.centraldatabase.model.activity.ActivityData;
import com.stc.centraldatabase.model.setting.SettingData;
import com.stc.centraldatabase.service.activity.ActivityDataService;
import com.stc.centraldatabase.service.activity.ActivityDataServiceImpl;
import com.stc.centraldatabase.service.setting.SettingDataService;
import com.stc.centraldatabase.service.setting.SettingDataServiceImpl;
import com.stc.centraldatatbase.util.CommonEnum;
import com.stc.centraldatatbase.util.PrjUtil;
import com.stc.centraldatatbase.util.Result;
import com.stc.centraldatatbase.util.SessionUtil;
import com.stc.centraldatatbase.util.SetupData;



@ManagedBean(name = "settingActivityAction")
@ViewScoped
public class SettingActivityAction implements Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = -425655372664387173L;
	SettingDataService settingService;
	ActivityDataService activityService;
	DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
	List<SettingData> settingImpactList;
	List<SettingData> settingOutcomeList;
	List<SettingData> settingOutputList;
	String returnPage = "";
	List<SetupData> templateList = new ArrayList<SetupData>();
	boolean finishSave = false;
	
	public List<SetupData> getTemplateList() {
		return templateList;
	}

	public void setTemplateList(List<SetupData> templateList) {
		this.templateList = templateList;
	}

	String title = "";

	
	public List<SettingData> getSettingOutputList() {
		return settingOutputList;
	}

	public void setSettingOutputList(List<SettingData> settingOutputList) {
		this.settingOutputList = settingOutputList;
	}

	public List<SettingData> getSettingOutcomeList() {
		return settingOutcomeList;
	}

	public void setSettingOutcomeList(List<SettingData> settingOutcomeList) {
		this.settingOutcomeList = settingOutcomeList;
	}

	public List<SettingData> getSettingImpactList() {
		return settingImpactList;
	}

	public void setSettingImpactList(List<SettingData> settingImpactList) {
		this.settingImpactList = settingImpactList;
	}
	
	public void addTemplateData()
	{
		SetupData setup = new SetupData();
		setup.setLabel("Template one");
		setup.setValue(1);
		templateList.add(setup);
		setup = new SetupData();
		setup.setLabel("Template two");
		setup.setValue(2);
		templateList.add(setup);
		setup = new SetupData();
		setup.setLabel("Template three");
		setup.setValue(3);
		templateList.add(setup);
		setup = new SetupData();
		setup.setLabel("Template four");
		setup.setValue(4);
		templateList.add(setup);
		setup = new SetupData();
		setup.setLabel("Template Five");
		setup.setValue(5);
		templateList.add(setup);
		setup = new SetupData();
		setup.setLabel("Template Six");
		setup.setValue(6 );
		templateList.add(setup);
	}

	@ManagedProperty(value = "#{activity}")
	ActivityData activity;
	
	
	public boolean isFinishSave() {
		return finishSave;
	}

	public void setFinishSave(boolean finishSave) {
		this.finishSave = finishSave;
	}

	public ActivityData getActivity() {
		return activity;
	}

	public void setActivity(ActivityData activity) {
		this.activity = activity;
	}

	@PostConstruct
	public void init()  
	{
		this.activity = new ActivityData();	
		this.settingService  = new SettingDataServiceImpl();
		this.activityService = new ActivityDataServiceImpl();
		this.settingImpactList = settingService.getSettingList("setting_goal");
		this.settingOutcomeList = settingService.getSettingList("setting_outcome");
		this.settingOutputList = settingService.getSettingList("setting_output");
		addTemplateData();
	}
	
	public String getSettingmenuLabel(List<SettingData> settingList , long key)
	{
		String res = "";
		
		for (SettingData settingData : settingList) 
		{
			if(key == settingData.getId())
			{
				res = settingData.getT1();
				break;
			}
				
		}
		return res;
	}
	
	public String appearTemplatePopup()
	{
		returnPage = getTemplatePage(activity.getTemplateKey());
		//returnPage = "template_one_popup";

		HttpSession session = SessionUtil.getSession();
		session.setAttribute("templateTitle", activity.getCode());
		session.setAttribute("templateParentid", activity.getId());
		RequestContext.getCurrentInstance().openDialog(returnPage, PrjUtil.getDialogOption(), null);
	
		return null;			
	}
	
	public String getTemplatePage(long id)
	{
		String res = "";
		for(CommonEnum.Template_Popup_Page g : CommonEnum.Template_Popup_Page.values())
		{
			if(g.value() == id)
			{
				res = g.description();
				break;
			}
			
		}
		return res;
	}
	
	public String register()
	{
		HttpSession session = SessionUtil.getSession();
		activity.setCreatedUserName((String) session.getAttribute("userid"));
		activity.setModifiedUserName((String) session.getAttribute("userid"));
		session.setAttribute("insert_success_title", "Activity Register Successfully!");
		Result res = new Result();
		res = activityService.insertActivityData(activity);
		activity.setId(res.getResparentid());
		if(res.isRes())
		{	
			activity.setImpactcode(getSettingmenuLabel(settingImpactList, activity.getImpactKey()));
			activity.setOutcomecode(getSettingmenuLabel(settingOutcomeList, activity.getOutcomeKey()));
			activity.setOutputcode(getSettingmenuLabel(settingOutcomeList, activity.getOutputKey()));
//			FacesContext.getCurrentInstance().getExternalContext().getFlash().put("activityData", activity);
//			return "activityRegistrationSuccess";
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "INFO!", "Save Activity Successfully!"));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			finishSave = true;
			return null;
		}			
		else
		{
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "ERROR!", "Setting's Codes are duplicate!"));
			return null;
		}
	}
	
}
