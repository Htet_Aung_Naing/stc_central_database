package com.stc.centraldatabase.action.activity;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import com.stc.centraldatabase.model.activity.ActivityData;
import com.stc.centraldatabase.service.activity.ActivityDataService;
import com.stc.centraldatabase.service.activity.ActivityDataServiceImpl;
import com.stc.centraldatatbase.util.SessionUtil;




@ManagedBean(name = "activityDeletAction")
@ViewScoped
public class ActivityDeleteAction implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 767999823111101208L;
	
	String impactID;
	ActivityData settingData;
	ActivityDataService settingService;
	String tbname= "";
	String returnPage = "";
	
	public String getImpactID() {
		return impactID;
	}


	public void setImpactID(String impactID) {
		this.impactID = impactID;
	}

	

	public ActivityData getSettingData() {
		return settingData;
	}


	public void setSettingData(ActivityData settingData) {
		this.settingData = settingData;
	}


	@PostConstruct
	public void init()
	{
			HttpSession session = SessionUtil.getSession();
			settingService = new ActivityDataServiceImpl();
			returnPage = (String) session.getAttribute("return_page");
			settingData = new ActivityData();
			if(impactID == null)
			{
				this.impactID = (String) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("impact_id");
				this.tbname = (String) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("tbname");
				this.settingData = settingService.findById(impactID);
			}
		
	}
	
	public String delete()
	{
		
		if(settingService.delete(impactID))
		{
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "INFO!", "Delete Activity Successfully!"));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			return returnPage;
		}
		else
		{
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "ERROR!", "Activity cannot delete!"));
			return null;
		}
		
	}
	
}
