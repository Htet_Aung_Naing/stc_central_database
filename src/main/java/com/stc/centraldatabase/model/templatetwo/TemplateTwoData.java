package com.stc.centraldatabase.model.templatetwo;

import java.io.Serializable;
import java.util.Date;

public class TemplateTwoData implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1293638622525069495L;
	
	int id;
	int parentid;
	String modifiedDate;
	String createdUserName;
	String modifiedUserName;
	String createdDate;
	String code;
	String desc;
	Date settingDate;
	String settingDateInDateFormat;
	int oscCommunityBoy;
	int oscCommunityGirl;
	int toscCommunity;
	double percentCommunityBoy;
	String oscCommunityDataSource;
	int oscEnrollCmssBoy;
	int oscEnrollCmssGirl;
	int toscEnrollCmss;
	double percentCommunityGirl;
	double tpercent;
	String oscEnrollCmssDatasource;
	
	public TemplateTwoData()
	{
		this.id = 0;
		this.parentid = 0;
		this.modifiedDate = "";
		this.createdDate = "";
		this.createdUserName= "";
		this.modifiedUserName = "";
		this.settingDate = new Date();
		this.settingDateInDateFormat = "";
		this.oscCommunityBoy = 0;
		this.oscCommunityGirl =0;
		this.percentCommunityBoy = 0;
		this.oscEnrollCmssBoy = 0;
		this.oscEnrollCmssGirl = 0;
		this.percentCommunityGirl = 0;
		this.oscCommunityDataSource = "";
		this.oscEnrollCmssDatasource = "";
		this.toscCommunity = 0;
		this.toscEnrollCmss = 0;
		this.tpercent = 0;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getParentid() {
		return parentid;
	}
	public void setParentid(int parentid) {
		this.parentid = parentid;
	}
	public String getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	public String getCreatedUserName() {
		return createdUserName;
	}
	public void setCreatedUserName(String createdUserName) {
		this.createdUserName = createdUserName;
	}
	public String getModifiedUserName() {
		return modifiedUserName;
	}
	public void setModifiedUserName(String modifiedUserName) {
		this.modifiedUserName = modifiedUserName;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public Date getSettingDate() {
		return settingDate;
	}
	public void setSettingDate(Date settingDate) {
		this.settingDate = settingDate;
	}
	public String getSettingDateInDateFormat() {
		return settingDateInDateFormat;
	}
	public void setSettingDateInDateFormat(String settingDateInDateFormat) {
		this.settingDateInDateFormat = settingDateInDateFormat;
	}
	public int getOscCommunityBoy() {
		return oscCommunityBoy;
	}
	public void setOscCommunityBoy(int oscCommunityBoy) {
		this.oscCommunityBoy = oscCommunityBoy;
	}
	public int getOscCommunityGirl() {
		return oscCommunityGirl;
	}
	public void setOscCommunityGirl(int oscCommunityGirl) {
		this.oscCommunityGirl = oscCommunityGirl;
	}
	
	public int getToscCommunity() {
		return toscCommunity;
	}

	public void setToscCommunity(int toscCommunity) {
		this.toscCommunity = toscCommunity;
	}

	public int getToscEnrollCmss() {
		return toscEnrollCmss;
	}

	public void setToscEnrollCmss(int toscEnrollCmss) {
		this.toscEnrollCmss = toscEnrollCmss;
	}

	public double getTpercent() {
		return tpercent;
	}

	public void setTpercent(double tpercent) {
		this.tpercent = tpercent;
	}

	public String getOscCommunityDataSource() {
		return oscCommunityDataSource;
	}
	public void setOscCommunityDataSource(String oscCommunityDataSource) {
		this.oscCommunityDataSource = oscCommunityDataSource;
	}
	public int getOscEnrollCmssBoy() {
		return oscEnrollCmssBoy;
	}
	public void setOscEnrollCmssBoy(int oscEnrollCmssBoy) {
		this.oscEnrollCmssBoy = oscEnrollCmssBoy;
	}
	public int getOscEnrollCmssGirl() {
		return oscEnrollCmssGirl;
	}
	public void setOscEnrollCmssGirl(int oscEnrollCmssGirl) {
		this.oscEnrollCmssGirl = oscEnrollCmssGirl;
	}
	

	public double getPercentCommunityBoy() {
		return percentCommunityBoy;
	}

	public void setPercentCommunityBoy(double percentCommunityBoy) {
		this.percentCommunityBoy = percentCommunityBoy;
	}

	public double getPercentCommunityGirl() {
		return percentCommunityGirl;
	}

	public void setPercentCommunityGirl(double percentCommunityGirl) {
		this.percentCommunityGirl = percentCommunityGirl;
	}

	public String getOscEnrollCmssDatasource() {
		return oscEnrollCmssDatasource;
	}
	public void setOscEnrollCmssDatasource(String oscEnrollCmssDatasource) {
		this.oscEnrollCmssDatasource = oscEnrollCmssDatasource;
	}
	
}
