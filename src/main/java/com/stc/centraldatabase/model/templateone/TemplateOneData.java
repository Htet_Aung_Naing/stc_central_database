package com.stc.centraldatabase.model.templateone;

import java.io.Serializable;
import java.util.Date;

public class TemplateOneData implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5689471420920420360L;
	
	int id;
	int parentid;
	String modifiedDate;
	String createdUserName;
	String modifiedUserName;
	String createdDate;
	String code;
	String desc;
	int noOfHouse;
	int noOfHouseHold;
	int maleLessthan5YearsCount;
	int femaleLessthan5YearsCount;
	int male5T18Count;
	int female5T18Count;
	int maleOver18Count;
	int femaleOver18Count;
	int totalMaleCount;
	int totalFemaleCount;
	Date settingDate;
	String settingDateInDateFormat;
	
	
	public String getSettingDateInDateFormat() {
		return settingDateInDateFormat;
	}
	public void setSettingDateInDateFormat(String settingDateInDateFormat) {
		this.settingDateInDateFormat = settingDateInDateFormat;
	}
	public int getTotalMaleCount() {
		return totalMaleCount;
	}
	public void setTotalMaleCount(int totalMaleCount) {
		this.totalMaleCount = totalMaleCount;
	}
	public int getTotalFemaleCount() {
		return totalFemaleCount;
	}
	public void setTotalFemaleCount(int totalFemaleCount) {
		this.totalFemaleCount = totalFemaleCount;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	public String getCreatedUserName() {
		return createdUserName;
	}
	public void setCreatedUserName(String createdUserName) {
		this.createdUserName = createdUserName;
	}
	public String getModifiedUserName() {
		return modifiedUserName;
	}
	public void setModifiedUserName(String modifiedUserName) {
		this.modifiedUserName = modifiedUserName;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public Date getSettingDate() {
		return settingDate;
	}
	public void setSettingDate(Date settingDate) {
		this.settingDate = settingDate;
	}
	public int getParentid() {
		return parentid;
	}
	public void setParentid(int parentid) {
		this.parentid = parentid;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public int getNoOfHouse() {
		return noOfHouse;
	}
	public void setNoOfHouse(int noOfHouse) {
		this.noOfHouse = noOfHouse;
	}
	public int getNoOfHouseHold() {
		return noOfHouseHold;
	}
	public void setNoOfHouseHold(int noOfHouseHold) {
		this.noOfHouseHold = noOfHouseHold;
	}
	public int getMaleLessthan5YearsCount() {
		return maleLessthan5YearsCount;
	}
	public void setMaleLessthan5YearsCount(int maleLessthan5YearsCount) {
		this.maleLessthan5YearsCount = maleLessthan5YearsCount;
	}
	public int getFemaleLessthan5YearsCount() {
		return femaleLessthan5YearsCount;
	}
	public void setFemaleLessthan5YearsCount(int femaleLessthan5YearsCount) {
		this.femaleLessthan5YearsCount = femaleLessthan5YearsCount;
	}
	public int getMale5T18Count() {
		return male5T18Count;
	}
	public void setMale5T18Count(int male5t18Count) {
		male5T18Count = male5t18Count;
	}
	public int getFemale5T18Count() {
		return female5T18Count;
	}
	public void setFemale5T18Count(int female5t18Count) {
		female5T18Count = female5t18Count;
	}
	public int getMaleOver18Count() {
		return maleOver18Count;
	}
	public void setMaleOver18Count(int maleOver18Count) {
		this.maleOver18Count = maleOver18Count;
	}
	public int getFemaleOver18Count() {
		return femaleOver18Count;
	}
	public void setFemaleOver18Count(int femaleOver18Count) {
		this.femaleOver18Count = femaleOver18Count;
	}
	
	public TemplateOneData()
	{
		this.code = "";
		this.desc = "";
		this.female5T18Count = 0;
		this.male5T18Count = 0;
		this.maleLessthan5YearsCount = 0;
		this.femaleLessthan5YearsCount = 0;
		this.maleOver18Count = 0;
		this.femaleOver18Count = 0;
		this.id = 0;
		this.parentid = 0;
		this.noOfHouse = 0;
		this.noOfHouseHold = 0;
		this.settingDate = new Date();
		this.totalFemaleCount = 0;
		this.settingDateInDateFormat = "";
		this.totalMaleCount = 0;
	}

}
