package com.stc.centraldatabase.action.activity;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.stc.centraldatabase.model.activity.ActivityData;
import com.stc.centraldatabase.service.activity.ActivityDataService;
import com.stc.centraldatabase.service.activity.ActivityDataServiceImpl;



@ManagedBean(name = "activityDetailAction")
@ViewScoped
public class ActivityDetailAction implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4372970301156244048L;
	ActivityDataService settingService;
	String settingid;
	String title;
	String tbname;

	ActivityData settingData;

	public String getSettingid() {
		return settingid;
	}


	public void setSettingid(String settingid) {
		this.settingid = settingid;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}



	public ActivityData getSettingData() {
		return settingData;
	}


	public void setSettingData(ActivityData settingData) {
		this.settingData = settingData;
	}


	@PostConstruct
	public void init()
	{
			settingService = new  ActivityDataServiceImpl();
			settingData = new ActivityData();
			if(settingid == null)
			{
				tbname = (String) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("tbname");
				settingid = (String) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("impact_id");
				settingData = settingService.findById(settingid);
				settingData.setVariance(settingData.getTargetpoint() - settingData.getReachpoint());
			}
		
	}
	
}
