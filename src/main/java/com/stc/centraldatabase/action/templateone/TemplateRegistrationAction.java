package com.stc.centraldatabase.action.templateone;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import com.stc.centraldatabase.model.setting.SettingData;
import com.stc.centraldatabase.model.templateone.TemplateOneData;
import com.stc.centraldatabase.service.templateone.TemplateOneDataService;
import com.stc.centraldatabase.service.templateone.TemplateOneDataServiceImpl;
import com.stc.centraldatatbase.util.SessionUtil;



@ManagedBean(name = "templateRegistrationAction")
@ViewScoped
public class TemplateRegistrationAction implements Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = -425655372664387173L;

	TemplateOneDataService templateService;
	String title;
	String parentid;
	DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
	boolean isPopup = false;

	

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public TemplateOneData getTemplateOneData() {
		return templateOneData;
	}

	public void setTemplateOneData(TemplateOneData templateOneData) {
		this.templateOneData = templateOneData;
	}





	@ManagedProperty(value = "#{templateone}")
	TemplateOneData templateOneData;
	
	


	


	@PostConstruct
	public void init()  
	{
		this.templateOneData = new TemplateOneData();
		
		if(FacesContext.getCurrentInstance().getExternalContext().getFlash().get("templateParentid") != null)
		{
			this.parentid = (String) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("templateParentid");
			this.title = (String) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("templateTitle");
		}else
		{
			HttpSession session = SessionUtil.getSession();
			this.title = session.getAttribute("templateTitle").toString();
			this.parentid = session.getAttribute("templateParentid").toString();
			isPopup = true;
		}
	
		this.templateService = new TemplateOneDataServiceImpl();
		
	}
	
	public String getSettingmenuLabel(List<SettingData> settingList , long key)
	{
		String res = "";
		
		for (SettingData settingData : settingList) 
		{
			if(key == settingData.getId())
			{
				res = settingData.getT1();
				break;
			}
				
		}
		return res;
	}
	


	
	
	public String register()
	{
		HttpSession session = SessionUtil.getSession();
		templateOneData.setCreatedUserName((String) session.getAttribute("userid"));
		templateOneData.setModifiedUserName((String) session.getAttribute("userid"));
		templateOneData.setParentid(Integer.parseInt(parentid));
		if(templateService.insertTemplateOneData(templateOneData))
		{	
			
			if(isPopup)
			{
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "INFO!", "Template One Register Successfully!"));
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				return"template_one_popup";
			}				
			else
			{
				FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateParentid",  String.valueOf(templateOneData.getParentid()));		
				FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateTitle",  templateOneData.getCode());
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "INFO!", "Template One Register Successfully!"));
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				return "template_one_registration";
			}				
		}			
		else
		{
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "ERROR!", "Setting's Codes are duplicate!"));
			return null;
		}
	}
	
	public String back()
	{		
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateParentid", String.valueOf(templateOneData.getParentid()));		
		return "activitySearch";
	}
	
}
