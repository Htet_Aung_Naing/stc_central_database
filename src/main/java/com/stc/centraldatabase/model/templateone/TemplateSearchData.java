package com.stc.centraldatabase.model.templateone;

import java.io.Serializable;

public class TemplateSearchData implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3838318799981092307L;
	
	String code ;
	String desc;
	int offset;
	String parentid;
	int limit;
	
	public String getParentid() {
		return parentid;
	}
	public void setParentid(String parentid) {
		this.parentid = parentid;
	}
	public int getOffset() {
		return offset;
	}
	public void setOffset(int offset) {
		this.offset = offset;
	}
	public int getLimit() {
		return limit;
	}
	public void setLimit(int limit) {
		this.limit = limit;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	
	public TemplateSearchData()
	{
		this.code = "";
		this.desc = "";
		this.parentid = "";
	}

}
