package com.stc.centraldatabase.model.templatesix;

import java.io.Serializable;
import java.util.Date;

public class TemplateSixData implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2042559502939540367L;
	
	int id;
	int parentid;
	String modifiedDate;
	String createdUserName;
	String modifiedUserName;
	String createdDate;
	String code;
	String desc;
	Date settingDate;
	String settingDateInDateFormat;
	String place;
	String toWhom;
	String withWhom;
	int participantMale;
	int participantFemale;
	int tParticipants;
	
	public TemplateSixData()
	{
		this.id = 0 ;
		this.parentid = 0;
		this.modifiedDate = "";
		this.createdDate = "";
		this.modifiedUserName = "";
		this.createdUserName = "";
		this.code = "";
		this.desc = "";
		this.settingDate = new Date();
		this.settingDateInDateFormat = "";
		this.place = "";
		this.toWhom = "";
		this.participantFemale = 0;
		this.participantMale = 0;
		this.tParticipants = 0;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getParentid() {
		return parentid;
	}
	public void setParentid(int parentid) {
		this.parentid = parentid;
	}
	public String getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	public String getCreatedUserName() {
		return createdUserName;
	}
	public void setCreatedUserName(String createdUserName) {
		this.createdUserName = createdUserName;
	}
	public String getModifiedUserName() {
		return modifiedUserName;
	}
	public void setModifiedUserName(String modifiedUserName) {
		this.modifiedUserName = modifiedUserName;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public Date getSettingDate() {
		return settingDate;
	}
	public void setSettingDate(Date settingDate) {
		this.settingDate = settingDate;
	}
	public String getSettingDateInDateFormat() {
		return settingDateInDateFormat;
	}
	public void setSettingDateInDateFormat(String settingDateInDateFormat) {
		this.settingDateInDateFormat = settingDateInDateFormat;
	}
	public String getPlace() {
		return place;
	}
	public void setPlace(String place) {
		this.place = place;
	}
	public String getToWhom() {
		return toWhom;
	}
	public void setToWhom(String toWhom) {
		this.toWhom = toWhom;
	}
	public String getWithWhom() {
		return withWhom;
	}
	public void setWithWhom(String withWhom) {
		this.withWhom = withWhom;
	}
	public int getParticipantMale() {
		return participantMale;
	}
	public void setParticipantMale(int participantMale) {
		this.participantMale = participantMale;
	}
	public int getParticipantFemale() {
		return participantFemale;
	}
	public void setParticipantFemale(int participantFemale) {
		this.participantFemale = participantFemale;
	}
	public int gettParticipants() {
		return tParticipants;
	}
	public void settParticipants(int tParticipants) {
		this.tParticipants = tParticipants;
	}
	
	

}
