package com.stc.centraldatabase.model.templatefour;

import java.io.Serializable;
import java.util.Date;

public class TemplateFourData implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2791390111827384920L;
	
	int id;
	int parentid;
	String modifiedDate;
	String createdUserName;
	String modifiedUserName;
	String createdDate;
	String code;
	String desc;
	Date settingDate;
	String settingDateInDateFormat;
	String nameOfIEC;
	int noOfPiece;
	
	public TemplateFourData()
	{
		this.id = 0 ;
		this.parentid = 0;
		this.modifiedDate = "";
		this.createdDate = "";
		this.createdUserName = "";
		this.modifiedUserName = "";
		this.code = "";
		this.desc = "";
		this.settingDate = new Date();
		this.settingDateInDateFormat = "";
		this.nameOfIEC = "";
		this.noOfPiece = 0;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getParentid() {
		return parentid;
	}
	public void setParentid(int parentid) {
		this.parentid = parentid;
	}
	public String getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	public String getCreatedUserName() {
		return createdUserName;
	}
	public void setCreatedUserName(String createdUserName) {
		this.createdUserName = createdUserName;
	}
	public String getModifiedUserName() {
		return modifiedUserName;
	}
	public void setModifiedUserName(String modifiedUserName) {
		this.modifiedUserName = modifiedUserName;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public Date getSettingDate() {
		return settingDate;
	}
	public void setSettingDate(Date settingDate) {
		this.settingDate = settingDate;
	}
	public String getSettingDateInDateFormat() {
		return settingDateInDateFormat;
	}
	public void setSettingDateInDateFormat(String settingDateInDateFormat) {
		this.settingDateInDateFormat = settingDateInDateFormat;
	}
	public String getNameOfIEC() {
		return nameOfIEC;
	}
	public void setNameOfIEC(String nameOfIEC) {
		this.nameOfIEC = nameOfIEC;
	}
	public int getNoOfPiece() {
		return noOfPiece;
	}
	public void setNoOfPiece(int noOfPiece) {
		this.noOfPiece = noOfPiece;
	}
	
	

}
