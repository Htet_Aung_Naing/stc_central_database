package com.stc.centraldatabase.action.setting;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.primefaces.component.datatable.DataTable;

import com.stc.centraldatabase.model.setting.LazySettingDataModel;
import com.stc.centraldatabase.model.setting.SettingData;
import com.stc.centraldatabase.model.setting.SettingSearchData;
import com.stc.centraldatabase.service.setting.SettingDataService;
import com.stc.centraldatabase.service.setting.SettingDataServiceImpl;
import com.stc.centraldatatbase.util.SessionUtil;





@ManagedBean(name = "settingOutcomeSearchAction")
@ViewScoped
public class SettingOutcomeSearchAction implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6474306882073540913L;
	
	SettingSearchData settingSearchData;
	List<SettingData> impactList;
	LazySettingDataModel settingdataModel;
	SettingDataService settingService;

	public LazySettingDataModel getSettingdataModel() {
		return settingdataModel;
	}

	public void setSettingdataModel(LazySettingDataModel settingdataModel) {
		this.settingdataModel = settingdataModel;
	}



	
	public List<SettingData> getImpactList() {
		return impactList;
	}

	public void setImpactList(List<SettingData> impactList) {
		this.impactList = impactList;
	}

	public SettingSearchData getSettingSearchData() {
		return settingSearchData;
	}

	public void setSettingSearchData(SettingSearchData settingSearchData) {
		this.settingSearchData = settingSearchData;
	}


	
	public void reset(String targetDataTable) {
		resetPagination(targetDataTable);
		LoadData();
	}

	
	private void resetPagination(String targetDataTable) {
		DataTable d = (DataTable) FacesContext.getCurrentInstance().getViewRoot()
				.findComponent("settingGoalSearchForm:settingSearchTable");
		d.setFirst(0);
	}
	
	public void search(String targetDataTable) {

		// Check Joined Date From should be earlier than Joined Date To.
			// Reset pagination and do the lazy data model process.
			resetPagination(targetDataTable);
			settingdataModel = new LazySettingDataModel(settingSearchData, "setting_outcome");
		
	}
	
	@PostConstruct
	public void LoadData()
	{
		settingService = new SettingDataServiceImpl();
		settingSearchData = new SettingSearchData();
		impactList = settingService.getSettingList("setting_goal");
		settingdataModel = new LazySettingDataModel(settingSearchData, "setting_outcome");
		
	}
	
	public String edit(String id) {
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("tbname", "setting_outcome");
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("outcome_id", id);
		return "settingOutcomeUpdate";
	}



	public String delete(String id) {
		HttpSession session = SessionUtil.getSession();
		session.setAttribute("delete_title", "Outcome Delete");
		session.setAttribute("return_page", "settingOutcomeSearch");
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("tbname", "setting_outcome");
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("impact_id", id);
		return "settingOutcomeDelete";
	}
	
	public String detail(String id) {
		HttpSession session = SessionUtil.getSession();
		session.setAttribute("detail_title", "Outcome Detail");
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("tbname", "setting_outcome");
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("impact_id", id);
		return "settingOutcomeDetail";
	}
	





}
