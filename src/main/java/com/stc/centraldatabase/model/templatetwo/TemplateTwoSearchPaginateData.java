package com.stc.centraldatabase.model.templatetwo;

import java.io.Serializable;
import java.util.List;

public class TemplateTwoSearchPaginateData implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8793922769046576099L;
	
	int count;
	List<TemplateTwoData> templateTwoDataList;
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public List<TemplateTwoData> getTemplateTwoDataList() {
		return templateTwoDataList;
	}
	public void setTemplateTwoDataList(List<TemplateTwoData> templateTwoDataList) {
		this.templateTwoDataList = templateTwoDataList;
	}


	
	

}
