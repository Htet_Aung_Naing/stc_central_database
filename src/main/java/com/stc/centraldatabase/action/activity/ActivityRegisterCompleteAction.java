package com.stc.centraldatabase.action.activity;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.stc.centraldatabase.model.activity.ActivityData;




@ManagedBean(name = "activityRegisterCompleteAction")
@ViewScoped
public class ActivityRegisterCompleteAction implements Serializable {

	/** serialVersionUID */
	private static final long serialVersionUID = 3949168443374966040L;

	ActivityData activity;



	public ActivityData getActivity() {
		return activity;
	}



	public void setActivity(ActivityData activity) {
		this.activity = activity;
	}



	@PostConstruct
	public void init() 
	{
		this.activity = (ActivityData) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("activityData");
		activity.setVariance(activity.getTargetpoint() - activity.getReachpoint());
		FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		
	}
}
