package com.stc.centraldatabase.service.templatefive;

import com.stc.centraldatabase.model.templateone.TemplateSearchData;
import com.stc.centraldatabase.model.templatetwo.TemplateTwoData;
import com.stc.centraldatabase.model.templatetwo.TemplateTwoSearchPaginateData;

public interface TemplateTwoDataService {
	
	
boolean insertTemplateTwoData(TemplateTwoData activity);
	
	boolean updateData(TemplateTwoData activity);
	
	TemplateTwoSearchPaginateData find(TemplateSearchData settingSearch );
	
	TemplateTwoData findById(String id);
	
	boolean validateTemplateDataUpdate(String id, String code);
	
	boolean delete(String activityId);

}
