package com.stc.centraldatabase.service.templatethree;

import com.stc.centraldatabase.model.templateone.TemplateSearchData;
import com.stc.centraldatabase.model.templatethree.TemplateThreeData;
import com.stc.centraldatabase.model.templatethree.TemplateThreeSearchPaginateData;

public interface TemplateThreeDataService {
	
	
boolean insertTemplateOneData(TemplateThreeData activity);
	
	boolean updateData(TemplateThreeData activity);
	
	TemplateThreeSearchPaginateData find(TemplateSearchData settingSearch );
	
	TemplateThreeData findById(String id);
	
	boolean validateTemplateDataUpdate(String id, String code);
	
	boolean delete(String activityId);

}
