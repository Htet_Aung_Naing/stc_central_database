package com.stc.centraldatabase.dao.templatefive;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.stc.centraldatabase.model.templatefive.TemplateFiveData;
import com.stc.centraldatabase.model.templatefive.TemplateFiveSearchPaginateData;
import com.stc.centraldatabase.model.templateone.TemplateSearchData;
import com.stc.centraldatatbase.util.DataBaseConnection;
import com.stc.centraldatatbase.util.PrjUtil;

public class TemplateFiveDataSearchDao 
{
	public  String getCriteria(TemplateSearchData criteria)
	{
		String filter = " where 1=1";
		
		if(!criteria.getCode().equals(""))
			
		{
			filter += " and code like '%"+criteria.getCode()+"%'";
			
		}
		
		if(!criteria.getDesc().equals(""))
			
		{
			filter += " and description like '%"+criteria.getDesc()+"%'";
			
		}
		if(!criteria.getParentid().equals(""))
		{
			filter += " and parent_id = "+criteria.getParentid();
		}
		if(criteria.getLimit() != 0)
			filter += " limit "+criteria.getOffset()+","+criteria.getLimit();

		return filter;
	}
	
	public static String getCountCriteria(TemplateSearchData criteria)
	{
		String filter = " where 1=1";
		
		if(!criteria.getCode().equals(""))
			
		{
			filter += " and code like '%"+criteria.getCode()+"%'";
			
		}
		
		if(!criteria.getDesc().equals(""))
			
		{
			filter += " and Description like '%"+criteria.getDesc()+"%'";
			
		}
		if(!criteria.getParentid().equals(""))
		{
			filter += " and parent_id = "+criteria.getParentid();
		}
		return filter;
	}
	
	public boolean validateSettingUpdate(String id,String code ) {
		PreparedStatement ps = null;
		try {
			Connection con = DataBaseConnection.getConnection();
			ps = con.prepareStatement("Select * from template_five where code = ? and parent_id <> ?");
			ps.setString(1, code);
			ps.setString(2, id);

			ResultSet rs = ps.executeQuery();

			if (rs.next()) 
			{
				return false;
			}
			else return true;
		} catch (SQLException ex) {
			System.out.println("Validation error -->" + ex.getMessage());
			return false;
		} 
		
	}
	
	public  int getTotalCount(TemplateSearchData search,Connection con)
	{
		int count = 0;
		String filter = getCountCriteria(search);
		String query = "Select count(*) as count from TEMPLATE_FIVE "+ filter;
		Statement st;
		try {
			st = con.createStatement();
			ResultSet rs = st.executeQuery(query);
			if(rs.next())
				count = rs.getInt("count");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 return count;
	}
	
	public  TemplateFiveSearchPaginateData find(TemplateSearchData usearch)
	{
		TemplateFiveSearchPaginateData res = new TemplateFiveSearchPaginateData();
		String filter = getCriteria(usearch);
		List<TemplateFiveData> resList = new ArrayList<TemplateFiveData>();
		Connection con = null;
		TemplateFiveData setting;
		String query = "Select * from TEMPLATE_FIVE" +filter;
		
		
		try {
			con = DataBaseConnection.getConnection();
			Statement st = con.createStatement();
			 ResultSet rs = st.executeQuery(query);
			 
			 while(rs.next())
			 {

				 setting = new TemplateFiveData();
				 setting.setId(rs.getInt("id"));
				 setting.setSettingDate(PrjUtil.StringtoDate(rs.getString("setting_date")));				 
				 setting.setCode(rs.getString("code"));
				 setting.setDesc(rs.getString("description"));
				 setting.setParentid(rs.getInt("parent_id"));
				 setting.setDirectMale(rs.getInt("direct_male"));
				 setting.setDirectFemale(rs.getInt("direct_female"));
				 setting.setDirectAdultMale(rs.getInt("direct_adult_male"));
				 setting.setDirectAdultFemale(rs.getInt("direct_adult_female"));
				 setting.setSciFund(rs.getLong("sci_fund"));
				 setting.setCommunityFund(rs.getLong("community_fund"));
				 setting.setSettingDateInDateFormat(PrjUtil.datetimeTostring(rs.getString("setting_date")));
				 
				 resList.add(setting);
			 }
			
			res.setCount(getTotalCount(usearch,con));
			res.setTemplatefiveDataList(resList);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			DataBaseConnection.close(con);
		}
			
		return res;
	}
	
	
	public TemplateFiveData findById(String id)
	{	
		Connection con = null;
		TemplateFiveData setting = null;
		String query = "Select * from template_five where id="+id ;
		
		
		try {
			con = DataBaseConnection.getConnection();
			Statement st = con.createStatement();
			 ResultSet rs = st.executeQuery(query);
			 
			 while(rs.next())
			 {
				 setting = new TemplateFiveData();
				 setting.setId(rs.getInt("id"));
				 setting.setSettingDate(PrjUtil.StringtoDate(rs.getString("setting_date")));
				 setting.setCode(rs.getString("code"));
				 setting.setDesc(rs.getString("description"));
				 setting.setParentid(rs.getInt("parent_id"));
				 setting.setDirectMale(rs.getInt("direct_male"));
				 setting.setDirectFemale(rs.getInt("direct_female"));
				 setting.setDirectAdultMale(rs.getInt("direct_adult_male"));
				 setting.setDirectAdultFemale(rs.getInt("direct_adult_female"));
				 setting.setSciFund(rs.getLong("sci_fund"));
				 setting.setCommunityFund(rs.getLong("community_fund"));
				 setting.setTdirect(setting.getDirectMale() + setting.getDirectFemale());
				 setting.setTdirectAdult(setting.getDirectAdultMale() + setting.getDirectAdultFemale());
				 setting.setTotalFund(setting.getSciFund() + setting.getCommunityFund());
				 setting.setSettingDateInDateFormat(PrjUtil.datetimeTostring(rs.getString("setting_date")));
			 }
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			DataBaseConnection.close(con);
		}
			
		 return setting;
	}
	
	
}
