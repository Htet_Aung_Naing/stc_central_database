package com.stc.centraldatabase.dao.templatesix;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Date;

import com.stc.centraldatabase.model.templatesix.TemplateSixData;
import com.stc.centraldatatbase.util.DataBaseConnection;
import com.stc.centraldatatbase.util.PrjUtil;

public class TemplateSixDataDao {
	
	public boolean insertTemplateSixData(TemplateSixData setting)
	{
		Connection con = null;
		PreparedStatement ps = null;
		

		 	try 
		 	{
				String query;
				
				if(setting != null)
				{
					con = DataBaseConnection.getConnection();
				     query = "INSERT INTO TEMPLATE_SIX (PARENT_ID, CREATED_DATE, CREATED_USER, MODIFIED_USER, "
				     		+ "MODIFIDED_DATE, CODE, DESCRIPTION, SETTING_DATE, PLACE, TO_WHOM, "
				     		+ "WITH_WHOM, PARTICIPANTS_MALE, PARTICIPANTS_FEMALE)"
				     		+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?)";
				     
				     
				     int i = 1;
				     ps = con.prepareStatement(query);
				     ps.setInt(i++, setting.getParentid());				    
				     ps.setString(i++, PrjUtil.stringToDate(new Date()));
				     ps.setString(i++, setting.getCreatedUserName());
				     ps.setString(i++, setting.getModifiedUserName());
				     ps.setString(i++, PrjUtil.stringToDate(new Date()));			    
				     ps.setString(i++, setting.getCode());
				     ps.setString(i++, setting.getDesc() );
				     ps.setString(i++, PrjUtil.stringToDate(setting.getSettingDate()));
				     ps.setString(i++, setting.getPlace());
				     ps.setString(i++, setting.getToWhom());
				     ps.setString(i++, setting.getWithWhom());
				     ps.setInt(i++, setting.getParticipantMale());
				     ps.setInt(i++, setting.getParticipantFemale());
				     
				     ps.execute();
				     con.close();
				     return true; 
					   				  
				}else
				{
				
					return false;
				}
				 
			} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
	
			return false;
			
		}
				
		}
	
	public  boolean updateTemplateSixData(TemplateSixData setting) 
	{
		Connection con = null;
		PreparedStatement ps = null;
		String query;
		try {
			con = DataBaseConnection.getConnection();
			
		
	     query = "UPDATE TEMPLATE_SIX SET MODIFIED_USER= ?, "
				   + "MODIFIDED_DATE=?, CODE =?, DESCRIPTION=?, SETTING_DATE=?, PLACE=?, TO_WHOM=?, "
				   + "WITH_WHOM=?, PARTICIPANTS_MALE=?, PARTICIPANTS_FEMALE=?"
			      + " WHERE ID = ?";
			     		
			    int i = 1;
				ps = con.prepareStatement(query);
				ps.setString(i++, setting.getModifiedUserName());
				ps.setString(i++, PrjUtil.stringToDate(new Date()));
			    ps.setString(i++, setting.getCode());
				ps.setString(i++, setting.getDesc());			
				ps.setString(i++, PrjUtil.stringToDate(setting.getSettingDate()));
				ps.setString(i++, setting.getPlace());
				ps.setString(i++, setting.getToWhom());
				ps.setString(i++, setting.getWithWhom());
				ps.setInt(i++, setting.getParticipantMale());
				ps.setInt(i++, setting.getParticipantFemale());
				ps.setLong(i++, setting.getId());
				ps.execute();
			      
			    con.close();
			    return true;
			
			
			
		} catch (SQLException ex) {
			System.out.println("Templage Six Data Registration Update error -->" + ex.getMessage());
			return false;
		} finally {
			DataBaseConnection.close(con);
		}
	}
	
	public  boolean delete(String settingid) {
		Connection con = null;
		PreparedStatement ps = null;
		String query;
		try {
			con = DataBaseConnection.getConnection();
			    	        
			     query = "DELETE FROM TEMPLATE_SIX where id=?";
			    int i = 1;
				ps = con.prepareStatement(query);
				ps.setString(i++, settingid);
				ps.execute();
			      
			    con.close();
			    return true;
			
			
			
		} catch (SQLException ex) {
			System.out.println("Template Six Delete error -->" + ex.getMessage());
			return false;
		} finally {
			DataBaseConnection.close(con);
		}
	}

}
