package com.stc.centraldatabase.action.templatethree;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import com.stc.centraldatabase.model.setting.SettingData;
import com.stc.centraldatabase.model.templatethree.TemplateThreeData;
import com.stc.centraldatabase.service.templatethree.TemplateThreeDataService;
import com.stc.centraldatabase.service.templatethree.TemplateThreeDataServiceImpl;
import com.stc.centraldatatbase.util.SessionUtil;



@ManagedBean(name = "templateThreeUpdateAction")
@ViewScoped
public class TemplateThreeUpdateAction implements Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = -425655372664387173L;

	TemplateThreeDataService templateService;
	String title;
	DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
	String templateId;
	String templateOnecode= "";

	

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public TemplateThreeData getTemplateThreeData() {
		return templateThreeData;
	}

	public void setTemplateThreeData(TemplateThreeData templateThreeData) {
		this.templateThreeData = templateThreeData;
	}


	@ManagedProperty(value = "#{templateone}")
	TemplateThreeData templateThreeData;
	

	@PostConstruct
	public void init()  
	{
		this.templateThreeData = new TemplateThreeData();
		
		this.templateId = (String) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("templateid");
		this.templateOnecode = (String) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("templatCode");
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateParentid", templateId );
		this.templateService = new TemplateThreeDataServiceImpl();
		this.templateThreeData = templateService.findById(templateId);
		this.title = templateThreeData.getCode();
	}
	
	public String getSettingmenuLabel(List<SettingData> settingList , long key)
	{
		String res = "";
		
		for (SettingData settingData : settingList) 
		{
			if(key == settingData.getId())
			{
				res = settingData.getT1();
				break;
			}
				
		}
		return res;
	}
	


	
	
	public String register()
	{
		HttpSession session = SessionUtil.getSession();
		templateThreeData.setCreatedUserName((String) session.getAttribute("userid"));
		templateThreeData.setModifiedUserName((String) session.getAttribute("userid"));
		if(templateService.validateTemplateDataUpdate(templateId, templateOnecode))
		{
			if(templateService.updateData(templateThreeData))
			{	
				FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateParentid",  String.valueOf(templateThreeData.getParentid()));	
				FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateTitle",  templateThreeData.getCode());
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "INFO!", "Template One Update Successfully!"));
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				return "template_three_search";
			}
			else
			{
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "ERROR!", "Setting's Codes are duplicate!"));
				return null;
			}
		}else
		{
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "ERROR!", "Codes must not be duplicate!"));
			return null;
		}		
	}
	
	public String back()
	{	
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateTitle",  templateThreeData.getCode());
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateParentid", String.valueOf(templateThreeData.getParentid()));		
		return "template_three_search";
	}
	
}
