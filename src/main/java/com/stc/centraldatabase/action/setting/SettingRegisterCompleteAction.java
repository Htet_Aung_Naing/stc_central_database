package com.stc.centraldatabase.action.setting;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import com.stc.centraldatabase.model.setting.SettingData;
import com.stc.centraldatatbase.util.SessionUtil;




@ManagedBean(name = "settingRegisterCompleteAction")
@ViewScoped
public class SettingRegisterCompleteAction implements Serializable {

	/** serialVersionUID */
	private static final long serialVersionUID = 3949168443374966040L;

	SettingData setting;
	String title;
	


	public String getTitle() {
		return title;
	}



	public void setTitle(String title) {
		this.title = title;
	}



	public SettingData getSetting() {
		return setting;
	}



	public void setSetting(SettingData setting) {
		this.setting = setting;
	}



	@PostConstruct
	public void init() 
	{
		HttpSession session = SessionUtil.getSession();
		title = (String) session.getAttribute("insert_success_title");

			this.setting = (SettingData) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("settingData");
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		
	}
}
