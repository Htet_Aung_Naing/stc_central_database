package com.stc.centraldatabase.action.templatefour;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import com.stc.centraldatabase.model.setting.SettingData;
import com.stc.centraldatabase.model.templatefour.TemplateFourData;
import com.stc.centraldatabase.service.templatefour.TemplateFourDataService;
import com.stc.centraldatabase.service.templatefour.TemplateFourDataServiceImpl;
import com.stc.centraldatatbase.util.SessionUtil;



@ManagedBean(name = "templateFourRegistrationAction")
@ViewScoped
public class TemplateFourRegistrationAction implements Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = -8891520626941344235L;
	/**
	 * 
	 */


	TemplateFourDataService templateService;
	String title;
	String parentid;
	DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
	boolean isPopup = false;
	

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	
	public TemplateFourData getTemplateFourData() {
		return templateFourData;
	}

	public void setTemplateFourData(TemplateFourData templateFourData) {
		this.templateFourData = templateFourData;
	}

	@ManagedProperty(value = "#{templatetwo}")
	TemplateFourData templateFourData;

	@PostConstruct
	public void init()  
	{
		this.templateFourData = new TemplateFourData();	
		
		if(FacesContext.getCurrentInstance().getExternalContext().getFlash().get("templateParentid") != null)
		{
			this.parentid = (String) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("templateParentid");
			this.title = (String) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("templateTitle");

		}else
		{
			HttpSession session = SessionUtil.getSession();
			this.title = session.getAttribute("templateTitle").toString();
			this.parentid = session.getAttribute("templateParentid").toString();
			isPopup = true;
		}
		
		this.templateService = new TemplateFourDataServiceImpl();
		
	}
	
	public String getSettingmenuLabel(List<SettingData> settingList , long key)
	{
		String res = "";
		
		for (SettingData settingData : settingList) 
		{
			if(key == settingData.getId())
			{
				res = settingData.getT1();
				break;
			}
				
		}
		return res;
	}
	


	
	
	public String register()
	{
		HttpSession session = SessionUtil.getSession();
		templateFourData.setCreatedUserName((String) session.getAttribute("userid"));
		templateFourData.setModifiedUserName((String) session.getAttribute("userid"));
		templateFourData.setParentid(Integer.parseInt(parentid));
		if(templateService.insertTemplateFourData(templateFourData))
		{	
			
			if(isPopup)
			{
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "INFO!", "Template Four Data  Register Successfully!"));
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				return "template_four_popup";
			}	
			else
			{
				FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateParentid",  String.valueOf(templateFourData.getParentid()));		
				FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateTitle",  templateFourData.getCode());
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "INFO!", "Template Four Data  Register Successfully!"));
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				return "template_four_registration";
			}				
		}			
		else
		{
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "ERROR!", "Setting's Codes are duplicate!"));
			return null;
		}
	}
	
	public String back()
	{	
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateTitle",  templateFourData.getCode());
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateParentid", String.valueOf(templateFourData.getParentid()));		
		return "activitySearch";
	}
	
}
