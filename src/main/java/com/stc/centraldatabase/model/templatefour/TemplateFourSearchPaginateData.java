package com.stc.centraldatabase.model.templatefour;

import java.io.Serializable;
import java.util.List;

public class TemplateFourSearchPaginateData implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8793922769046576099L;
	
	int count;
	List<TemplateFourData> templateFourDataList;
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public List<TemplateFourData> getTemplateFourDataList() {
		return templateFourDataList;
	}
	public void setTemplateFourDataList(List<TemplateFourData> templateFourDataList) {
		this.templateFourDataList = templateFourDataList;
	}
	


	
	

}
