package com.stc.centraldatabase.action.setting;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.primefaces.component.datatable.DataTable;

import com.stc.centraldatabase.model.setting.LazySettingDataModel;
import com.stc.centraldatabase.model.setting.SettingData;
import com.stc.centraldatabase.model.setting.SettingSearchData;
import com.stc.centraldatabase.service.setting.SettingDataService;
import com.stc.centraldatabase.service.setting.SettingDataServiceImpl;
import com.stc.centraldatatbase.util.SessionUtil;





@ManagedBean(name = "settingOutputSearchAction")
@ViewScoped
public class SettingOutputSearchAction implements Serializable{


	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6499344160282589714L;
	
	SettingSearchData settingSearchData;
	List<SettingData> impactList;
	List<SettingData> outcomeList;
	LazySettingDataModel settingdataModel;
	SettingDataService settingService;

	public List<SettingData> getImpactList() {
		return impactList;
	}

	public void setImpactList(List<SettingData> impactList) {
		this.impactList = impactList;
	}

	public List<SettingData> getOutcomeList() {
		return outcomeList;
	}

	public void setOutcomeList(List<SettingData> outcomeList) {
		this.outcomeList = outcomeList;
	}

	public LazySettingDataModel getSettingdataModel() {
		return settingdataModel;
	}

	public void setSettingdataModel(LazySettingDataModel settingdataModel) {
		this.settingdataModel = settingdataModel;
	}



	
	public SettingSearchData getSettingSearchData() {
		return settingSearchData;
	}

	public void setSettingSearchData(SettingSearchData settingSearchData) {
		this.settingSearchData = settingSearchData;
	}


	
	public void reset(String targetDataTable) {
		resetPagination(targetDataTable);
		LoadData();
	}

	
	private void resetPagination(String targetDataTable) {
		DataTable d = (DataTable) FacesContext.getCurrentInstance().getViewRoot()
				.findComponent("settingGoalSearchForm:settingSearchTable");
		d.setFirst(0);
	}
	
	public void search(String targetDataTable) {

		// Check Joined Date From should be earlier than Joined Date To.
			// Reset pagination and do the lazy data model process.
			resetPagination(targetDataTable);
			settingdataModel = new LazySettingDataModel(settingSearchData, "setting_output");
		
	}
	
	@PostConstruct
	public void LoadData()
	{
		settingService = new SettingDataServiceImpl();
		settingSearchData = new SettingSearchData();
		impactList = settingService.getSettingList("setting_goal");
		outcomeList = settingService.getSettingList("setting_outcome");
		settingdataModel = new LazySettingDataModel(settingSearchData, "setting_output");
		
	}
	
	public String edit(String id) {
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("tbname", "setting_output");
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("output_id", id);
		return "settingOutputUpdate";
	}



	public String delete(String id) {
		HttpSession session = SessionUtil.getSession();
		session.setAttribute("delete_title", "Output Delete");
		session.setAttribute("return_page", "outputSearch");
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("tbname", "setting_output");
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("impact_id", id);
		return "settingOutputDelete";
	}
	
	public String detail(String id) {
		HttpSession session = SessionUtil.getSession();
		session.setAttribute("detail_title", "Output Detail");
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("tbname", "setting_output");
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("impact_id", id);
		return "settingOutputDetail";
	}
	

}
