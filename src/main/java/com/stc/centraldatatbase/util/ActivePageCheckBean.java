package com.stc.centraldatatbase.util;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;


@Component
public class ActivePageCheckBean  {



	public ActivePageCheckBean() {
		
	}

	public boolean check(Authentication authentication, HttpServletRequest request) {
		boolean isAuthorize = true;

		if (request.getServletPath().contains("javax.faces.resource")
				|| request.getServletPath().contains("resources")) {
			return isAuthorize = true;
		}

		Collection<SimpleGrantedAuthority> authorities = (Collection<SimpleGrantedAuthority>) authentication
				.getAuthorities();

		if (authorities.contains(new SimpleGrantedAuthority("ROLE_ANONYMOUS"))) {

			 isAuthorize = false;
		} 

		return isAuthorize;
	}

}
