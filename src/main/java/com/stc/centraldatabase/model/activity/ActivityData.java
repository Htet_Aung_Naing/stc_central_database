package com.stc.centraldatabase.model.activity;

import java.io.Serializable;
import java.util.Date;

public class ActivityData implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1653094890737596454L;
	
	String createdDate;
	String code;
	String desc;
	String modifiedDate;
	String createdUserName;
	String modifiedUserName;
	Date setting_date;
	int record_status;
	String settingDate;
	String impactcode;
	String outputcode;
	String outcomecode;
	String template;
	String townshipCode;
	String townshipName;
	String villageCode;
	String villageName;
	String interventionProgram;
	String activityName;
	String programPeriod;

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	long id;
	long targetpoint;
	long reachpoint;
	long variance;
	long impactKey;
	long outcomeKey;
	long outputKey;
	long templateKey;
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public String getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	public String getCreatedUserName() {
		return createdUserName;
	}
	public void setCreatedUserName(String createdUserName) {
		this.createdUserName = createdUserName;
	}
	public String getModifiedUserName() {
		return modifiedUserName;
	}
	public void setModifiedUserName(String modifiedUserName) {
		this.modifiedUserName = modifiedUserName;
	}
	public Date getSetting_date() {
		return setting_date;
	}
	public void setSetting_date(Date setting_date) {
		this.setting_date = setting_date;
	}
	public int getRecord_status() {
		return record_status;
	}
	public void setRecord_status(int record_status) {
		this.record_status = record_status;
	}
	public String getSettingDate() {
		return settingDate;
	}
	public void setSettingDate(String settingDate) {
		this.settingDate = settingDate;
	}
	public String getImpactcode() {
		return impactcode;
	}
	public void setImpactcode(String impactcode) {
		this.impactcode = impactcode;
	}
	public String getOutputcode() {
		return outputcode;
	}
	public void setOutputcode(String outputcode) {
		this.outputcode = outputcode;
	}
	public String getOutcomecode() {
		return outcomecode;
	}
	public void setOutcomecode(String outcomecode) {
		this.outcomecode = outcomecode;
	}
	public String getTemplate() {
		return template;
	}
	public void setTemplate(String template) {
		this.template = template;
	}
	public String getTownshipCode() {
		return townshipCode;
	}
	public void setTownshipCode(String townshipCode) {
		this.townshipCode = townshipCode;
	}
	public String getTownshipName() {
		return townshipName;
	}
	public void setTownshipName(String townshipName) {
		this.townshipName = townshipName;
	}
	public String getVillageCode() {
		return villageCode;
	}
	public void setVillageCode(String villageCode) {
		this.villageCode = villageCode;
	}
	public String getVillageName() {
		return villageName;
	}
	public void setVillageName(String villageName) {
		this.villageName = villageName;
	}
	public String getInterventionProgram() {
		return interventionProgram;
	}
	public void setInterventionProgram(String interventionProgram) {
		this.interventionProgram = interventionProgram;
	}
	public String getActivityName() {
		return activityName;
	}
	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}
	public String getProgramPeriod() {
		return programPeriod;
	}
	public void setProgramPeriod(String programPeriod) {
		this.programPeriod = programPeriod;
	}
	public long getTargetpoint() {
		return targetpoint;
	}
	public void setTargetpoint(long targetpoint) {
		this.targetpoint = targetpoint;
	}
	public long getReachpoint() {
		return reachpoint;
	}
	public void setReachpoint(long reachpoint) {
		this.reachpoint = reachpoint;
	}
	public long getVariance() {
		return variance;
	}
	public void setVariance(long variance) {
		this.variance = variance;
	}
	public long getImpactKey() {
		return impactKey;
	}
	public void setImpactKey(long impactKey) {
		this.impactKey = impactKey;
	}
	public long getOutcomeKey() {
		return outcomeKey;
	}
	public void setOutcomeKey(long outcomeKey) {
		this.outcomeKey = outcomeKey;
	}
	public long getOutputKey() {
		return outputKey;
	}
	public void setOutputKey(long outputKey) {
		this.outputKey = outputKey;
	}
	public long getTemplateKey() {
		return templateKey;
	}
	public void setTemplateKey(long templateKey) {
		this.templateKey = templateKey;
	}
	public ActivityData() {
		super();
		
		this.activityName = "";
		this.createdDate = "";
		this.modifiedDate = "";
		this.createdUserName = "";
		this.record_status = 0;
		this.modifiedUserName= "";
		this.id = 0;
		this.settingDate = "";
		this.outputcode = "";
		this.outcomecode = "";
		this.template = "";
		this.targetpoint = 0;
		this.reachpoint = 0;
		this.variance = 0;
		this.impactcode = "";
		this.impactKey = 0;
		this.outputKey = 0;
		this.outcomeKey = 0;
		this.templateKey = 0;
		this.villageCode = "";
		this.villageName = "";
		this.townshipCode = "";
		this.townshipName = "";
		this.programPeriod = "";
		
	}

}
