package com.stc.centraldatabase.service.user;

import java.util.List;

import com.stc.centraldatabase.model.user.PasswordData;
import com.stc.centraldatabase.model.user.UserInfo;
import com.stc.centraldatabase.model.user.UserSearchData;
import com.stc.centraldatabase.model.user.UserSearchPaginateData;
import com.stc.centraldatatbase.util.SetupData;


public interface UserService {
	
	UserInfo getUser(String id , String password);
	
	boolean userInsert(UserInfo user);
	
	List<SetupData> getRoleData();
	
	UserInfo getUserbyId(String userid);
	
	boolean delete(String userid);
	
	boolean updatePassword(PasswordData pwd);
	
	boolean validateUserUpdate(String userid , long id);
	
	boolean update(UserInfo user);
	
	UserSearchPaginateData find(UserSearchData userSearch);

}
