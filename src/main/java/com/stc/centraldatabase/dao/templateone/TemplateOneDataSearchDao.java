package com.stc.centraldatabase.dao.templateone;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.stc.centraldatabase.model.templateone.TemplateOneData;
import com.stc.centraldatabase.model.templateone.TemplateOneSearchPaginateData;
import com.stc.centraldatabase.model.templateone.TemplateSearchData;
import com.stc.centraldatatbase.util.DataBaseConnection;
import com.stc.centraldatatbase.util.PrjUtil;

public class TemplateOneDataSearchDao 
{
	public  String getCriteria(TemplateSearchData criteria)
	{
		String filter = " where 1=1";
		
		if(!criteria.getCode().equals(""))
			
		{
			filter += " and code like '%"+criteria.getCode()+"%'";
			
		}
		
		if(!criteria.getDesc().equals(""))
			
		{
			filter += " and description like '%"+criteria.getDesc()+"%'";
			
		}
		if(!criteria.getParentid().equals(""))
		{
			filter += " and parent_id = "+criteria.getParentid();
		}
		if(criteria.getLimit() != 0)
			filter += " limit "+criteria.getOffset()+","+criteria.getLimit();

		return filter;
	}
	
	public static String getCountCriteria(TemplateSearchData criteria)
	{
		String filter = " where 1=1";
		
		if(!criteria.getCode().equals(""))
			
		{
			filter += " and code like '%"+criteria.getCode()+"%'";
			
		}
		
		if(!criteria.getDesc().equals(""))
			
		{
			filter += " and Description like '%"+criteria.getDesc()+"%'";
			
		}
		if(!criteria.getParentid().equals(""))
		{
			filter += " and parent_id = "+criteria.getParentid();
		}
		return filter;
	}
	
	public boolean validateSettingUpdate(String id,String code ) {
		PreparedStatement ps = null;
		try {
			Connection con = DataBaseConnection.getConnection();
			ps = con.prepareStatement("Select * from template_one where code = ? and parent_id <> ?");
			ps.setString(1, code);
			ps.setString(2, id);

			ResultSet rs = ps.executeQuery();

			if (rs.next()) 
			{
				return false;
			}
			else return true;
		} catch (SQLException ex) {
			System.out.println("Validation error -->" + ex.getMessage());
			return false;
		} 
		
	}
	
	public  int getTotalCount(TemplateSearchData search,Connection con)
	{
		int count = 0;
		String filter = getCountCriteria(search);
		String query = "Select count(*) as count from TEMPLATE_ONE "+ filter;
		Statement st;
		try {
			st = con.createStatement();
			ResultSet rs = st.executeQuery(query);
			if(rs.next())
				count = rs.getInt("count");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 return count;
	}
	
	public  TemplateOneSearchPaginateData find(TemplateSearchData usearch)
	{
		TemplateOneSearchPaginateData res = new TemplateOneSearchPaginateData();
		String filter = getCriteria(usearch);
		List<TemplateOneData> resList = new ArrayList<TemplateOneData>();
		Connection con = null;
		TemplateOneData setting;
		String query = "Select * from TEMPLATE_ONE" +filter;
		
		
		try {
			con = DataBaseConnection.getConnection();
			Statement st = con.createStatement();
			 ResultSet rs = st.executeQuery(query);
			 
			 while(rs.next())
			 {
				 setting = new TemplateOneData();
				 setting.setId(rs.getInt("id"));
				 setting.setSettingDate(PrjUtil.StringtoDate(rs.getString("setting_date")));
				 
				 setting.setCode(rs.getString("code"));
				 setting.setDesc(rs.getString("description"));
				 setting.setParentid(rs.getInt("parent_id"));
				 setting.setNoOfHouse(rs.getInt("no_of_house"));
				 setting.setNoOfHouseHold(rs.getInt("no_of_household"));
				 setting.setMaleLessthan5YearsCount(rs.getInt("male_less_5"));
				 setting.setFemaleLessthan5YearsCount(rs.getInt("female_less_5"));
				 setting.setMale5T18Count(rs.getInt("male_btw_5_18"));
				 setting.setFemale5T18Count(rs.getInt("female_btw_5_18"));
				 setting.setMaleOver18Count(rs.getInt("male_over_18"));
				 setting.setFemaleOver18Count(rs.getInt("female_over_18"));	
				 setting.setSettingDateInDateFormat(PrjUtil.datetimeTostring(rs.getString("setting_date")));
				 
				 resList.add(setting);
			 }
			
			res.setCount(getTotalCount(usearch,con));
			res.setTemplateOneDataList(resList);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			DataBaseConnection.close(con);
		}
			
		return res;
	}
	
	
	public TemplateOneData findById(String id)
	{	
		Connection con = null;
		TemplateOneData setting = null;
		String query = "Select * from template_one where id="+id ;
		
		
		try {
			con = DataBaseConnection.getConnection();
			Statement st = con.createStatement();
			 ResultSet rs = st.executeQuery(query);
			 
			 while(rs.next())
			 {
				 setting = new TemplateOneData();
				 setting.setId(rs.getInt("id"));
				 setting.setSettingDate(PrjUtil.StringtoDate(rs.getString("setting_date")));
				 setting.setCode(rs.getString("code"));
				 setting.setDesc(rs.getString("description"));
				 setting.setParentid(rs.getInt("parent_id"));
				 setting.setNoOfHouse(rs.getInt("no_of_house"));
				 setting.setNoOfHouseHold(rs.getInt("no_of_household"));
				 setting.setMaleLessthan5YearsCount(rs.getInt("male_less_5"));
				 setting.setFemaleLessthan5YearsCount(rs.getInt("female_less_5"));
				 setting.setMale5T18Count(rs.getInt("male_btw_5_18"));
				 setting.setFemale5T18Count(rs.getInt("female_btw_5_18"));
				 setting.setMaleOver18Count(rs.getInt("male_over_18"));
				 setting.setFemaleOver18Count(rs.getInt("female_over_18"));
				 setting.setTotalFemaleCount(setting.getFemaleLessthan5YearsCount() + setting.getFemale5T18Count() + setting.getFemaleOver18Count());
				 setting.setTotalMaleCount(setting.getMaleLessthan5YearsCount() + setting.getMale5T18Count() + setting.getMaleOver18Count());
			 }
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			DataBaseConnection.close(con);
		}
			
		 return setting;
	}
	
	
}
