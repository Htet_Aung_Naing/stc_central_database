package com.stc.centraldatabase.mgr.templatesix;



import com.stc.centraldatabase.dao.templatesix.TemplateSixDataDao;
import com.stc.centraldatabase.dao.templatesix.TemplateSixDataSearchDao;
import com.stc.centraldatabase.model.templateone.TemplateSearchData;
import com.stc.centraldatabase.model.templatesix.TemplateSixData;
import com.stc.centraldatabase.model.templatesix.TemplateSixSearchPaginateData;

public class TemplateSixMgr 
{
	public static boolean inserData(TemplateSixData template)
	{
		TemplateSixDataDao templateFiveDao = new TemplateSixDataDao();
		return templateFiveDao.insertTemplateSixData(template);
	}
	
	public static boolean updateData(TemplateSixData activity)
	{
		TemplateSixDataDao templateFiveDao = new TemplateSixDataDao();
		return templateFiveDao.updateTemplateSixData(activity);
	}
	
	public static TemplateSixSearchPaginateData find(TemplateSearchData settingSearch)
	{
		TemplateSixDataSearchDao settingSearchDao = new TemplateSixDataSearchDao();
		return settingSearchDao.find(settingSearch);
	}
	
	public static TemplateSixData findById(String id)
	{
		TemplateSixDataSearchDao activitySearchDao = new TemplateSixDataSearchDao();
		return activitySearchDao.findById(id);
	}
	
	public static boolean validateUpdate(String id,String code)
	{
		TemplateSixDataSearchDao activitySearchDao = new TemplateSixDataSearchDao();
		return activitySearchDao.validateSettingUpdate(id, code);
	}

	public static boolean delete(String id)
	{
		TemplateSixDataDao templateFiveDao = new TemplateSixDataDao();
		return templateFiveDao.delete(id);
	}
}
