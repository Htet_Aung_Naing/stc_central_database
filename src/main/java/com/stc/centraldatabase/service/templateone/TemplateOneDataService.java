package com.stc.centraldatabase.service.templateone;

import com.stc.centraldatabase.model.templateone.TemplateOneData;
import com.stc.centraldatabase.model.templateone.TemplateOneSearchPaginateData;
import com.stc.centraldatabase.model.templateone.TemplateSearchData;

public interface TemplateOneDataService {
	
	
boolean insertTemplateOneData(TemplateOneData activity);
	
	boolean updateData(TemplateOneData activity);
	
	TemplateOneSearchPaginateData find(TemplateSearchData settingSearch );
	
	TemplateOneData findById(String id);
	
	boolean validateTemplateDataUpdate(String id, String code);
	
	boolean delete(String activityId);

}
