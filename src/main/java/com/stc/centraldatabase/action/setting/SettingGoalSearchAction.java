package com.stc.centraldatabase.action.setting;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.primefaces.component.datatable.DataTable;

import com.stc.centraldatabase.model.setting.LazySettingDataModel;
import com.stc.centraldatabase.model.setting.SettingSearchData;
import com.stc.centraldatatbase.util.SessionUtil;
import com.stc.centraldatatbase.util.SetupData;





@ManagedBean(name = "settingGoalSearchAction")
@ViewScoped
public class SettingGoalSearchAction implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6474306882073540913L;
	
	SettingSearchData settingSearchData;
	List<SetupData>roleList;
	LazySettingDataModel settingdataModel;

	public LazySettingDataModel getSettingdataModel() {
		return settingdataModel;
	}

	public void setSettingdataModel(LazySettingDataModel settingdataModel) {
		this.settingdataModel = settingdataModel;
	}

	public List<SetupData> getRoleList() {
		return roleList;
	}

	public void setRoleList(List<SetupData> roleList) {
		this.roleList = roleList;
	}


	
	public SettingSearchData getSettingSearchData() {
		return settingSearchData;
	}

	public void setSettingSearchData(SettingSearchData settingSearchData) {
		this.settingSearchData = settingSearchData;
	}


	
	public void reset(String targetDataTable) {
		resetPagination(targetDataTable);
		LoadData();
	}

	
	private void resetPagination(String targetDataTable) {
		DataTable d = (DataTable) FacesContext.getCurrentInstance().getViewRoot()
				.findComponent("settingGoalSearchForm:settingSearchTable");
		d.setFirst(0);
	}
	
	public void search(String targetDataTable) {

		// Check Joined Date From should be earlier than Joined Date To.
			// Reset pagination and do the lazy data model process.
			resetPagination(targetDataTable);
			settingdataModel = new LazySettingDataModel(settingSearchData, "setting_goal");
		
	}
	
	@PostConstruct
	public void LoadData()
	{
		settingSearchData = new SettingSearchData();
		settingdataModel = new LazySettingDataModel(settingSearchData, "setting_goal");
		
	}
	
	public String edit(String id) {
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("tbname", "setting_goal");
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("impact_id", id);
		return "settingGoalUpdate";
	}



	public String delete(String id) {
		HttpSession session = SessionUtil.getSession();
		session.setAttribute("delete_title", "Impact Delete");
		session.setAttribute("return_page", "goalSearch");
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("tbname", "setting_goal");
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("impact_id", id);
		return "settingDelete";
	}
	
	public String detail(String id) {
		HttpSession session = SessionUtil.getSession();
		session.setAttribute("detail_title", "Impact Detail");
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("tbname", "setting_goal");
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("impact_id", id);
		return "settingDetail";
	}
	





}
