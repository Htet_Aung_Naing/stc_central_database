package com.stc.centraldatabase.action.setting;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import com.stc.centraldatabase.model.setting.SettingData;
import com.stc.centraldatabase.service.setting.SettingDataService;
import com.stc.centraldatabase.service.setting.SettingDataServiceImpl;
import com.stc.centraldatatbase.util.SessionUtil;




@ManagedBean(name = "settingDeletAction")
@ViewScoped
public class SettingDeleteAction implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 767999823111101208L;
	
	String impactID;
	String title="";
	SettingData settingData;
	SettingDataService settingService;
	String tbname= "";
	String returnPage = "";
	
	public String getImpactID() {
		return impactID;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setImpactID(String impactID) {
		this.impactID = impactID;
	}

	public SettingData getSettingData() {
		return settingData;
	}

	public void setSettingData(SettingData settingData) {
		this.settingData = settingData;
	}

	@PostConstruct
	public void init()
	{
			HttpSession session = SessionUtil.getSession();
			title = (String) session.getAttribute("delete_title");
			returnPage = (String) session.getAttribute("return_page");
			settingService = new SettingDataServiceImpl();
			settingData = new SettingData();
			if(impactID == null)
			{
				this.impactID = (String) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("impact_id");
				this.tbname = (String) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("tbname");
				this.settingData = settingService.findByid(impactID, tbname);
			}
		
	}
	
	public String delete()
	{
		
		if(settingService.delete(impactID, tbname))
		{
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "INFO!", "Delete Impact Successfully!"));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			return returnPage;
		}
		else
		{
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "ERROR!", "Impact cannot delete!"));
			return null;
		}
		
	}
	
}
