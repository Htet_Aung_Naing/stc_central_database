package com.stc.centraldatabase.model.activity;

import java.io.Serializable;
import java.util.List;

public class ActivitySearchPaginateData implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8793922769046576099L;
	
	int count;
	List<ActivityData> activityDataLsit;
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public List<ActivityData> getActivityDataLsit() {
		return activityDataLsit;
	}
	public void setActivityDataLsit(List<ActivityData> activityDataLsit) {
		this.activityDataLsit = activityDataLsit;
	}
	

}
