package com.stc.centraldatabase.dao.templatethree;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Date;

import com.stc.centraldatabase.model.templatethree.TemplateThreeData;
import com.stc.centraldatatbase.util.DataBaseConnection;
import com.stc.centraldatatbase.util.PrjUtil;

public class TemplateThreeDataDao {
	
	public boolean insertTemplateThreeData(TemplateThreeData setting)
	{
		Connection con = null;
		PreparedStatement ps = null;
		

		 	try 
		 	{
				String query;
				
				if(setting != null)
				{
					con = DataBaseConnection.getConnection();
					
					
					
				     query = "INSERT INTO TEMPLATE_THREE (PARENT_ID,CODE,DESCRIPTION,CREATED_USER,CREATED_DATE,MODIFIED_USER_NAME"
				     		+ ",MODIFIED_DATE,SETTING_DATE, ENROLL_BOY , "
							+"ENROLL_GIRL ,  ENROLL_POOR_BOY ,  ENROLL_POOR_GIRL ,  ENROLL_DISABILITY_BOY ,  ENROLL_DISABILITY_GIRL ," 
							+" ENROLL_ETHNIC_BOY ,  ENROLL_ETHNIC_GIRL)"
				     		+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?,?,?)";
				     
				     
				     int i = 1;
				     ps = con.prepareStatement(query);
				     ps.setInt(i++, setting.getParentid());
				     ps.setString(i++, setting.getCode());
				     ps.setString(i++, setting.getDesc() );
				     ps.setString(i++, setting.getCreatedUserName());
				     ps.setString(i++, PrjUtil.stringToDate(new Date()));
				     ps.setString(i++, setting.getModifiedUserName());
				     ps.setString(i++, PrjUtil.stringToDate(new Date()));
				     ps.setString(i++, PrjUtil.stringToDate(setting.getSettingDate()));
				     ps.setInt(i++, setting.getEnrollBoys());
				     ps.setInt(i++, setting.getEnrollGirls());
				     ps.setInt(i++, setting.getEnrollPoorBoys());
				     ps.setInt(i++, setting.getEnrollPoorGirls());
				     ps.setInt(i++, setting.getEnrollDisabiltyBoys());
				     ps.setInt(i++, setting.getEnrollDisabilityGirls());
				     ps.setInt(i++, setting.getEnrollEthnicBoys());
				     ps.setInt(i++, setting.getEnrollEthnicGirls());
				     
				     ps.execute();
				     con.close();
				     return true; 
					   				  
				}else
				{
				
					return false;
				}
				 
			} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
	
			return false;
			
		}
				
		}
	
	public  boolean updateTemplateThreeData(TemplateThreeData setting) 
	{
		Connection con = null;
		PreparedStatement ps = null;
		String query;
		try {
			con = DataBaseConnection.getConnection();
			
		
	     query = "UPDATE TEMPLATE_THREE SET CODE=? ,DESCRIPTION=? ,MODIFIED_USER_NAME=?"
				     		+ ",MODIFIED_DATE=? ,SETTING_DATE=? , ENROLL_BOY=? , "
							+"ENROLL_GIRL=? ,  ENROLL_POOR_BOY=? ,  ENROLL_POOR_GIRL=? ,  ENROLL_DISABILITY_BOY=? ,  ENROLL_DISABILITY_GIRL=? ," 
							+" ENROLL_ETHNIC_BOY=? ,  ENROLL_ETHNIC_GIRL=? WHERE ID = ?";
			     		
			    int i = 1;
				ps = con.prepareStatement(query);
				ps.setString(i++ , setting.getCode());
				ps.setString(i++ , setting.getDesc());				
				ps.setString(i++, setting.getModifiedUserName());
				ps.setString(i++, PrjUtil.stringToDate(new Date()));
				ps.setString(i++, PrjUtil.stringToDate(setting.getSettingDate()));
				ps.setInt(i++, setting.getEnrollBoys());
				ps.setInt(i++, setting.getEnrollGirls());
				ps.setInt(i++, setting.getEnrollPoorBoys());
				ps.setInt(i++, setting.getEnrollPoorGirls());
				ps.setInt(i++, setting.getEnrollDisabiltyBoys());
				ps.setInt(i++, setting.getEnrollDisabilityGirls());
				ps.setInt(i++, setting.getEnrollEthnicBoys());
				ps.setInt(i++, setting.getEnrollEthnicGirls());
				ps.setLong(i++, setting.getId());
				ps.execute();
			      
			    con.close();
			    return true;
			
			
			
		} catch (SQLException ex) {
			System.out.println("Templage Three Data Registration Update error -->" + ex.getMessage());
			return false;
		} finally {
			DataBaseConnection.close(con);
		}
	}
	
	public  boolean delete(String settingid) {
		Connection con = null;
		PreparedStatement ps = null;
		String query;
		try {
			con = DataBaseConnection.getConnection();
			    	        
			     query = "DELETE FROM TEMPLATE_THREE where id=?";
			    int i = 1;
				ps = con.prepareStatement(query);
				ps.setString(i++, settingid);
				ps.execute();
			      
			    con.close();
			    return true;
			
			
			
		} catch (SQLException ex) {
			System.out.println("Template three Delete error -->" + ex.getMessage());
			return false;
		} finally {
			DataBaseConnection.close(con);
		}
	}

}
