package com.stc.centraldatabase.model.templateone;

import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.stc.centraldatabase.service.templateone.TemplateOneDataService;
import com.stc.centraldatabase.service.templateone.TemplateOneDataServiceImpl;






public class LazyTemplateoneDataModel extends LazyDataModel<TemplateOneData>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7167457755325524229L;
	/**
	 * 
	 */


	
	TemplateSearchData settingSearchData;
	TemplateOneDataService settingService;
	String tbname = "";


	

	public TemplateSearchData getSettingSearchData() {
		return settingSearchData;
	}

	public void setSettingSearchData(TemplateSearchData settingSearchData) {
		this.settingSearchData = settingSearchData;
	}

	public  LazyTemplateoneDataModel(TemplateSearchData settingSearchData) {
		this.settingSearchData = settingSearchData;
	}

	@Override
	public List<TemplateOneData> load(int first, int pageSize, String sortField,
			SortOrder sortOrder, Map<String, Object> filters) {
		settingService = new TemplateOneDataServiceImpl();
		
		settingSearchData.setOffset(first);
		settingSearchData.setLimit(pageSize);
		
		TemplateOneSearchPaginateData userPaginateData = settingService.find(settingSearchData);
		
		setRowCount(userPaginateData.getCount());
		setPageSize(settingSearchData.getLimit());
		
		List<TemplateOneData> userList = userPaginateData.getTemplateOneDataList();
		
		return userList;
	}

}
