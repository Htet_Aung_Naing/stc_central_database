package com.stc.centraldatabase.action.setting;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import com.stc.centraldatabase.model.setting.SettingData;
import com.stc.centraldatabase.service.setting.SettingDataService;
import com.stc.centraldatabase.service.setting.SettingDataServiceImpl;
import com.stc.centraldatatbase.util.SessionUtil;



@ManagedBean(name = "settingOutputAction")
@ViewScoped
public class SettingOutputAction implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8960748410953236774L;

	SettingDataService settingService;
	DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
	List<SettingData> settingImpactList;
	List<SettingData> settingOutcomeList;
	String title = "";

	
	public List<SettingData> getSettingOutcomeList() {
		return settingOutcomeList;
	}

	public void setSettingOutcomeList(List<SettingData> settingOutcomeList) {
		this.settingOutcomeList = settingOutcomeList;
	}

	public List<SettingData> getSettingImpactList() {
		return settingImpactList;
	}

	public void setSettingImpactList(List<SettingData> settingImpactList) {
		this.settingImpactList = settingImpactList;
	}

	@ManagedProperty(value = "#{setting}")
	SettingData setting;
	
	


	public SettingData getSetting() {
		return setting;
	}

	public void setSetting(SettingData setting) {
		this.setting = setting;
	}

	@PostConstruct
	public void init()  
	{
		this.setting = new SettingData();	
		this.settingService  = new SettingDataServiceImpl();
		this.settingImpactList = settingService.getSettingList("setting_goal");
		this.settingOutcomeList = settingService.getSettingList("setting_outcome");

	}
	
	public String getSettingmenuLabel(List<SettingData> settingList , long key)
	{
		String res = "";
		
		for (SettingData settingData : settingList) 
		{
			if(key == settingData.getId())
			{
				res = settingData.getT1();
				break;
			}
				
		}
		return res;
	}
	
	public String getOutputSettingmenuLabel(List<SettingData> settingList , long key)
	{
		String res = "";
		
		for (SettingData settingData : settingList) 
		{
			if(key == settingData.getId())
			{
				res = settingData.getT1();
				break;
			}
				
		}
		return res;
	}
	
	
	public String register()
	{
		HttpSession session = SessionUtil.getSession();
		setting.setCreatedUserName((String) session.getAttribute("userid"));
		setting.setModifiedUserName((String) session.getAttribute("userid"));
		session.setAttribute("insert_success_title", "Output Register Successfully!");
		if(settingService.insertSettingData(setting,"setting_output"))
		{	
			setting.setImpactcode(getSettingmenuLabel(settingImpactList, setting.getN1()));
			setting.setOutputcode(getOutputSettingmenuLabel(settingOutcomeList, setting.getN2()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().put("settingData", setting);
			return "goalRegistrationSuccess";
		}			
		else
		{
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "ERROR!", "Setting's Codes are duplicate!"));
			return null;
		}
	}
	
}
