
package com.stc.centraldatabase.model.setting;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

public class SettingData implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3610707976609136739L;
	
	long id;
	String t1;
	String t2;
	String t3;
	String t4;
	String t5;
	long n1;
	long n2;
	long n3;
	long n4;
	long n5;
	String createdDate;
	String modifiedDate;
	String createdUserName;
	String modifiedUserName;
	Date setting_date;
	int record_status;
	String settingDate;
	String impactcode;
	String outputcode;
	String outcomecode;
	String template;
	

	public String getTemplate() {
		return template;
	}
	public void setTemplate(String template) {
		this.template = template;
	}
	public String getOutcomecode() {
		return outcomecode;
	}
	public void setOutcomecode(String outcomecode) {
		this.outcomecode = outcomecode;
	}
	public String getOutputcode() {
		return outputcode;
	}
	public void setOutputcode(String outputcode) {
		this.outputcode = outputcode;
	}
	public String getImpactcode() {
		return impactcode;
	}
	public void setImpactcode(String impactcode) {
		this.impactcode = impactcode;
	}
	public String getSettingDate() {
		return settingDate;
	}
	public void setSettingDate(String settingDate) {
		this.settingDate = settingDate;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public int getRecord_status() {
		return record_status;
	}
	public void setRecord_status(int record_status) {
		this.record_status = record_status;
	}
	public String getT1() {
		return t1;
	}
	public void setT1(String t1) {
		this.t1 = t1;
	}
	public String getT2() {
		return t2;
	}
	public void setT2(String t2) {
		this.t2 = t2;
	}
	public String getT3() {
		return t3;
	}
	public void setT3(String t3) {
		this.t3 = t3;
	}
	public String getT4() {
		return t4;
	}
	public void setT4(String t4) {
		this.t4 = t4;
	}
	public String getT5() {
		return t5;
	}
	public void setT5(String t5) {
		this.t5 = t5;
	}
	public long getN1() {
		return n1;
	}
	public void setN1(long n1) {
		this.n1 = n1;
	}
	public long getN2() {
		return n2;
	}
	public void setN2(long n2) {
		this.n2 = n2;
	}
	public long getN3() {
		return n3;
	}
	public void setN3(long n3) {
		this.n3 = n3;
	}
	public long getN4() {
		return n4;
	}
	public void setN4(long n4) {
		this.n4 = n4;
	}
	public long getN5() {
		return n5;
	}
	public void setN5(long n5) {
		this.n5 = n5;
	}
	
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public String getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	public String getCreatedUserName() {
		return createdUserName;
	}
	public void setCreatedUserName(String createdUserName) {
		this.createdUserName = createdUserName;
	}
	public String getModifiedUserName() {
		return modifiedUserName;
	}
	public void setModifiedUserName(String modifiedUserName) {
		this.modifiedUserName = modifiedUserName;
	}
	public Date getSetting_date() {
		return setting_date;
	}
	public void setSetting_date(Date setting_date) {
		this.setting_date = setting_date;
	}
	public SettingData()
	{
		Calendar calendar = Calendar.getInstance();
		java.sql.Date startDate = new java.sql.Date(calendar.getTime().getTime());
		this.t1 = "";
		this.t2 = "";
		this.t3 = "";
		this.t4 = "";
		this.t5 = "";
		this.n1 = 0;
		this.n2 = 0;
		this.n3 = 0;
		this.n4 = 0;
		this.n5 = 0;
		this.setting_date = startDate;
		this.createdDate = "";
		this.modifiedDate = "";
		this.createdUserName = "";
		this.record_status = 0;
		this.modifiedUserName= "";
		this.id = 0;
		this.settingDate = "";
		this.outputcode = "";
		this.outcomecode = "";
		this.template = "";
	}
	

}
