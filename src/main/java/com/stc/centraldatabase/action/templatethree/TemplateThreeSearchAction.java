package com.stc.centraldatabase.action.templatethree;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.component.datatable.DataTable;

import com.stc.centraldatabase.model.templateone.TemplateSearchData;
import com.stc.centraldatabase.model.templatethree.LazyTemplateThreeDataModel;
import com.stc.centraldatabase.service.templatethree.TemplateThreeDataService;
import com.stc.centraldatabase.service.templatethree.TemplateThreeDataServiceImpl;
import com.stc.centraldatatbase.util.CommonEnum;





@ManagedBean(name = "templateThreeSearchAction")
@ViewScoped
public class TemplateThreeSearchAction implements Serializable{


	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6499344160282589714L;
	
	TemplateSearchData templateSearchData;
	String title;
	String parentid = "";
	LazyTemplateThreeDataModel settingdataModel;
	TemplateThreeDataService settingService;



	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public TemplateSearchData getTemplateSearchData() {
		return templateSearchData;
	}

	public void setTemplateSearchData(TemplateSearchData templateSearchData) {
		this.templateSearchData = templateSearchData;
	}


	public LazyTemplateThreeDataModel getSettingdataModel() {
		return settingdataModel;
	}

	public void setSettingdataModel(LazyTemplateThreeDataModel settingdataModel) {
		this.settingdataModel = settingdataModel;
	}

	public void reset(String targetDataTable) {
		resetPagination(targetDataTable);
		LoadData();
	}

	
	private void resetPagination(String targetDataTable) {
		DataTable d = (DataTable) FacesContext.getCurrentInstance().getViewRoot()
				.findComponent("settingGoalSearchForm:settingSearchTable");
		d.setFirst(0);
	}
	
	public void search(String targetDataTable) {

		// Check Joined Date From should be earlier than Joined Date To.
			// Reset pagination and do the lazy data model process.
			resetPagination(targetDataTable);
			settingdataModel = new LazyTemplateThreeDataModel(templateSearchData);
		
	}
	
	public String getTemplatePage(long id)
	{
		String res = "";
		for(CommonEnum.Template_Page g : CommonEnum.Template_Page.values())
		{
			if(g.value() == id)
			{
				res = g.description();
				break;
			}
			
		}
		return res;
	}
	
	@PostConstruct
	public void LoadData()
	{
		settingService = new TemplateThreeDataServiceImpl();
		templateSearchData = new TemplateSearchData();
		if(this.parentid == null)
			this.parentid = (String) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("templateParentid");
		if(this.title == null)
			this.title =  (String) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("templateTitle");
		templateSearchData.setParentid(parentid);
		settingdataModel = new LazyTemplateThreeDataModel(templateSearchData);
		
	}
	
	public String edit(String id , String code) {
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateid", id);
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateCode", code);
		return "template_three_update";
	}



	public String delete(String id) {
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateid", id);
		
		return "template_three_delete";
	}
	
	public String detail(String id) {
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateid", id);
		return "template_three_detail";
	}
	
	
	

}
