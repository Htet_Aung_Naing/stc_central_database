package com.stc.centraldatatbase.util;



import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class PrjUtil {

	public static int mLastIndx;
	public static final String mGSRegx= "[^A-Za-z0-9]{1}[\\d]+";
	public static final String REGEX_EMAIL = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.(?:[a-zA-Z]{2,6})$";
	public static final boolean mPreFlag = false;
	public static final boolean mRealFlag = true;

	public static boolean is12TimeFormat(String aTime){
		boolean l_ret = false;
		String time = "(1[012]|[1-9]):[0-5][0-9][\\s](am|pm)";

		if(aTime.matches(time)){
			l_ret = true;
		}
		return l_ret;
	}
	public static boolean match(String aRegex, String aValue)
	{
		boolean l_result = false;
		l_result = aValue.matches(aRegex);		
		return l_result;
	}

	public static Map<String, Object> getDialogOption()
	{
		Map<String, Object> dialogOptions;
		dialogOptions = new HashMap<String, Object>();
		dialogOptions.put("modal", true);
		dialogOptions.put("draggable", false);
		dialogOptions.put("resizable", true);
		dialogOptions.put("contentHeight", 900);
		dialogOptions.put("contentWidth", 800);
		//dialogOptions.put("header", "Header");
	return dialogOptions;
	}

	public static String formatHHMMSS(String p){
		String ret = "";
		ret = p.substring(0,2)+":"+p.substring(2,4)+":"+p.substring(4,6);
		return ret;
	}




	public static String datetimeTostring(String p_date){
		String ret="";
		p_date = p_date.replace("-", "");
		try{
			ret = p_date.substring(6, 8) + "/" + p_date.substring(4, 6) + "/" + p_date.substring(0, 4) ;
		} catch (Exception e){
			e.printStackTrace();
		}
		return ret;
	}



	public static String datetoString(String aDate)
	{
		String l_date = "";

		String [] l_arr = aDate.split("/");
		l_date = l_arr[2] + l_arr[1] + l_arr[0];

		return l_date;
	}
	public static String datetoStringForTextFile(String aDate)
	{
		String l_date = "";

		String [] l_arr = aDate.split("/");
		l_date=l_arr[0]+l_arr[1]+l_arr[2];

		return l_date;
	}

	@SuppressWarnings("deprecation")
	public static java.util.Date stringtoDate(String aDate)
	{
		int l_year = Integer.parseInt(aDate.substring(0, 4));
		int l_month = Integer.parseInt(aDate.substring(4, 6));
		int l_date = Integer.parseInt(aDate.substring(6));

		Date l_Date = new Date(l_year - 1900, l_month - 1, l_date);

		return l_Date;
	}

	@SuppressWarnings("deprecation")
	public static java.util.Date stringtoDateddmmyy(String aDate)
	{
		int l_date = Integer.parseInt(aDate.substring(0,2));
		int l_month = Integer.parseInt(aDate.substring(3, 5));
		int l_year = Integer.parseInt(aDate.substring(7));		

		Date l_Date = new Date(l_year - 1900, l_month - 1, l_date);

		return l_Date;
	}

	public static String stringtoDateInDateFormat(String aDate)
	{
		String l_Date = "";
		if(!aDate.equals("")){
			l_Date = aDate.substring(6) + "/" + aDate.substring(4, 6) + "/" + aDate.substring(0 ,4);
		}
		return l_Date;
	}

	public static String dateTimeToDateFormatString(String aDate){
		String l_Date = ""; 
		if(!aDate.equals("")){
			l_Date = aDate.substring(0 ,4)+aDate.substring(4, 6)+aDate.substring(6,8) ;
		}
		return l_Date;
	}
	public static String stringTodateTimeFormat(String aDate)
	{
		String l_Date = ""; 	
		String s="";
		String m="";
		String h="";
		String ap="";
		if(!aDate.equals("")){
			h= aDate.substring(8,10);
			m=aDate.substring(10,12);
			s=aDate.substring(12,14);
			ap=aDate.substring(14);
			l_Date = aDate.substring(6,8) + "/" + aDate.substring(4, 6) + "/" + aDate.substring(0 ,4) +" "+h+":"+m+":"+s+ ap;
		}
		return l_Date;
	}	

	public static String stringToTimeFormat(String aDate)
	{
		String l_Date = ""; 	


		String time="";

		if(!aDate.equals("")){
			time= aDate.substring(11,19);
			l_Date = time;
		}
		return l_Date;
	}	

	public static boolean isyyyyMMddFormart(String aDate){
		boolean l_ret = false;
		if(aDate.matches("[\\d]{8}")){
			l_ret = true;
		}
		return l_ret;
	}

	public static boolean isddMMyyyFormat(String aDate){
		boolean l_ret = false;
		String date = "(0?[1-9]|[12][0-9]|3[01])/(0?[1-9]|1[012])/((19|20)\\d\\d)";
		if(aDate.matches(date)){
			l_ret = true;
		}
		return l_ret;
	}
	public static boolean isTimeFormat(String aTime){
		boolean l_ret = false;
		String time = "([01]?[0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]";
		if(aTime.matches(time)){
			l_ret = true;
		}
		return l_ret;
	}
	public static void main(String[] args){
		System.out.println("Days:::"+getDiffDate("20141226", "20150125"));//30
		System.out.println("Format:::"+datetoString("27/05/2015"));//20150527
		System.out.println("Change Format to Date:::"+stringtoDate(datetoString("27/05/2015")));//Wed May 27 00:00:00 MMT 2015
		System.out.println("Change Format to Date:::"+dateTimeToDateFormatString("20150527"));//20150527
		System.out.println("Change Format to Date:::"+datetimeTostring("20150527"));//27/05/2015
		System.out.println("Change Format to Date:::"+stringtoDateInDateFormat("20150527"));//27/05/2015
	}

	public static boolean isNumber(String p) {
		boolean ret = false;
		try{
			if( p.matches("[-|+]?([0-9]*\\.?[0-9]*)"))
				ret = true;

		} catch (Exception ex){}
		return ret;
	}
	public static boolean isNumberformat(String p){
		boolean ret=false;
		if(p.matches("[-]?\\d*"))
			ret=true;
		return ret;
	}
	public static boolean isFloating(String p) {
		boolean ret = false;
		try{    		
			if(p.matches("([0-9]*\\.?[0-9]*)"))
				ret=true;

		} catch (Exception ex){}
		return ret;
	}

	public static String getStartZero(int aZeroCount, String aValue)
	{
		while (aValue.length() < aZeroCount) {
			aValue = "0" + aValue;
		}
		return aValue;
	}


	


	@SuppressWarnings("deprecation")
	public static Date getNextDay(String p_date)
	{	
		int l_year = Integer.parseInt(p_date.substring(0, 4));
		int l_month = Integer.parseInt(p_date.substring(4, 6));
		int l_day = Integer.parseInt(p_date.substring(6))+1;

		Date l_Date = new Date(l_year - 1900, l_month - 1, l_day);
		return l_Date;
	}

	@SuppressWarnings("deprecation")
	public static int getDiffDate(String aFDate,String aTDate)
	{
		Date startDate = new Date(Integer.parseInt(aFDate.substring(0, 4)),Integer.parseInt(aFDate.substring(4 ,6))-1, Integer.parseInt(aFDate.substring(6)));
		Date endDate = new Date(Integer.parseInt(aTDate.substring(0, 4)),Integer.parseInt(aTDate.substring(4 ,6))-1, Integer.parseInt(aTDate.substring(6)));
		int difInDays = (int) (((endDate.getTime() - startDate.getTime())/(1000*60*60*24)));
		return difInDays;
	}




	public static int calculateAge(String dateOfBirth){
		int age = 0;
		/*Calendar born = Calendar.getInstance();
	    Calendar now = Calendar.getInstance();
	    if(dateOfBirth!= null) {
	        now.setTime(new Date());
	        born.setTime(ClientUtil.stringtoDate(dateOfBirth));  
	        if(born.after(now)) {
	            throw new IllegalArgumentException("Can't be born in the future");
	        }
	        age = now.get(Calendar.YEAR) - born.get(Calendar.YEAR);             
	        if(now.get(Calendar.DAY_OF_YEAR) < born.get(Calendar.DAY_OF_YEAR))  {
	            age-=1;
	        }
	    }  */
		return age;
	}

	@SuppressWarnings("deprecation")
	public static int getAge(String pDate) {
		int year=0, month=0, day=0;
		year = Integer.parseInt(pDate.substring(0, 4));
		month = Integer.parseInt(pDate.substring(4, 6));
		day = Integer.parseInt(pDate.substring(6,8));
		Date now = new Date();
		int nowDay = now.getDate();
		int nowMonth = now.getMonth()+1;
		int nowYear = now.getYear()+1900;
		int result = nowYear - year;

		if (nowMonth < month) {
			result--;
		}
		else if (month == nowMonth && day > nowDay) {
			result--;
		}
		return result;
	}


	public static int compareTwoDates(String date1, String date2){	//add hsd															
		//	String d1 = ServerUtil.datetoString(date1);															
		//String d2 = ServerUtil.datetoString(date2);	
		System.out.println("Test"+date1);
		Date dat1= StringtoDate(date1);															
		Date dat2= StringtoDate(date2);															
		return dat1.compareTo(dat2);															
	}
	
	public static String stringToDate(Date date)
	{
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		return sdf.format(date);
	}
	
	@SuppressWarnings("deprecation")
	public static java.util.Date StringtoDate(String aDate)//add hsd
	{
		int l_year = Integer.parseInt(aDate.substring(0, 4));
		int l_month = Integer.parseInt(aDate.substring(4, 6));

		int l_date = Integer.parseInt(aDate.substring(6));

		Date l_Date = new Date(l_year - 1900, l_month - 1, l_date);

		return l_Date;
	}


}