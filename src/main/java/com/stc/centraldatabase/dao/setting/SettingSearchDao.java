package com.stc.centraldatabase.dao.setting;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.stc.centraldatabase.model.setting.SettingData;
import com.stc.centraldatabase.model.setting.SettingSearchData;
import com.stc.centraldatabase.model.setting.SettingSearchPaginateData;
import com.stc.centraldatatbase.util.CommonEnum;
import com.stc.centraldatatbase.util.DataBaseConnection;
import com.stc.centraldatatbase.util.PrjUtil;




public class SettingSearchDao {
	
	public  SettingSearchPaginateData find(SettingSearchData usearch , String tbname)
	{
		SettingSearchPaginateData res = new SettingSearchPaginateData();
		String filter = getCriteria(usearch);
		List<SettingData> resList = new ArrayList<SettingData>();
		Connection con = null;
		SettingData setting;
		String query = "Select id,setting_date,t1,t2,t3,t4,t5,n1,n2,n3,n4,n5 from "+ tbname +filter;
		
		
		try {
			con = DataBaseConnection.getConnection();
			Statement st = con.createStatement();
			 ResultSet rs = st.executeQuery(query);
			 
			 while(rs.next())
			 {
				 setting = new SettingData();
				 setting.setId(rs.getInt("id"));
				 setting.setSetting_date(PrjUtil.StringtoDate(rs.getString("setting_date")));
				 setting.setSettingDate(PrjUtil.datetimeTostring(rs.getString("setting_date")));
				 setting.setT1(rs.getString("t1"));
				 setting.setT2(rs.getString("t2"));
				 setting.setT3(rs.getString("t3"));
				 setting.setT4(rs.getString("t4"));
				 setting.setT5(rs.getString("t5"));
				 setting.setN1(rs.getLong("n1"));
				 setting.setN2(rs.getLong("n2"));
				 setting.setN3(rs.getLong("n3"));
				 setting.setN4(rs.getLong("n4"));
				 setting.setN5(rs.getLong("n5"));
				 
				 if(setting.getN1()!=0)
				 {
					 SettingSearchDao settingSearch = new SettingSearchDao();
					 SettingData impactSetting = settingSearch.getSettingById(String.valueOf(setting.getN1()), "setting_goal");
					 if(impactSetting != null)
					 {
						 setting.setImpactcode(impactSetting.getT1());
					 }
				 }
				 
				 if(setting.getN2()!=0)
				 {
					 SettingSearchDao settingSearch = new SettingSearchDao();
					 SettingData outcomeSetting = settingSearch.getSettingById(String.valueOf(setting.getN2()), "setting_outcome");
					 if(outcomeSetting != null)
					 {
						 setting.setOutcomecode(outcomeSetting.getT1());
					 }
				 }
				 
				 if(setting.getN3()!=0)
				 {
					 SettingSearchDao settingSearch = new SettingSearchDao();
					 SettingData outputSetting = settingSearch.getSettingById(String.valueOf(setting.getN3()), "setting_output");
					 if(outputSetting != null)
					 {
						 setting.setOutputcode(outputSetting.getT1());
					 }
				 }
				 
				 if(setting.getN4() != 0)
				 {
					 for(CommonEnum.Template t : CommonEnum.Template.values())
						{
							if(t.value() == setting.getN4())
							{
								setting.setTemplate(t.description());
								break;
							}
						}
				 }
				 
				 resList.add(setting);
			 }
			
			res.setCount(getTotalUserCount(usearch,con , tbname));
			res.setSettingDataList(resList);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			DataBaseConnection.close(con);
		}
			
		return res;
	}
	
	public  List<SettingData> getSettingList(String tbname)
	{

		List<SettingData> resList = new ArrayList<SettingData>();
		Connection con = null;
		SettingData setting;
		String query = "Select id,setting_date,t1,t2,t3,t4,t5,n1,n2,n3,n4,n5 from "+ tbname ;
		
		
		try {
			con = DataBaseConnection.getConnection();
			Statement st = con.createStatement();
			 ResultSet rs = st.executeQuery(query);
			 
			 while(rs.next())
			 {
				 setting = new SettingData();
				 setting.setId(rs.getInt("id"));
				 setting.setSetting_date(PrjUtil.StringtoDate(rs.getString("setting_date")));
				 setting.setSettingDate(PrjUtil.datetimeTostring(rs.getString("setting_date")));
				 setting.setT1(rs.getString("t1"));
				 setting.setT2(rs.getString("t2"));
				 setting.setT3(rs.getString("t3"));
				 setting.setT4(rs.getString("t4"));
				 setting.setT5(rs.getString("t5"));
				 setting.setN1(rs.getLong("n1"));
				 setting.setN2(rs.getLong("n2"));
				 setting.setN3(rs.getLong("n3"));
				 setting.setN4(rs.getLong("n4"));
				 setting.setN5(rs.getLong("n5"));
				 resList.add(setting);
			 }
					
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			DataBaseConnection.close(con);
		}
			
		return resList;
	}
	
	public  int getTotalUserCount(SettingSearchData search,Connection con , String tbname)
	{
		int count = 0;
		String filter = getCountCriteria(search);
		String query = "Select count(*) as count from "+tbname+ filter;
		Statement st;
		try {
			st = con.createStatement();
			ResultSet rs = st.executeQuery(query);
			if(rs.next())
				count = rs.getInt("count");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 return count;
	}
	
	public  SettingData getSettingById(String id , String tbname)
	{
		SettingData setting = new SettingData();
		Connection con = null;
		String query = "Select id,setting_date,t1,t2,t3,t4,t5,n1,n2,n3,n4,n5 from "+tbname+" where id = "+id;
		
		con = DataBaseConnection.getConnection();
		Statement st;
		try {
			st = con.createStatement();
			 ResultSet rs = st.executeQuery(query);
			 if(rs.next())
			 {
				 setting.setId(rs.getInt("id"));
				 setting.setSetting_date(PrjUtil.StringtoDate(rs.getString("setting_date")));
				 setting.setSettingDate(PrjUtil.datetimeTostring(rs.getString("setting_date")));
				 setting.setT1(rs.getString("t1"));
				 setting.setT2(rs.getString("t2"));
				 setting.setT3(rs.getString("t3"));
				 setting.setT4(rs.getString("t4"));
				 setting.setT5(rs.getString("t5"));
				 setting.setN1(rs.getLong("n1"));
				 setting.setN2(rs.getLong("n2"));
				 setting.setN3(rs.getLong("n3"));
				 setting.setN4(rs.getLong("n4"));
				 setting.setN5(rs.getLong("n5"));
				 
				 if(setting.getN1()!=0)
				 {
					 SettingSearchDao settingSearch = new SettingSearchDao();
					 SettingData impactSetting = settingSearch.getSettingById(String.valueOf(setting.getN1()), "setting_goal");
					 if(impactSetting != null)
					 {
						 setting.setImpactcode(impactSetting.getT1());
					 }
				 }
				 
				 if(setting.getN2()!=0)
				 {
					 SettingSearchDao settingSearch = new SettingSearchDao();
					 SettingData outcomeSetting = settingSearch.getSettingById(String.valueOf(setting.getN2()), "setting_outcome");
					 if(outcomeSetting != null)
					 {
						 setting.setOutcomecode(outcomeSetting.getT1());
					 }
				 }
				 
				 if(setting.getN3()!=0)
				 {
					 SettingSearchDao settingSearch = new SettingSearchDao();
					 SettingData outputSetting = settingSearch.getSettingById(String.valueOf(setting.getN3()), "setting_output");
					 if(outputSetting != null)
					 {
						 setting.setOutputcode(outputSetting.getT1());
					 }
				 }
				 
				 if(setting.getN4() != 0)
				 {
					 for(CommonEnum.Template t : CommonEnum.Template.values())
						{
							if(t.value() == setting.getN4())
							{
								setting.setTemplate(t.description());
								break;
							}
						}
				 }
				 
			 }

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			DataBaseConnection.close(con);
		}
		
		
		return setting;
	}
	
	public  SettingData getSettingByCode(String code , String tbname)
	{
		SettingData setting = null;
		Connection con = null;
		String query = "Select id,setting_date,t1,t2,t3,t4,t5,n1,n2,n3,n4,n5 from "+tbname+" where t1 = '"+code+"'";
		
		con = DataBaseConnection.getConnection();
		Statement st;
		try {
			st = con.createStatement();
			 ResultSet rs = st.executeQuery(query);
			 if(rs.next())
			 {
				 setting = new SettingData();
				 setting.setId(rs.getInt("id"));
				 setting.setSetting_date(PrjUtil.StringtoDate(rs.getString("setting_date")));
				 setting.setSettingDate(PrjUtil.stringToDate(rs.getDate("setting_date")));
				 setting.setT1(rs.getString("t1"));
				 setting.setT2(rs.getString("t2"));
				 setting.setT3(rs.getString("t3"));
				 setting.setT4(rs.getString("t4"));
				 setting.setT5(rs.getString("t5"));
				 setting.setN1(rs.getLong("n1"));
				 setting.setN2(rs.getLong("n2"));
				 setting.setN3(rs.getLong("n3"));
				 setting.setN4(rs.getLong("n4"));
				 setting.setN5(rs.getLong("n5"));
				 
				 if(setting.getN1()!=0)
				 {
					 SettingSearchDao settingSearch = new SettingSearchDao();
					 SettingData impactSetting = settingSearch.getSettingById(String.valueOf(setting.getN1()), "setting_goal");
					 if(impactSetting != null)
					 {
						 setting.setImpactcode(impactSetting.getT1());
					 }
				 }
				 
				 if(setting.getN2()!=0)
				 {
					 SettingSearchDao settingSearch = new SettingSearchDao();
					 SettingData outcomeSetting = settingSearch.getSettingById(String.valueOf(setting.getN2()), "setting_outcome");
					 if(outcomeSetting != null)
					 {
						 setting.setOutcomecode(outcomeSetting.getT1());
					 }
				 }
				 
				 if(setting.getN3()!=0)
				 {
					 SettingSearchDao settingSearch = new SettingSearchDao();
					 SettingData outputSetting = settingSearch.getSettingById(String.valueOf(setting.getN3()), "setting_output");
					 if(outputSetting != null)
					 {
						 setting.setOutputcode(outputSetting.getT1());
					 }
				 }
				 
				 if(setting.getN4() != 0)
				 {
					 for(CommonEnum.Template t : CommonEnum.Template.values())
						{
							if(t.value() == setting.getN4())
							{
								setting.setTemplate(t.description());
								break;
							}
						}
				 }
			 }

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			DataBaseConnection.close(con);
		}
		
		
		return setting;
	}
	
	public boolean validateSettingUpdate(String id,String code , String tbname) {
		PreparedStatement ps = null;
		try {
			Connection con = DataBaseConnection.getConnection();
			ps = con.prepareStatement("Select * from "+tbname+" where t1 = ? and id <> ?");
			ps.setString(1, code);
			ps.setString(2, id);

			ResultSet rs = ps.executeQuery();

			if (rs.next()) 
			{
				return false;
			}
			else return true;
		} catch (SQLException ex) {
			System.out.println("Login error -->" + ex.getMessage());
			return false;
		} 
		
	}
	
	
	
	public  String getCriteria(SettingSearchData criteria)
	{
		String filter = " where 1=1";
		
		if(!criteria.getCode().equals(""))
			
		{
			filter += " and t1 like '%"+criteria.getCode()+"%'";
			
		}
		if(criteria.getN1() != 0 )
		{
			filter += " and N1="+criteria.getN1();
		}
		
		if(criteria.getN2() != 0 )
		{
			filter += " and N2="+criteria.getN2();
		}
		
		if(criteria.getN3() != 0 )
		{
			filter += " and N3="+criteria.getN3();
		}
		
		filter += " limit "+criteria.getOffset()+","+criteria.getLimit();
		
		return filter;
	}
	
	public static String getCountCriteria(SettingSearchData criteria)
	{
		String filter = " where 1=1";
		
		if(!criteria.getCode().equals(""))		
		{
			filter += " and t1 like '%"+criteria.getCode()+"%'";
			
		}
		
		if(criteria.getN1() != 0 )
		{
			filter += " and N1="+criteria.getN1();
		}
		
		if(criteria.getN2() != 0 )
		{
			filter += " and N2="+criteria.getN2();
		}
		
		if(criteria.getN3() != 0 )
		{
			filter += " and N3="+criteria.getN3();
		}
		
		return filter;
	}
	
	

}
