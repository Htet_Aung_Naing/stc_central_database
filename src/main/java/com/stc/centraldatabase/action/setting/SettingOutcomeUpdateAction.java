package com.stc.centraldatabase.action.setting;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import com.stc.centraldatabase.model.setting.SettingData;
import com.stc.centraldatabase.service.setting.SettingDataService;
import com.stc.centraldatabase.service.setting.SettingDataServiceImpl;
import com.stc.centraldatatbase.util.SessionUtil;



@ManagedBean(name = "settingOutcomeUpdateAction")
@ViewScoped
public class SettingOutcomeUpdateAction implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8960748410953236774L;

	SettingDataService settingService;
	DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
	List<SettingData> settingImpactList;
	String settingID;
	
	public List<SettingData> getSettingImpactList() {
		return settingImpactList;
	}

	public void setSettingImpactList(List<SettingData> settingImpactList) {
		this.settingImpactList = settingImpactList;
	}

	@ManagedProperty(value = "#{setting}")
	SettingData setting;
	
	


	public SettingData getSetting() {
		return setting;
	}

	public void setSetting(SettingData setting) {
		this.setting = setting;
	}

	@PostConstruct
	public void init()  
	{
		this.setting = new SettingData();	
		this.settingService  = new SettingDataServiceImpl();
		this.settingImpactList = settingService.getSettingList("setting_goal");
		settingID = (String) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("outcome_id");
		this.setting = settingService.findByid(settingID, "setting_outcome");

	}
	
	public String update()
	{
		HttpSession session = SessionUtil.getSession();
		setting.setModifiedUserName((String) session.getAttribute("userid"));
		session.setAttribute("update_success_title", "Outcome Update Successfully!");
		if(settingService.validateSettingUpdate(settingID, setting.getT1(), "setting_outcome"))
		{
			if(settingService.update(setting, "setting_outcome")) 
			{	
				FacesContext.getCurrentInstance().getExternalContext().getFlash().put("settingUpdateData", setting);
				return "settingUpdateSuccess";
			}
				
			else
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "ERROR!", "Cannot Update!"));
		}else
		{
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "ERROR!", "Code must not duplicate!"));
		}
		
		return null;
	}
	
}
