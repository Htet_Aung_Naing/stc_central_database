package com.stc.centraldatabase.dao.activity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;

import com.stc.centraldatabase.model.activity.ActivityData;
import com.stc.centraldatatbase.util.DataBaseConnection;
import com.stc.centraldatatbase.util.PrjUtil;
import com.stc.centraldatatbase.util.Result;

public class ActivityDao {
	
	public Result insertActionData(ActivityData setting)
	{
		Connection con = null;
		PreparedStatement ps = null;
		Result res = new Result();

		 	try 
		 	{
				String query;
				
				if(setting != null)
				{
					con = DataBaseConnection.getConnection();
				     query = "INSERT INTO SETTING_ACTIVITY (`SETTING_DATE`, `CREATED_USER_NAME`, "
				     		+ "`CREATED_DATE`, `MODIFIED_DATE`, `MODIFIED_USER_NAME`,"
				     		+ " `RECORD_STATUS`, `CODE`, `DESCRIPTION`, `TOWNSHIP_CODE`, `TOWNSHIP_NAME`, `VILLAGE_CODE`, "
				     		+ "`VILLAGE_NAME`, `PROGRAM_PERIOD`, `TARGET`, `REACH` , `IMPACT_KEY`, `OUTCOME_KEY`,"
				     		+ "OUTPUT_KEY,TEMPLATE_KEY,OTHER_PROGRAM) "
				     		+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?,?,?)";
				     
				     
				     int i = 1;
				     ps = con.prepareStatement(query,Statement.RETURN_GENERATED_KEYS);
				     ps.setString(i++, PrjUtil.stringToDate(setting.getSetting_date()));
				     ps.setString(i++, setting.getCreatedUserName());
				     ps.setString(i++, PrjUtil.stringToDate(new Date()));
				     ps.setString(i++, PrjUtil.stringToDate(new Date()));
				     ps.setString(i++, setting.getModifiedUserName());
				     ps.setInt(i++, setting.getRecord_status());
				     ps.setString(i++, setting.getCode());
				     ps.setString(i++, setting.getDesc());
				     ps.setString(i++, setting.getTownshipCode());
				     ps.setString(i++, setting.getTownshipName());
				     ps.setString(i++, setting.getVillageCode());
				     ps.setString(i++, setting.getVillageName());
				     ps.setString(i++, setting.getProgramPeriod());
				     ps.setLong(i++, setting.getTargetpoint());
				     ps.setLong(i++, setting.getReachpoint());
				     ps.setLong(i++, setting.getImpactKey());
				     ps.setLong(i++, setting.getOutcomeKey());
				     ps.setLong(i++, setting.getOutputKey());
				     ps.setLong(i++, setting.getTemplateKey());
				     ps.setString(i++, setting.getInterventionProgram());
				     ps.execute();
				    
				     res.setRes(true);
				     ResultSet rs = ps.getGeneratedKeys();
						if(rs.next())
							res.setResparentid(rs.getInt(1));
						
						 con.close();
				     return res; 
					   				  
				}else
				{
					res.setRes(false);
					return res;
				}
				 
			} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
	
			res.setRes(false);
			return res;
			
		}
				
		}
	
	public  boolean updateActivity(ActivityData setting) {
		Connection con = null;
		PreparedStatement ps = null;
		String query;
		try {
			con = DataBaseConnection.getConnection();
			
		
	     query = "UPDATE setting_activity SET SETTING_DATE = ?,MODIFIED_DATE = ?, MODIFIED_USER_NAME = ?,"
			     		+ "CODE=?,DESCRIPTION=?,TOWNSHIP_CODE=?,TOWNSHIP_NAME=?,VILLAGE_CODE=?, "
			     		+ "VILLAGE_NAME=?,PROGRAM_PERIOD=?,TARGET=?,REACH=?,IMPACT_KEY=?,OUTCOME_KEY=?"
			     		+ ",OUTPUT_KEY=?,TEMPLATE_KEY=?,OTHER_PROGRAM=? WHERE ID = ?";
			     		
			    int i = 1;
				ps = con.prepareStatement(query);
				 ps.setString(i++, PrjUtil.stringToDate(setting.getSetting_date()));

				ps.setString(i++, PrjUtil.stringToDate(new Date()));
				ps.setString(i++, setting.getModifiedUserName());
				ps.setString(i++, setting.getCode());
				ps.setString(i++, setting.getDesc());
				ps.setString(i++, setting.getTownshipCode());
				ps.setString(i++, setting.getTownshipName());
				ps.setString(i++, setting.getVillageCode());
				ps.setString(i++, setting.getVillageName());
				ps.setString(i++, setting.getProgramPeriod());
				ps.setLong(i++, setting.getTargetpoint());
				ps.setLong(i++, setting.getReachpoint());
				ps.setLong(i++, setting.getImpactKey());
				ps.setLong(i++, setting.getOutcomeKey());
				ps.setLong(i++, setting.getOutputKey());
				ps.setLong(i++, setting.getTemplateKey());
				ps.setString(i++, setting.getInterventionProgram());
				ps.setLong(i++, setting.getId());
				ps.execute();
			      
			    con.close();
			    return true;
			
			
			
		} catch (SQLException ex) {
			System.out.println("Activity Registration Update error -->" + ex.getMessage());
			return false;
		} finally {
			DataBaseConnection.close(con);
		}
	}
	
	public  boolean delete(String settingid) {
		Connection con = null;
		PreparedStatement ps = null;
		String query;
		try {
			con = DataBaseConnection.getConnection();
			    	        
			     query = "DELETE FROM setting_activity where id=?";
			    int i = 1;
				ps = con.prepareStatement(query);
				ps.setString(i++, settingid);
				ps.execute();
			      
			    con.close();
			    return true;
			
			
			
		} catch (SQLException ex) {
			System.out.println("Setting Delete error -->" + ex.getMessage());
			return false;
		} finally {
			DataBaseConnection.close(con);
		}
	}
	

}
