package com.stc.centraldatabase.action.user;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;

import com.stc.centraldatabase.dao.user.LoginDao;
import com.stc.centraldatabase.model.user.UserInfo;
import com.stc.centraldatabase.service.user.UserService;
import com.stc.centraldatabase.service.user.UserServiceImpl;
import com.stc.centraldatatbase.util.SessionUtil;


@ManagedBean(name = "loginAction")
@SessionScoped
public class LoginAction implements Serializable{
	

	UserService userService;
	
	

	
	public UserInfo getUser() {
		return user;
	}
	public void setUser(UserInfo user) {
		this.user = user;
	}
	/**
	 * 
	 */
	
	
	private static final long serialVersionUID = -4930794008257080949L;
	
	String userid;
	String pwd;
	int role;
	
	@ManagedProperty(value = "#{userInfo}")
	UserInfo user;
	

	@ManagedProperty(value = "#{authenManager}")
	AuthenticationManager authenManager;
	
	
	public AuthenticationManager getAuthenManager() {
		return authenManager;
	}
	public void setAuthenManager(AuthenticationManager authenManager) {
		this.authenManager = authenManager;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getPwd() { 
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	public int getRole() {
		return role;
	}
	public void setRole(int role) {
		this.role = role;
	}
	
	public String login()
	{
		try 
		{
		
			Authentication request = new UsernamePasswordAuthenticationToken(userid, pwd);
			
			Authentication result = authenManager.authenticate(request);
			SecurityContextHolder.getContext().setAuthentication(result);
			 this.user = LoginDao.validate(userid, pwd);
			 
			 userService = new UserServiceImpl();
			
			 user = userService.getUser(userid, pwd);
			
			if (user != null) {
				HttpSession session = SessionUtil.getSession();
				session.setAttribute("userid", userid);
				session.setAttribute("role", user.getRole());

				role = user.getRole();
				return "home";
			} else {
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "ERROR!", "User name and password are incorrect!"));
				return null;
			}
		
		} catch (AuthenticationException e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "ERROR!", "User name and password are incorrect!"));
			return null;
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public String logout() {
		
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().clear();
		SecurityContextHolder.clearContext();
		
		return "login";
	}


}
