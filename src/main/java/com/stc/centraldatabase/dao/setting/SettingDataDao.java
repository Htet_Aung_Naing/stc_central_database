package com.stc.centraldatabase.dao.setting;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Date;

import com.stc.centraldatabase.model.setting.SettingData;
import com.stc.centraldatatbase.util.DataBaseConnection;
import com.stc.centraldatatbase.util.PrjUtil;


public class SettingDataDao {

	public boolean insertSettingData(SettingData setting , String tbname)
	{
		Connection con = null;
		PreparedStatement ps = null;
		

		 	try 
		 	{
				String query;
				
				if(setting != null)
				{
					con = DataBaseConnection.getConnection();
				     query = "INSERT INTO "+tbname+" (`SETTING_DATE`, `CREATED_USER_NAME`, "
				     		+ "`CREATED_DATE`, `MODIFIED_DATE`, `MODIFIED_USER_NAME`,"
				     		+ " `RECORD_STATUS`, `T1`, `T2`, `T3`, `T4`, `T5`, `N1`, `N2`, `N3`, `N4`, `N5`) "
				     		+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
				     
				     
				     int i = 1;
				     ps = con.prepareStatement(query);
				     ps.setString(i++, PrjUtil.stringToDate(setting.getSetting_date()));
				     ps.setString(i++, setting.getCreatedUserName());
				     ps.setString(i++, PrjUtil.stringToDate(new Date()));
				     ps.setString(i++, PrjUtil.stringToDate(new Date()));
				     ps.setString(i++, setting.getModifiedUserName());
				     ps.setInt(i++, setting.getRecord_status());
				     ps.setString(i++, setting.getT1() );
				     ps.setString(i++, setting.getT2() );
				     ps.setString(i++, setting.getT3() );
				     ps.setString(i++, setting.getT4() );
				     ps.setString(i++, setting.getT5() );
				     ps.setLong(i++, setting.getN1());
				     ps.setLong(i++, setting.getN2());
				     ps.setLong(i++, setting.getN3());
				     ps.setLong(i++, setting.getN4());
				     ps.setLong(i++, setting.getN5());
				     ps.execute();
				     con.close();
				     return true; 
					   				  
				}else
				{
				
					return false;
				}
				 
			} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
	
			return false;
			
		}
				
		}
	
	public  boolean update(SettingData setting , String tbname) {
		Connection con = null;
		PreparedStatement ps = null;
		String query;
		try {
			con = DataBaseConnection.getConnection();
			
	     query = "UPDATE "+tbname+" SET `SETTING_DATE` = ?,`MODIFIED_DATE` = ?, `MODIFIED_USER_NAME` = ?,"
			     		+ "`T1` = ?, `T2` = ?, `T3` = ?,`T4` = ?, `T5` = ?, `N1` = ?, `N2` = ?, "
			     		+ "`N3` = ?, N4 = ? , `N5`= ? WHERE `ID` = ?";
			    int i = 1;
				ps = con.prepareStatement(query);
				 ps.setString(i++, PrjUtil.stringToDate(setting.getSetting_date()));
				ps.setString(i++, PrjUtil.stringToDate(new Date()));
				ps.setString(i++, setting.getModifiedUserName());
				ps.setString(i++, setting.getT1());
				ps.setString(i++, setting.getT2());
				ps.setString(i++, setting.getT3());
				ps.setString(i++, setting.getT4());
				ps.setString(i++, setting.getT5());
				ps.setLong(i++, setting.getN1());
				ps.setLong(i++, setting.getN2());
				ps.setLong(i++, setting.getN3());
				ps.setLong(i++, setting.getN4());
				ps.setLong(i++, setting.getN5());
				ps.setLong(i++, setting.getId());
				ps.execute();
			      
			    con.close();
			    return true;
			
			
			
		} catch (SQLException ex) {
			System.out.println("Setting Registration Update error -->" + ex.getMessage());
			return false;
		} finally {
			DataBaseConnection.close(con);
		}
	}
	
	
	public  boolean delete(String settingid , String tbname) {
		Connection con = null;
		PreparedStatement ps = null;
		String query;
		try {
			con = DataBaseConnection.getConnection();
			    	        
			     query = "DELETE FROM "+tbname+" where id=?";
			    int i = 1;
				ps = con.prepareStatement(query);
				ps.setString(i++, settingid);
				ps.execute();
			      
			    con.close();
			    return true;
			
			
			
		} catch (SQLException ex) {
			System.out.println("Setting Delete error -->" + ex.getMessage());
			return false;
		} finally {
			DataBaseConnection.close(con);
		}
	}
	
}
