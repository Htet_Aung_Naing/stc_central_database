package com.stc.centraldatabase.model.templatethree;

import java.io.Serializable;
import java.util.Date;

public class TemplateThreeData implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6823864922195671024L;
	
	String modifiedDate;
	String createdUserName;
	String modifiedUserName;
	String createdDate;
	String code;
	String desc;
	Date settingDate;
	String settingDateInDateFormat;
	int enrollBoys;
	int enrollGirls;
	int enrollPoorBoys;
	int enrollPoorGirls;
	int enrollDisabiltyBoys;
	int enrollDisabilityGirls;
	int enrollEthnicBoys;
	int enrollEthnicGirls;
	int tEnroll;
	int tEnrllPoor;
	int tEnrollDisability;
	int tEthnic;
	int id;
	int parentid;
	String percentageMale;
	String percentageFemale;
	
	public String getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	public String getCreatedUserName() {
		return createdUserName;
	}
	public void setCreatedUserName(String createdUserName) {
		this.createdUserName = createdUserName;
	}
	public String getModifiedUserName() {
		return modifiedUserName;
	}
	public void setModifiedUserName(String modifiedUserName) {
		this.modifiedUserName = modifiedUserName;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public String getPercentageMale() {
		return percentageMale;
	}
	public void setPercentageMale(String percentageMale) {
		this.percentageMale = percentageMale;
	}
	public String getPercentageFemale() {
		return percentageFemale;
	}
	public void setPercentageFemale(String percentageFemale) {
		this.percentageFemale = percentageFemale;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public Date getSettingDate() {
		return settingDate;
	}
	public void setSettingDate(Date settingDate) {
		this.settingDate = settingDate;
	}
	public String getSettingDateInDateFormat() {
		return settingDateInDateFormat;
	}
	public void setSettingDateInDateFormat(String settingDateInDateFormat) {
		this.settingDateInDateFormat = settingDateInDateFormat;
	}
	public int getEnrollBoys() {
		return enrollBoys;
	}
	public void setEnrollBoys(int enrollBoys) {
		this.enrollBoys = enrollBoys;
	}
	public int getEnrollGirls() {
		return enrollGirls;
	}
	public void setEnrollGirls(int enrollGirls) {
		this.enrollGirls = enrollGirls;
	}
	public int getEnrollPoorBoys() {
		return enrollPoorBoys;
	}
	public void setEnrollPoorBoys(int enrollPoorBoys) {
		this.enrollPoorBoys = enrollPoorBoys;
	}
	public int getEnrollPoorGirls() {
		return enrollPoorGirls;
	}
	public void setEnrollPoorGirls(int enrollPoorGirls) {
		this.enrollPoorGirls = enrollPoorGirls;
	}
	public int getEnrollDisabiltyBoys() {
		return enrollDisabiltyBoys;
	}
	public void setEnrollDisabiltyBoys(int enrollDisabiltyBoys) {
		this.enrollDisabiltyBoys = enrollDisabiltyBoys;
	}
	public int getEnrollDisabilityGirls() {
		return enrollDisabilityGirls;
	}
	public void setEnrollDisabilityGirls(int enrollDisabilityGirls) {
		this.enrollDisabilityGirls = enrollDisabilityGirls;
	}
	public int getEnrollEthnicBoys() {
		return enrollEthnicBoys;
	}
	public void setEnrollEthnicBoys(int enrollEthnicBoys) {
		this.enrollEthnicBoys = enrollEthnicBoys;
	}
	public int getEnrollEthnicGirls() {
		return enrollEthnicGirls;
	}
	public void setEnrollEthnicGirls(int enrollEthnicGirls) {
		this.enrollEthnicGirls = enrollEthnicGirls;
	}
	public int gettEnroll() {
		return tEnroll;
	}
	public void settEnroll(int tEnroll) {
		this.tEnroll = tEnroll;
	}
	public int gettEnrllPoor() {
		return tEnrllPoor;
	}
	public void settEnrllPoor(int tEnrllPoor) {
		this.tEnrllPoor = tEnrllPoor;
	}
	public int gettEnrollDisability() {
		return tEnrollDisability;
	}
	public void settEnrollDisability(int tEnrollDisability) {
		this.tEnrollDisability = tEnrollDisability;
	}
	public int gettEthnic() {
		return tEthnic;
	}
	public void settEthnic(int tEthnic) {
		this.tEthnic = tEthnic;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getParentid() {
		return parentid;
	}
	public void setParentid(int parentid) {
		this.parentid = parentid;
	}
	
	public TemplateThreeData()
	{
		this.id = 0;
		this.code = "";
		this.desc = "";
		this.enrollBoys = 0;
		this.enrollGirls = 0;
		this.enrollPoorBoys = 0;
		this.enrollPoorGirls = 0;
		this.enrollDisabiltyBoys = 0;
		this.enrollDisabilityGirls = 0;
		this.parentid = 0;
		this.enrollEthnicBoys = 0;
		this.enrollEthnicGirls = 0;
		this.tEnrllPoor = 0;
		this.tEnroll = 0;
		this.tEnrollDisability = 0;
		this.tEthnic = 0;
		this.percentageFemale = "";
		this.percentageMale = "";
		this.createdDate = "";
		this.createdUserName = "";
		this.modifiedDate = "";
		this.modifiedUserName = "";
		this.percentageFemale = "";
		this.percentageMale = "";
	}

}
