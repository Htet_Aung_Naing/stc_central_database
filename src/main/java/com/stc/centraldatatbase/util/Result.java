package com.stc.centraldatatbase.util;

import java.io.Serializable;

public class Result implements Serializable{


	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2847222060951752348L;
	int resparentid;
	boolean res;
	public int getResparentid() {
		return resparentid;
	}
	public void setResparentid(int resparentid) {
		this.resparentid = resparentid;
	}
	public boolean isRes() {
		return res;
	}
	public void setRes(boolean res) {
		this.res = res;
	}
	
	public Result()
	{
		this.res = false;
		this.resparentid = 0;
	}

}
