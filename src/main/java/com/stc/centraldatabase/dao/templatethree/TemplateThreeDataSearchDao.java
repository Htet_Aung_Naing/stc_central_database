package com.stc.centraldatabase.dao.templatethree;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.stc.centraldatabase.model.templateone.TemplateSearchData;
import com.stc.centraldatabase.model.templatethree.TemplateThreeData;
import com.stc.centraldatabase.model.templatethree.TemplateThreeSearchPaginateData;
import com.stc.centraldatatbase.util.DataBaseConnection;
import com.stc.centraldatatbase.util.PrjUtil;

public class TemplateThreeDataSearchDao 
{
	public  String getCriteria(TemplateSearchData criteria)
	{
		String filter = " where 1=1";
		
		if(!criteria.getCode().equals(""))
			
		{
			filter += " and code like '%"+criteria.getCode()+"%'";
			
		}
		
		if(!criteria.getDesc().equals(""))
			
		{
			filter += " and description like '%"+criteria.getDesc()+"%'";
			
		}
		if(!criteria.getParentid().equals(""))
		{
			filter += " and parent_id = "+criteria.getParentid();
		}
		if(criteria.getLimit() != 0)
			filter += " limit "+criteria.getOffset()+","+criteria.getLimit();

		return filter;
	}
	
	public static String getCountCriteria(TemplateSearchData criteria)
	{
		String filter = " where 1=1";
		
		if(!criteria.getCode().equals(""))
			
		{
			filter += " and code like '%"+criteria.getCode()+"%'";
			
		}
		
		if(!criteria.getDesc().equals(""))
			
		{
			filter += " and Description like '%"+criteria.getDesc()+"%'";
			
		}
		if(!criteria.getParentid().equals(""))
		{
			filter += " and parent_id = "+criteria.getParentid();
		}
		return filter;
	}
	
	public boolean validateSettingUpdate(String id,String code ) {
		PreparedStatement ps = null;
		try {
			Connection con = DataBaseConnection.getConnection();
			ps = con.prepareStatement("Select * from template_three where code = ? and parent_id <> ?");
			ps.setString(1, code);
			ps.setString(2, id);

			ResultSet rs = ps.executeQuery();

			if (rs.next()) 
			{
				return false;
			}
			else return true;
		} catch (SQLException ex) {
			System.out.println("Validation error -->" + ex.getMessage());
			return false;
		} 
		
	}
	
	public  int getTotalCount(TemplateSearchData search,Connection con)
	{
		int count = 0;
		String filter = getCountCriteria(search);
		String query = "Select count(*) as count from TEMPLATE_THREE "+ filter;
		Statement st;
		try {
			st = con.createStatement();
			ResultSet rs = st.executeQuery(query);
			if(rs.next())
				count = rs.getInt("count");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 return count;
	}
	
	public  TemplateThreeSearchPaginateData find(TemplateSearchData usearch)
	{
		TemplateThreeSearchPaginateData res = new TemplateThreeSearchPaginateData();
		String filter = getCriteria(usearch);
		List<TemplateThreeData> resList = new ArrayList<TemplateThreeData>();
		Connection con = null;
		TemplateThreeData setting;
		String query = "Select * from TEMPLATE_THREE" +filter;
		
		
		try {
			con = DataBaseConnection.getConnection();
			Statement st = con.createStatement();
			 ResultSet rs = st.executeQuery(query);
			 
			 while(rs.next())
			 {
				 
				 setting = new TemplateThreeData();
				 setting.setId(rs.getInt("id"));
				 setting.setSettingDate(PrjUtil.StringtoDate(rs.getString("setting_date")));				 
				 setting.setCode(rs.getString("code"));
				 setting.setDesc(rs.getString("description"));
				 setting.setParentid(rs.getInt("parent_id"));
				 setting.setEnrollBoys(rs.getInt("enroll_boy"));
				 setting.setEnrollGirls(rs.getInt("enroll_girl"));
				 setting.setEnrollPoorBoys(rs.getInt("enroll_poor_boy"));
				 setting.setEnrollPoorGirls(rs.getInt("enroll_poor_girl"));
				 setting.setEnrollDisabiltyBoys(rs.getInt("ENROLL_DISABILITY_BOY"));
				 setting.setEnrollDisabilityGirls(rs.getInt("ENROLL_DISABILITY_GIRL"));
				 setting.setEnrollEthnicBoys(rs.getInt("ENROLL_ETHNIC_BOY"));
				 setting.setEnrollEthnicGirls(rs.getInt("ENROLL_ETHNIC_GIRL"));
				 setting.setSettingDateInDateFormat(PrjUtil.datetimeTostring(rs.getString("setting_date")));
				 
				 resList.add(setting);
			 }
			
			res.setCount(getTotalCount(usearch,con));
			res.setTemplateOneDataList(resList);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			DataBaseConnection.close(con);
		}
			
		return res;
	}
	
	
	public TemplateThreeData findById(String id)
	{	
		Connection con = null;
		TemplateThreeData setting = null;
		String query = "Select * from template_three where id="+id ;
		
		
		try {
			con = DataBaseConnection.getConnection();
			Statement st = con.createStatement();
			 ResultSet rs = st.executeQuery(query);
			 
			 while(rs.next())
			 {
				
				 setting = new TemplateThreeData();
				 setting.setId(rs.getInt("id"));
				 setting.setParentid(rs.getInt("parent_id"));
				 setting.setSettingDate(PrjUtil.StringtoDate(rs.getString("setting_date")));
				 setting.setCode(rs.getString("code"));
				 setting.setDesc(rs.getString("description"));
				 setting.setParentid(rs.getInt("parent_id"));
				 setting.setEnrollBoys(rs.getInt("enroll_boy"));
				 setting.setEnrollGirls(rs.getInt("enroll_girl"));
				 setting.setEnrollPoorBoys(rs.getInt("enroll_poor_boy"));
				 setting.setEnrollPoorGirls(rs.getInt("enroll_poor_girl"));
				 setting.setEnrollDisabiltyBoys(rs.getInt("ENROLL_DISABILITY_BOY"));
				 setting.setEnrollDisabilityGirls(rs.getInt("ENROLL_DISABILITY_GIRL"));
				 setting.setEnrollEthnicBoys(rs.getInt("ENROLL_ETHNIC_BOY"));
				 setting.setEnrollEthnicGirls(rs.getInt("ENROLL_ETHNIC_GIRL"));
				 double percentageRecord = ((double)setting.getEnrollDisabiltyBoys()+setting.getEnrollEthnicBoys()+setting.getEnrollPoorBoys())/(double)setting.getEnrollBoys() * 100 ;
				 setting.setSettingDateInDateFormat(PrjUtil.datetimeTostring(rs.getString("setting_date")));
				 setting.setPercentageMale(String.format("%.2f", percentageRecord));
				 percentageRecord = ((double)setting.getEnrollDisabilityGirls()+setting.getEnrollEthnicGirls()+setting.getEnrollPoorGirls())/(double)setting.getEnrollGirls() * 100 ;
				 setting.setPercentageFemale(String.format("%.2f", percentageRecord));
			 }
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			DataBaseConnection.close(con);
		}
			
		 return setting;
	}
	
	
}
