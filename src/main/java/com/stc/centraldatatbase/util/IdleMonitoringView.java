package com.stc.centraldatatbase.util;



import java.io.IOException;

import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
 
@ManagedBean
public class IdleMonitoringView {
     
    public void onIdle() throws IOException {
    	String redirectPath = FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath()+"/view/errorpages/expire.xhtml";
    	FacesContext.getCurrentInstance().getExternalContext().redirect(redirectPath);
    }
 
}
