package com.stc.centraldatabase.action.templatefour;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.stc.centraldatabase.model.templatefour.TemplateFourData;
import com.stc.centraldatabase.service.templatefour.TemplateFourDataService;
import com.stc.centraldatabase.service.templatefour.TemplateFourDataServiceImpl;



@ManagedBean(name = "templateFourDeleteAction")
@ViewScoped
public class TemplateFourDeleteAction implements Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = -425655372664387173L;

	TemplateFourDataService templateService;
	String title;
	DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
	String templateId;
	String templateOnecode= "";

	

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	

	@ManagedProperty(value = "#{templatefour}")
	TemplateFourData templateFourData;
	

	public TemplateFourData getTemplateFourData() {
		return templateFourData;
	}

	public void setTemplateFourData(TemplateFourData templateFourData) {
		this.templateFourData = templateFourData;
	}

	@PostConstruct
	public void init()  
	{
		this.templateFourData = new TemplateFourData();
		
		this.templateId = (String) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("templateid");
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateParentid", templateId );
		this.templateService = new TemplateFourDataServiceImpl();
		this.templateFourData = templateService.findById(templateId);
		this.title = templateFourData.getCode()+" Delete";
	}
	
	public String delete()
	{
		
		if(templateService.delete(templateId))
		{
			FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateParentid",  String.valueOf(templateFourData.getParentid()));	
			FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateTitle",  templateFourData.getCode());
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "INFO!", "Delete Template Four Data Successfully!"));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			return "template_four_search";
		}
		else
		{
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "ERROR!", "Setting Data cannot delete!"));
			return null;
		}
		
	}
	
	public String back()
	{	
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateTitle",  templateFourData.getCode());
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("templateParentid", String.valueOf(templateFourData.getParentid()));		
		return "template_four_search";
	}
	
	
	
}
