package com.stc.centraldatabase.dao.activity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.stc.centraldatabase.dao.setting.SettingSearchDao;
import com.stc.centraldatabase.model.activity.ActivityData;
import com.stc.centraldatabase.model.activity.ActivitySearchData;
import com.stc.centraldatabase.model.activity.ActivitySearchPaginateData;
import com.stc.centraldatabase.model.setting.SettingData;
import com.stc.centraldatatbase.util.CommonEnum;
import com.stc.centraldatatbase.util.DataBaseConnection;
import com.stc.centraldatatbase.util.PrjUtil;

public class ActivitySearchDao {
	
	public  String getCriteria(ActivitySearchData criteria)
	{
		String filter = " where 1=1";
		
		if(!criteria.getCode().equals(""))
			
		{
			filter += " and code like '%"+criteria.getCode()+"%'";
			
		}
		if(criteria.getImpactkey() != 0 )
		{
			filter += " and impact_key="+criteria.getImpactkey();
		}
		
		if(criteria.getOutcomekey() != 0 )
		{
			filter += " and outcome_key="+criteria.getOutcomekey();
		}
		
		if(criteria.getOutputkey() != 0 )
		{
			filter += " and output_key="+criteria.getOutputkey();
		}
		
		if(!criteria.getTownshipCode().equals(""))
		{
			filter += " and townshp_code like '%"+criteria.getTownshipCode()+"%'";
		}
		
		if(!criteria.getTownshipName().equals(""))
		{
			filter += " and townshp_name like '%"+criteria.getTownshipName()+"%'";
		}
		
		if(!criteria.getVillageCode().equals(""))
		{
			filter += " and village_code like '%"+criteria.getVillageCode()+"%'";
		}
		
		if(!criteria.getVillageName().equals(""))
		{
			filter += " and village_name like '%"+criteria.getVillageName()+"%'";
		}
		
		
		filter += " limit "+criteria.getOffset()+","+criteria.getLimit();
		
		
		
		return filter;
	}
	
	public  int getTotalUserCount(ActivitySearchData search,Connection con)
	{
		int count = 0;
		String filter = getCountCriteria(search);
		String query = "Select count(*) as count from setting_activity "+ filter;
		Statement st;
		try {
			st = con.createStatement();
			ResultSet rs = st.executeQuery(query);
			if(rs.next())
				count = rs.getInt("count");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 return count;
	}
	
	
	public static String getCountCriteria(ActivitySearchData criteria)
	{
		String filter = " where 1=1";
		
		if(!criteria.getCode().equals(""))
			
		{
			filter += " and code like '%"+criteria.getCode()+"%'";
			
		}
		if(criteria.getImpactkey() != 0 )
		{
			filter += " and impact_key="+criteria.getImpactkey();
		}
		
		if(criteria.getOutcomekey() != 0 )
		{
			filter += " and outcome_key="+criteria.getOutcomekey();
		}
		
		if(criteria.getOutputkey() != 0 )
		{
			filter += " and output_key="+criteria.getOutputkey();
		}
		
		if(!criteria.getTownshipCode().equals(""))
		{
			filter += " and townshp_code like '%"+criteria.getTownshipCode()+"%'";
		}
		
		if(!criteria.getTownshipName().equals(""))
		{
			filter += " and townshp_name like '%"+criteria.getTownshipName()+"%'";
		}
		
		if(!criteria.getVillageCode().equals(""))
		{
			filter += " and village_code like '%"+criteria.getVillageCode()+"%'";
		}
		
		if(!criteria.getVillageName().equals(""))
		{
			filter += " and village_name like '%"+criteria.getVillageName()+"%'";
		}
		
		
		
		
		return filter;
	}
	
	public boolean validateSettingUpdate(String id,String code ) {
		PreparedStatement ps = null;
		try {
			Connection con = DataBaseConnection.getConnection();
			ps = con.prepareStatement("Select * from setting_activity where code = ? and id <> ?");
			ps.setString(1, code);
			ps.setString(2, id);

			ResultSet rs = ps.executeQuery();

			if (rs.next()) 
			{
				return false;
			}
			else return true;
		} catch (SQLException ex) {
			System.out.println("Validation error -->" + ex.getMessage());
			return false;
		} 
		
	}
	
	public ActivityData findById(String id)
	{	
		Connection con = null;
		ActivityData setting = null;
		String query = "Select * from setting_activity where id="+id ;
		
		
		try {
			con = DataBaseConnection.getConnection();
			Statement st = con.createStatement();
			 ResultSet rs = st.executeQuery(query);
			 
			 while(rs.next())
			 {
				 setting = new ActivityData();
				 setting.setId(rs.getInt("id"));
				 setting.setSetting_date(PrjUtil.StringtoDate(rs.getString("setting_date")));
				 setting.setSettingDate(PrjUtil.datetimeTostring(rs.getString("setting_date")));
				 setting.setCode(rs.getString("code"));
				 setting.setDesc(rs.getString("description"));
				 setting.setTownshipCode(rs.getString("township_code"));
				 setting.setTownshipName(rs.getString("township_name"));
				 setting.setVillageCode(rs.getString("village_code"));
				 setting.setVillageName(rs.getString("village_name"));
				 setting.setProgramPeriod(rs.getString("program_period"));
				 setting.setTargetpoint(rs.getLong("target"));
				 setting.setReachpoint(rs.getLong("reach"));
				 setting.setImpactKey(rs.getLong("impact_key"));
				 setting.setOutcomeKey(rs.getLong("outcome_key"));
				 setting.setOutputKey(rs.getLong("output_key"));
				 setting.setTemplateKey(rs.getLong("template_key"));
				 
				 if(setting.getImpactKey()!=0)
				 {
					 SettingSearchDao settingSearch = new SettingSearchDao();
					 SettingData impactSetting = settingSearch.getSettingById(String.valueOf(setting.getImpactKey()), "setting_goal");
					 if(impactSetting != null)
					 {
						 setting.setImpactcode(impactSetting.getT1());
					 }
				 }
				 
				 if(setting.getOutcomeKey()!=0)
				 {
					 SettingSearchDao settingSearch = new SettingSearchDao();
					 SettingData outcomeSetting = settingSearch.getSettingById(String.valueOf(setting.getOutcomeKey()), "setting_outcome");
					 if(outcomeSetting != null)
					 {
						 setting.setOutcomecode(outcomeSetting.getT1());
					 }
				 }
				 
				 if(setting.getOutputKey()!=0)
				 {
					 SettingSearchDao settingSearch = new SettingSearchDao();
					 SettingData outputSetting = settingSearch.getSettingById(String.valueOf(setting.getOutputKey()), "setting_output");
					 if(outputSetting != null)
					 {
						 setting.setOutputcode(outputSetting.getT1());
					 }
				 }
				 
				 if(setting.getTemplateKey() != 0)
				 {
					 for(CommonEnum.Template t : CommonEnum.Template.values())
						{
							if(t.value() == setting.getTemplateKey())
							{
								setting.setTemplate(t.description());
								break;
							}
						}
				 }
				 
				
			 }
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			DataBaseConnection.close(con);
		}
			
		 return setting;
	}
	
	public  ActivitySearchPaginateData find(ActivitySearchData usearch)
	{
		ActivitySearchPaginateData res = new ActivitySearchPaginateData();
		String filter = getCriteria(usearch);
		List<ActivityData> resList = new ArrayList<ActivityData>();
		Connection con = null;
		ActivityData setting;
		String query = "Select * from setting_activity" +filter;
		
		
		try {
			con = DataBaseConnection.getConnection();
			Statement st = con.createStatement();
			 ResultSet rs = st.executeQuery(query);
			 
			 while(rs.next())
			 {
				 setting = new ActivityData();
				 setting.setId(rs.getInt("id"));
				 setting.setSetting_date(PrjUtil.StringtoDate(rs.getString("setting_date")));
				 setting.setSettingDate(PrjUtil.datetimeTostring(rs.getString("setting_date")));
				 setting.setCode(rs.getString("code"));
				 setting.setDesc(rs.getString("description"));
				 setting.setTownshipCode(rs.getString("township_code"));
				 setting.setTownshipName(rs.getString("township_name"));
				 setting.setVillageCode(rs.getString("village_code"));
				 setting.setVillageName(rs.getString("village_name"));
				 setting.setProgramPeriod(rs.getString("program_period"));
				 setting.setTargetpoint(rs.getLong("target"));
				 setting.setReachpoint(rs.getLong("reach"));
				 setting.setImpactKey(rs.getLong("impact_key"));
				 setting.setOutcomeKey(rs.getLong("outcome_key"));
				 setting.setOutputKey(rs.getLong("output_key"));
				 setting.setTemplateKey(rs.getLong("template_key"));
				 
				 if(setting.getImpactKey()!=0)
				 {
					 SettingSearchDao settingSearch = new SettingSearchDao();
					 SettingData impactSetting = settingSearch.getSettingById(String.valueOf(setting.getImpactKey()), "setting_goal");
					 if(impactSetting != null)
					 {
						 setting.setImpactcode(impactSetting.getT1());
					 }
				 }
				 
				 if(setting.getOutcomeKey()!=0)
				 {
					 SettingSearchDao settingSearch = new SettingSearchDao();
					 SettingData outcomeSetting = settingSearch.getSettingById(String.valueOf(setting.getOutcomeKey()), "setting_outcome");
					 if(outcomeSetting != null)
					 {
						 setting.setOutcomecode(outcomeSetting.getT1());
					 }
				 }
				 
				 if(setting.getOutputKey()!=0)
				 {
					 SettingSearchDao settingSearch = new SettingSearchDao();
					 SettingData outputSetting = settingSearch.getSettingById(String.valueOf(setting.getOutputKey()), "setting_output");
					 if(outputSetting != null)
					 {
						 setting.setOutputcode(outputSetting.getT1());
					 }
				 }
				 
				 if(setting.getTemplateKey() != 0)
				 {
					 for(CommonEnum.Template t : CommonEnum.Template.values())
						{
							if(t.value() == setting.getTemplateKey())
							{
								setting.setTemplate(t.description());
								break;
							}
						}
				 }
				 
				 resList.add(setting);
			 }
			
			res.setCount(getTotalUserCount(usearch,con));
			res.setActivityDataLsit(resList);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			DataBaseConnection.close(con);
		}
			
		return res;
	}

}
